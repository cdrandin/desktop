Christopher Randin
CPSC 223P
cdrandin@csu.fulleton.edu
~
Week 1 Assignment
~
Import files from Python are needed: math, datetime

To execute python scripts just enter: "python3.2 problem1.py" and "python3.2 problem.py"

file:problem1.py
Issue: Did not ask for meteric or US measurements
Is a BMI and BMR calculator. Which takes patient info, using that info to then calculate the BMI and BMR.
Then determine whether the patient needs to gain or lose a certain amount of weight. Patients at the normal range
will be prompted to continue their diet and keep within the normal their BMR.  Patients below or above normal range
will be prompted to change their diet and follow the prompted calorie intake for the 52 weeks.

file:problem2.py
Issue: N/A
Contain 2 functions that finds prime numbers within the range of (2 - 100,000).
1 method uses trial division while the other uses Eratosthenes.  Each function is then timed
before and after their function calls. Then calculated into milliseconds. The time for both
processes are then prompted.
