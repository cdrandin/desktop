#!/usr/bin/env python3.2
'''
Christopher Randin
CPSC 223P Assignment 1 Week 1
Due: 9/6

Problem #2
'''
import math
from datetime import datetime

def trial_division(number):
	number += 1 #for use of the range function to include the value in number itself
	prime = [] #store prime numbers

	#iterate through each number from 2 to number(max range)
	for i in range(2, number):
		#check if prime
		#if a number is divided with remainder 0 at least once in each possible case, then it's not a prime 
		#else append to prime list
		for j in range(2,i):
			if i % j == 0:
				break
		else:
			prime.append(i)

	return prime

def Eratosthenes(number):
	number += 1 #for use of the range function to include the value in number itself
	numbers = [] #store truth values of all given numbers within the range
	prime = [] #store prime numbers

	#place the truth values for all numbers within the range as true
	for i in range(0,number):
		numbers.append(True)

	#process of eliminating composite numbers marking them as false
	#true values is considered prime, then that value will be appended into 
	#a seperate list
	for i in range(2,len(numbers)):
		if numbers[i] == True:
			prime.append(int(i))
			for j in range(i,len(numbers),i):
				numbers[j]=False
	
	return(prime)

def main():
	prime1= [] #containing trail_division method
	prime2= [] #containing Eratosthenes method

	#call functions to find primes within the range of (2 to max_n)
	max_n= 1299828

#---->'''
	#get time before and after trial division function call 
	#datetime.now() captures time at the instance of being called
	time_trial_1= datetime.now()
	prime1= trial_division(max_n)
	time_trial_2= datetime.now()
	time_trial_time= (time_trial_2-time_trial_1)

		print("\nThe time it takes to run trial division: %.5f milliseconds\n" %(time_trial_time.seconds * 1000 + time_trial_time.microseconds * 0.001))
#---->'''

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#get time before and after eratosthenes function call 
	#datetime.now() captures time at the instance of being called
#---->'''
	time_eratosthenes_1= datetime.now()
	prime2= Eratosthenes(max_n)	
	time_eratosthenes_2= datetime.now()
	time_eratosthenes_time= (time_eratosthenes_2 - time_eratosthenes_1)

	#Once the time has been captures. They are in microseconds using datetime.now() method. 
	#Convert it to milliseconds(i.e. 1 microsecond = 0.001 milliseconds)
	#using datatime class attribute '.microsecond' to obtain the time then convert it to milliseconds
	#Then using '.seconds' to obtain the number of seconds it took to run the program. 
	#Convert it to milliseconds(i.e. 1 second = 1000 milliseconds)
	
	print("The time it takes to run Eratosthenes: %.5f milliseconds\n" %(time_eratosthenes_time.seconds * 1000 + time_eratosthenes_time.microseconds * 0.001))
#---->'''

if __name__ == '__main__':
	main()

