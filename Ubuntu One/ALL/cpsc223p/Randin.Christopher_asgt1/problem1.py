#!/usr/bin/env python3.2
'''
Christopher Randin
CPSC 223P Assignment 1 Week 1
Due: 9/6

Problem #1
'''
import math

#process BMI and BMR using assigned functions
#output: BMI and BMR in that order
def Process_BMI_BMR(weight,height,age,gender):
	BMI= (weight/(height * height))

	height_in_cm= height * 100

	if (gender=='M' or gender=='m'):
		BMR= ((weight * 13.397) + (height_in_cm * 4.799) - (age * 5.667) + 88.362)

	elif (gender=='F' or gender=='f'):
		BMR= ((weight * 9.247) + (height_in_cm * 3.098) - (age * 4.330) + 447.593)

	else :
		print("Gender Error!")
		exit(1)

	return BMI,BMR

#function to determine the severity of the patient's current BMI range
#output: prompt displaying severity of BMI, whether to lose or gain, upper and lower bounds
def BMI_TABLE(BMI):
	if BMI < 16.00:
		severity= "Severe Thinness"
		maintain_weight= "gain"
		upper= 16.00
		lower= None

	elif BMI < 16.99:
		severity= "Moderate Thinness"
		maintain_weight= "gain"
		upper= 16.99
		lower= 16.00

	elif BMI < 18.49:
		severity= "Mild Thinness"
		maintain_weight= "gain"
		upper= 18.49
		lower= 17.00

	elif BMI < 24.99:
		severity= "Normal Range"
		maintain_weight= "normal"
		upper= 24.99
		lower= 18.50

	elif BMI < 29.99:
		severity= "Overweight"
		maintain_weight= "lose"
		upper= 30.00
		lower= 25.00

	elif BMI < 34.99:
		severity= "Obese Class I (Moderate)"
		maintain_weight= "lose"
		upper= 35.00
		lower= 30.00

	elif BMI < 39.99:
		severity= "Obese Class II(Severe)"
		maintain_weight= "lose"
		upper= 39.99
		lower= 35.00

	else:
		severity= "Obese Class III(Very Severe)"
		maintain_weight= "lose"
		upper= None
		lower= 39.99

	return severity, maintain_weight, upper, lower

#store patients info regarding to calculate BMI and BMR
patient_info = {}

####
# pre-determined info
#
#patient_info['gender']= 'M'
#patient_info['age']= 18
#patient_info['height']= 1.905
#patient_info['weight']= 187
#patient_info['weight']= patient_info['weight'] * 0.453592 #now in kg
# end
####

#Ask for patient info. Assigning into dictionary.

patient_info['gender']= str(input("Enter your gender(M/F or m/f): "))
patient_info['age']= int(input("Enter your age: "))

# 1 ft = 0.3048 meters
# 1 in = 0.0254 meters
patient_info['height']= float(input("Enter your height(in meters i.e.1ft=0.3048m | 1in=0.0254): "))

# 1 pound = 0.453592 kg
patient_info['weight']= float(input("Enter your weight(in lbs.): "))
patient_info['weight']= patient_info['weight'] * 0.453592 #now in kg



#units currently in meteric
#pass through functions.
#returned BMI, BMI, and severity of BMI(BMI_Status)
patient_info['BMI'], patient_info['BMR']= Process_BMI_BMR(patient_info['weight'],patient_info['height'],patient_info['age'],patient_info['gender'])

#upper_BMI and lower_BMI used to determine the boundaries of the BMI category.
#if upper_BMI is none, meaning there is no catergory above the current location(meaning severe obese)
#if lower_BMI is none, meaning there is no catergory below the current location(meaning severe Thinness)
patient_info['BMI_Status'], weight_management, upper_BMI, lower_BMI= BMI_TABLE(patient_info['BMI'])

#display patient's BMI and BMI along with their current position in correlation with the BMI table
print ("Your BMI is %.2f. Category: %s" %(patient_info['BMI'],patient_info['BMI_Status']))
print ("Your BMR is %.2f " %(round(patient_info['BMR'],2)))
print()

#find the neccessary weight to achieve the above and lower BMI scales
weight_upper= (upper_BMI * (patient_info['height'] * patient_info['height']))
weight_lower= (lower_BMI * (patient_info['height'] * patient_info['height']))

#calculate the difference in weight as to how far they are from the BMI scales
upper_weight_needed = math.fabs(weight_upper - patient_info['weight'])
lower_weight_needed = math.fabs(weight_lower - patient_info['weight'])

#(upper/lower)_weight_needed is how many kgs from being away to the threshold of the BMI scale
#using that. Convert it to the number of kcal / day. Multiply both sides by weight needed to maintain equilibrium
#~(7709 kcal * weight_needed) = (1 kg * weight_needed)

#which gives the total number of calories needed over 52 weeks: 
#~(7709 kcal * weight_needed) / 52 weeks = weight_needed kg / 52 weeks

#divide by 52 weeks to get kcal for 1 week conversion:
#~((7709 kcal * weight_needed)/ 52) / week 

#divide by the 7 days in a week for 1 day conversion:
#~(((7709 kcal * weight_needed)/ 52)/7) / day

#this is used ot calculate the extra calories needed to gain or lose weight to next BMI category
#measurements of kcal/day
cal_gain= (((7709 * upper_weight_needed)/ 52)/7) 
cal_lose= (((7709 * lower_weight_needed)/ 52)/7) 

#using the bounds to prompt user the possible routes of gain or losing weights into the next possible category.
#if the bounds are 'none' then there is no next possible category
#return cal gain/lose in kcal / day
if upper_BMI == None:
	print("You only choice in the next category is below, meaning you must lose weight")
	print("You need to maintain a %.02f calories per day over the next 52 weeks to lose enough weight to drop to the next category" %(patient_info['BMR'] -cal_lose))
elif lower_BMI == None:
	print("You only choice in the next category is above, meaning you must gain weight")
	print()
	print("You need to maintain a %.02f calories per day over the next 52 weeks to lose enough weight to drop to the next category" %(patient_info['BMR'] + cal_gain))
else:
	print("If you want to go to the above category in the BMI chart you need to maintain a %.02f calories per day for the next 52 weeks" %(patient_info['BMR'] + cal_gain))
	print()
	print("If you want to go to the below category in the BMI chart you need to maintain a %.02f calories per day for the next 52 weeks" %(patient_info['BMR'] - cal_lose))

