#!/usr/bin/env python3.2
'''
Christopher Randin
CPSC 223P Assignment 2
Due: 9/13

Problem #4
'''
import sys
import math

#program usage:
#python3.2(or ./problem4.py) <filename> <keyword> 
#python3.2(or ./problem4.py) decode.py *something* 

def main():
	#handle exception if a string isn't entered at command line
	#if error. Exit program.
	try:
		infile = open(sys.argv[1], "r")
		outfile = open(sys.argv[1][:-4]+"-clear.txt", "w")
		keyword = sys.argv[2]
		message = infile.read()
		message = list(message)

	except IndexError:
		print("No string entered from command line.\nCommand format: python3.2 [filename] [string(keyword)]")
		exit(1)	
	
	code_word = []
	
	#find how many keywords are needed to fill the original	text
	#returning ciper text. preparing to be encoded.
	for i in range(math.ceil(len(message)/len(keyword))):
		for j in range(len(keyword)):
			code_word.append(keyword[j])	
		
	#The person sending the message chooses a keyword and repeats it until it matches the length of the plaintext
	#match ciper text with message including ' '. Just to make future operations easier. 
	for i in range(len(message)):
		if not message[i].isalpha():
			code_word.insert(i,' ')


	#ASCII value 65 = 'A' and  97 = 'a' CAPS difference of 32
	#-1 on the range to exclude end character('\n') from being countered
	#file SHOULD end with end character '\n'
	for i in range(len(message) - 1):
		
		#find the difference from the current letter to A.
		#'A' = 65, but to include A for counter we minus 1 = 64	
		ASCII_A = 64

		#'a' = 97,but to include a for counter we minus 1 = 96	
		ASCII_a = 96

		#max number of alphabet is set to 27 to count all 26 letters
		max_alphabet = 27

		#distance from current letter in code word from a
		#used to determine shift 
		code_distance = ord(code_word[i])- ASCII_a
		
		#Used to handle uppercase letters just trivial for dealing with ascii values
		if message[i].isupper():		
			
			#distance between current letter of message from 'a'. 
			message_distance = ord(message[i]) - ASCII_A

			#Find difference between the current letter position with the code word and message
			#Handle negative difference value
			if ((message_distance - code_distance ) < 0):

				outfile.write(chr(message_distance - code_distance + max_alphabet + ASCII_A))

			#if difference are positive
			elif (( message_distance - code_distance ) >= 0):
	
				#plus 1 at the end since letter isn't counting 1 short
				outfile.write(chr(int(math.fabs(code_distance - message_distance)) + ASCII_A + 1))
		
		#Used to handle lowercase letters just trivial for dealing with ascii values
		elif message[i].islower():
	
			#distance between current letter of message from 'a'. 
			message_distance = ord(message[i]) - ASCII_a

			#Find difference between the current letter position with the code word and message
			#Handle negative difference value
			if ((message_distance - code_distance ) < 0):

				outfile.write(chr(message_distance - code_distance + max_alphabet + ASCII_a))

			#if difference are positive
			elif (( message_distance - code_distance ) >= 0):

				#plus 1 at the end since letter isn't counting 1 short
				outfile.write(chr(int(math.fabs(code_distance - message_distance)) + ASCII_a + 1))
			
		#if there is a space. Just pass over a space to show neater format.
		elif message[i] == ' ':
			outfile.write(' ')
			
	print("\nDecoded message is in:",outfile.name,end='\n\n')

	infile.close()
	outfile.close()
		
if __name__== "__main__":
	main()
	
