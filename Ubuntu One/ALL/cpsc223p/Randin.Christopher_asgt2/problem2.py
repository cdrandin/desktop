#!/usr/bin/env python3.2
'''
Christopher Randin
CPSC 223P Assignment 2
Due: 9/13

Problem #2
'''
#use sys.argv to get the arguements passed through the command line
#sys.argv[0] is the name of the file being called

import sys

#convert binary input to text hence, b2s
#returns a list of the characters for each corresponding binary value
def b2s(binary):
	#using the built in functions:
	#~int(*number(string format)*[,*base*]) 
	#base is counting based off the number of possible values. ex.) passing 2 as the base. gives to 2 possible bases( 0 and 1 )
	#number has to be in a string format

	#~chr() - converts ASCII value to it's assigned character

	string= []

	for i in binary:
		string.append(chr(int(i,2)))
	
	return string

#display string
def display(string):
	print()

	for i in string:
		print(i,end='')

	print()

def main():
#handle exception if a string isn't entered at command line
#if error. Exit program.
	try:
		filename= sys.argv[1].split()		

	except IndexError:
		print("No string entered from command line.\nCommand format: python3.2 [string(binary rep.)]")
		exit(1)

	string= b2s(binary)
	display(string)

if __name__ == "__main__":
	main()
