#!/usr/bin/env python3.2
'''
Christopher Randin
CPSC 223P Assignment 2
Due: 9/13

Problem #1
'''
#use sys.argv to get the arguements passed through the command line
#sys.argv[0] is the name of the file being called

import sys

#converts string to binary hence, s2b
#returns a list of the binary values for each corresponding character
def a2b(string):
	#this will contain the binary values of the inputted string
	binary_values= []

	#Using the built in function:
	#ord() - to convert a character to its assigned ASCII value.
	#bin() - to convert the ASCII value into a binary number given the format "0bxxxx" where x is 0 or 1.
	for i in string:
		temp= bin(ord(i))[2:].zfill(8) #[2:] discards '0b'prefix | .zfill() fills in the remaining within 8-bits with 0s
		bin_len= len(temp) #get length of binary. find difference in length of bin and the required n-bits
	
		#-(bin_len) % 8 == abs(bin_len - 8) in terms of finding difference between values
		#^ can be replaced with ".zfill"

		binary_values.append(temp)

	return(binary_values)

#For formating purposes.
#Have 7 columns
#returns nothing
def display(string):
	count=0
	print()

	for i in string:
		if count>=7:
			print()
			count=0
		print(i,end=' ')
		count += 1

	print()
	
def main():
	#handle exception if a string isn't entered at command line
	#if error. Exit program.
	try:
		string= []
		for i in range(len(sys.argv[1])):
			string.append(sys.argv[1][i])

	except IndexError:
		print("No string entered from command line.\nCommand format: python3.2 [string(letters)])
		exit(1)

	binary= a2b(string)
	display(binary)

if __name__ == '__main__':
	main()
