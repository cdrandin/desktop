import sys
import os
		
def main():
	fifoname='/tmp/myFIFO'

	if os.path.exists(fifoname):
		os.unlink(fifoname)

	os.mkfifo(fifoname)
	
	fifo_fd=open(fifoname, 'r')

	#get chift value
	shift=int(next(fifo_fd))

	#get message
	message=next(fifo_fd)



	#encode message with regards to the shift
	for i in message:
		if not i.isalpha():
			print(i, end="")

		elif i.islower:
			move = ord(i) + shift
			if move > 122:
				move -= 26
			print(chr(move), end="")

		elif i.isupper:
			move = ord(i) + shift
			if move > 90:
				move -= 26
			print(chr(move), end="")
	print()

if '__main__' == __name__:
	main()
