import sys
import os

#Fibonacci generator
def fibonacciIterator(n):
	a, b = 1, 1
	while a < n:
		yield a
		a, b = b, a+b

def main():
	try:
		print("fibonacci seriescle up to {}".format(sys.argv[1]))
		fifoname = '/tmp/myFIFO'

		if os.path.exists(fifoname):
			os.unlink(fifoname)

		os.mkfifo(fifoname)

		fifo_fd = open(fifoname, 'w')

		MAX = int(sys.argv[1])
		gen = fibonacciIterator(MAX)

		for i in gen:
			print(i)
			print(i, file=fifo_fd)

	except IndexError:
		sys.exit('Need at least 1 parameter')

if "__main__" == __name__:
	main()
