import os
import sys

def main():
	try:
		shift = sys.argv[1]
		message = sys.argv[2]

	except IndexError:
		sys.exit("Missing parameter")

	except:
		sys.exit("Incorrect type")

	fifoname = '/tmp/myFIFO'

	fifo_fd = open(fifoname, 'w')

	fifo_fd.write(shift + '\n')
	fifo_fd.write(message)

if "__main__" == __name__:
    main()

