import sys
import os

def main():
	try:
		MAX = int(sys.argv[1])

		fifoname = '/tmp/myFIFO'
		counter = 0

		with open(fifoname,'r') as f:
			for i in f:
				print(i,end='')
				counter += 1
				if counter == MAX:
					break

	except IndexError:
		sys.exit('Need at least 1 parameter')

if '__main__' == __name__:
	main()
