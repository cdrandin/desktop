import time
#import sys
import math

def main():
    #if want to take take argument from terminal for date.  
    #Format %day %month %year, ex.) 12 Aug 12; for 12th day of August in the year 2012
    #if len(sys.argv) == 4
    #    date = sys.argv[1:]
    #    date = date.split()
    #    future_day = time.strptime(date[0]+' '+date[1]+' '+date[2][2:], "%d %b %y")


    #get today's date with specific parameters
    today = time.localtime(time.time())

    #.yday for year of the day
    #reminder:  do difference of days, week, hours, min

    #current_day  = time.strptime(today[0]+' '+today[1]+' '+today[2][2:],"%d %b %y")
    election_day = time.strptime("06 Nov 12", "%d %b %y")  

    day_dif  = ((election_day.tm_yday - today.tm_yday) % 7) -1 #exclude counting today
    week_dif = math.floor((election_day.tm_yday - today.tm_yday) / 7)

    #handle to not count today 
    if day_dif < 0:
        sys.exit("Election day is today!")
    elif day_dif >= 0:
        pass
    else:
        sys.exit("You missed the election!")

    print("Election day in {} weeks, {} days.\nSo go and vote!".format(week_dif,day_dif))

if "__main__" == __name__:
    main()

