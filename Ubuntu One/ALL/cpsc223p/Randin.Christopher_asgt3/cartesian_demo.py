#!/usr/bin/env python3.2
#Christopher Randin
#CPSC 223
#Assignment 3
#Problem 1

import math

#Two-dimensional Cartesian coordinate
class Cartesian2D:
	def __init__(self,x,y):
		self.x = x
		self.y = y

	#Using the distance formula between the two points. Return value.
	def distanceTo(self,vector):
		return math.sqrt((vector.x - self.x)**2 + (vector.y - self.y)**2)
	
	#the length of the point from the origin
	def length(self):
		return math.sqrt(self.x**2 + self.y**2)

	#normalize the point. x/|A|  x is the point and |A| is the magnitude from the point
	def normalize(self):
		return Cartesian2D(self.x/self.length(),self.y/self.length())

	#Overloading '+' to add the vertices and return the new points
	def __add__(self,other):
		return Cartesian2D(self.x + other.x, self.y + other.y)

	#Overloading '-' to subtract the vertices and return the new points
	def __sub__(self,other):
		return Cartesian2D(self.x - other.x, self.y - other.y)

	#Overloading '*' to multiply the vertice with the given value and return the new points
	def __mul__(self,other):
		return Cartesian2D(self.x * other, self.y * other)

#returns the dot product of 2 vertices
def dot(v1, v2):
	return (v1.x * v2.x + v1.y * v2.y)


def main():
	a = Cartesian2D(2.3, 3.4)
	b = Cartesian2D(4.5, 1.8)
	c = Cartesian2D(8.1, 0.3)
	print("The distance from a to b is {}".format(a.distanceTo(b)))
	print("The distance from b to c is {}".format(b.distanceTo(c)))
	d = a + b
	print("a + b = ({},{})".format(d.x, d.y))
	d = c - b
	print("c - b = ({}, {})".format(d.x, d.y))
	print("The length of a is {}".format(a.length()))
	print("The length of b is {}".format(b.length()))
	print("The length of c is {}".format(c.length()))
	unita = a.normalize()
	unitb = b.normalize()
	unitc = c.normalize()
	print("The length of unit a is {}".format(unita.length()))
	print("The length of unit b is {}".format(unitb.length()))
	print("The length of unit c is {}".format(unitc.length()))
	s = 4
	d = unita * s
	print(d)
	print("The length of d is {}".format(d.length()))
	e = unitb * s
	f = dot(a, b)
	g = dot(unita, unitb)
	h = dot(d, e)
	print("dot(a, b) = {}".format(f))
	print("dot(unita, unitb = {}".format(g))
	print("dot(d, e) = {}".format(h))

if __name__ == "__main__":
  main( )
