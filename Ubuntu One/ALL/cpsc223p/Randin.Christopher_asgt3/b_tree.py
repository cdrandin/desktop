class node:
	def __init__(self, data):
		# initializes the data members
		self.left = None
		self.right = None
		self.data = data

class BT:
	def __init__(self):
		# initializes the root member
		self.root = None
    
	def addNode(self, data):
		# creates a new node and returns it
		return node(data)

	def insert(self, root, data):
		# inserts a new data
		if root == None:
			# it there isn't any data
			# adds it and returns
			return self.addNode(data)
		else:
			# if data is smaller than the current node value. Move to the left
			#LEFT SIDE
			if len(data) < len(root.data):
				root.left = self.insert(root.left, data)
			# if data is larger than the current node value. Move to the right
			#RIGHT SIDE
			else:
				# processes the right-sub-tree
				root.right = self.insert(root.right, data)
			return root
		
	def shortWord(self, root):
		#go to the furthest left node, which contains the smallest word then return it
		while(root.left != None):
			root = root.left

		return root.data

	def longWord(self, root):
		#go to the furthest right node, whcih contains the largest word then return it
		while(root.right != None):
			root = root.right

		return root.data

	def display(self, root):
		if root == None:
			pass
		else:
			self.display(root.left)
			print(root.data,end=' ')
			self.display(root.right)
			
