#!/usr/bin/env python3.2

#Christopher Randin
#CPSC 223
#Assignment 3
#Problem 2

import math

# Quaternions class definition here
class Quaternion:
	# define a + b i + c j + d k
	def __init__(self,a,b,c,d):
		self.a = a
		self.b = b
		self.c = c
		self.d = d
		
	#compute the conjucate of the Quaternion
	def conjugate(self):
		return (self.a,-self.b,-self.c,-self.d)

	#compute the norm of the Quaternion
	def norm(self):
		print(math.sqrt((self.a**2) + (self.b**2) + (self.c**2) + (self.d**2)))

	#compute the angle of the Quaternion
	def angle(self):
		w = self.a
		theta = 2*math.acos(w-math.floor(w))
		w = math.cos(theta/2)

		if math.fabs(w) < 1: 
			theta = 2*math.acos(w)

		elif math.fabs(w) == 1.0: 
			theta = 0
		
		return(theta)

	#compute the axis of the Quaternion
	def axis(self):
		w = self.a
		theta = 2*math.acos(w-math.floor(w))
		w = math.cos(theta/2)

		if math.fabs(w) < 1: 
			axis_u = (self.b/math.sqrt(1-(w**2)), self.c/math.sqrt(1-(w**2)), self.d/math.sqrt(1-(w**2)))
		elif math.fabs(w) == 1.0: 
			axis_u = (self.b, self.c, self.d)
		
		return(axis_u)

	#overloaded operators

	def __add__(self,other):
		return Quaternion(self.a + other.a, self.b + other.b, self.c + other.c, self.d + other.d)

	def __sub__(self,other):
		return Quaternion(self.a - other.a, self.b - other.b, self.c - other.c, self.d - other.d)

	def __mul__(self,other):
		return Quaternion((self.a*other.a)-(self.b*other.b)-(self.c*other.c)-(self.d*other.d),	#* 1
		(self.b*other.a)+(self.a*other.b)+(self.c*other.d)-(self.d*other.c),					#* i	  
		(self.a*other.c)-(self.b*other.d)+(self.c*other.a)+(self.d*other.b),					#* j
		(self.a*other.d)+(self.b*other.c)-(self.c*other.b)+(self.d*other.a))					#* k

def main():
	# Quaternion a & b are defined by four constants, which
	# define a + b i + c j + d k where a...d are parameter 1...4 of
	# the constructor
	a = Quaternion(.3, 3.2, 6.5, 1.1)
	b = Quaternion(.3, 4.7, 7.6, 2.2)
	c = a + b
	print(c)
	c = a - b
	print(c)
	print(a.conjugate())
	print(b.conjugate())
	print(c.conjugate())
	print(a*b)
	print(b*a)
	print(a*c)
	print(c*a)
	print("a's angle {} rad and axis {}".format(a.angle(), a.axis()))
	print("b's angle {} rad and axis {}".format(b.angle(), b.axis()))
	print("c's angle {} radand axis {}".format(c.angle(), c.axis()))
	a.norm() 
	b.norm() 
	c.norm() 
	print("a's angle {} rad and axis {}".format(a.angle(), a.axis()))
	print("b's angle {} rad and axis {}".format(b.angle(), b.axis()))
	print("c's angle {} rad and axis {}".format(c.angle(), c.axis()))
if __name__ == "__main__":
	main()
