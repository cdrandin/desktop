#!/usr/bin/env python3.2

#Christopher Randin
#CPSC 223
#Assignment 3
#Problem 3

import sys
from b_tree import *

def main():
	#handle exception if a string isn't entered at command line
	#if error. Exit program.
	try:
		infile = open(sys.argv[1],'r')	
		outfile = open(sys.argv[1][:-4] + "-report.txt", 'w')

	except IndexError:
		print("No string entered from command line.\nCommand format: ./word_length.py [file name].*")
		exit(1)

	#firstTime used to force creating a first instance of the root for the BT
	#when first root is created this turns false not allowing for the creation of multiples roots.
	#BTree creates the tree
	#counter is a dictionary to keep track of the words put into the binary tree
	firstTime = True
	BTree = BT()
	counter = {}

	#read each word from the file. Seperating spaces, end characters. 
	#put words into a binary tree in order of the length of the word
	#~~Assuming there are no conjunctions~~
	for i in infile:
		#dump the first line read into temp which is a list
		temp = i.split()
		
		#traverse the list
		for j in temp:		
			#remove unwanted special characters	
			j = j.strip('.,;\'~!@#$%^&*()_+')

			# make all to lower keep track of words, excluding uppercase
			j = j.lower()

			#if this is the first root creating a new tree
			if firstTime:
				ROOT = BTree.addNode(j)
				firstTime = False
				counter[j] = 1

			#if BT already exist
			else:
				BTree.insert(ROOT,j)

				#keep track of duplicate words, count them.
				if(j in counter):
					counter[j] += 1
				#if new word entered into binary tree, add to counter, which is a dictionary
				else:
					counter[j] = 1
	
	#write to file. Writing shortest word, longest word, and a histogram of the 20 common words from the file.
	outfile.write("Shortest word:\n")
	outfile.write(BTree.shortWord(ROOT))
	outfile.write("\nLongest word:\n")
	outfile.write(BTree.longWord(ROOT))
	outfile.write('\n')
	outfile.write('\nTop 20 common words\n')

	#contains most common words from the input file starting with most used ending with least used
	top20words = sorted(counter, key=lambda i: counter[i], reverse=True)

	#If there are more then 20 common words. Cut only the first 20.
	if len(top20words)>20:
		top20words = top20words[:20]

	#write to file the top 20 cmost used words
	for i in top20words:
		#special format {0} is first parameter {0:<14} is first parameter filling in 14 spaces to the left '<' *directional*
		outfile.write("{0:<14}  ({1}) ".format(i, counter[i]) + ' ') 
		j = 0

		#create a histogram with '*' symbol representation
		while(j < counter[i]):
			outfile.write('*')
			j+=1

		outfile.write('\n')

	infile.close()
	outfile.close()

if __name__ == "__main__":
	main()

