from  xml.dom.minidom import parseString
import urllib.request
import re
import os
from shutil import move

def main():
	CWD  = os.getcwd() + '/'

	url = 'http://www.nasa.gov/rss/lg_image_of_the_day.rss'
	webpage = urllib.request.urlopen(url)
	urlsrc = webpage.read().decode('utf-8')
	webpage.close()
	
	dom = parseString(urlsrc)

	tag = dom.getElementsByTagName('enclosure')[0].toxml()

	m = re.search('http://[A-z/.]*([0-9]+[A-z_]+.jpg)',tag)

	imgurl = m.group(0)
	imgname = m.group(1)

	img = urllib.request.urlopen(imgurl)
	imgsrc = img.read()
	img.close()

	f = open(imgname, 'wb')
	f.write(imgsrc)
	move(cwd+imgname,HOME+ 'cpsc223p/Randin.Christopher_asgt8/') #move to specific file. could place into img only file
	f.close()

	
if "__main__" == __name__:
	main()
