import urllib.request
import sys

def main():
	if len(sys.argv) != 3:
		sys.exit("Missing arguments")
	try:
		URL = sys.argv[1]
	  FileName = sys.argv[2]
	except IndexError:
		sys.exit("Missing arguments")

	try:
		URLFile = urllib.request.urlopen(URL)
	except urllib.error.URLError:
		sys.exit('{} is not an existing URL'.format(URL))
		
	with open(FileName,'w') as f:
		for i in URLFile:
			f.write(i.decode("utf-8"))
			
		print("URL HTTP code in {}".format(FileName))
		URLFile.close()
	
if "__main__" == __name__:
	main()
