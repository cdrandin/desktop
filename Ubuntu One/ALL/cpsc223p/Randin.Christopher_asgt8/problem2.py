import sys
import smtplib
import getopt

def encode(code_word, filename):

	f = open(filename, 'r')

	#finds the length of the code word
	length=len(code_word)

	#reads the file
	message = f.read()

	#starts pointer at 0 use the code word one letter at a time
	pointer=0
	
	msg=""

	
	#for every letter in the message cipher it and write it to the file
	for i in message:
		if not i.isalpha():
			msg += i
			pointer -= 1

		elif i.isupper():
			temp = ord(code_word[pointer])-97

			if (ord(i) + temp) > 90:
				msg += (chr((ord(i)+temp)-26))
			else:
				msg += (chr(ord(i)+temp))
		else:
			temp = ord(code_word[pointer])-97

			if (ord(i) + temp) > 122:
				msg += (chr((ord(i)+temp)-26))
			else:
				msg += (chr(ord(i)+temp))
		pointer += 1
		if pointer == length:
			pointer = 0

	return msg

def main():
	try:
		opts, args = getopt.getopt(sys.argv[1:], "w:f:a:")
		
	except getopt.GetoptError as err:
		print (str(err)) # will print something like "option -a not recognized"
		exit(1)
		
	for o,a in opts:
		if o == '-w':
			code_word = a
		elif o == '-f':
			fileName = a
		elif o == '-a':
			addr = a

	msg = 'Subject: Encoded message\n'
	msg = encode(code_word,fileName)

	fromaddr = 'cdrandin'
	toaddrs  = addr


	server = smtplib.SMTP('esu.fullerton.edu')
	server.set_debuglevel(1)
	server.sendmail(fromaddr, toaddrs, msg)
	server.quit()

if __name__ == "__main__":
	main( )
