import os
import sys

def main():	
	try:
		#http://docs.python.org/py3k/library/getopt.html
		path = sys.argv[1]
	except:
		sys.exit("Error no path given")

	if os.path.isfile(path) or os.path.isdir(path):
		for (dirpath, dirnames, filenames) in os.walk(path):
			print(dirpath)
			for i in filenames:
				print('   {} ===>  Stat: {}'.format(i, os.stat(dirpath + '/' + i)), end = '\n\n')
	else:
		sys.exit("File path does not exist")

if __name__ == "__main__":
	main()
