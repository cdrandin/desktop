#!/usr/bin/env python3.2
'''
Christopher Randin
CPSC 223P Assignment 1 Week 6
Due: 10/25

Problem #1
'''
import math
import sys
import getopt

#process BMI and BMR using assigned functions
#output: BMI and BMR in that order
def Process_BMI_BMR(weight,height,age,gender):
	BMI= (weight/(height * height))

	height_in_cm= height * 100

	if (gender=='M' or gender=='m'):
		BMR= ((weight * 13.397) + (height_in_cm * 4.799) - (age * 5.667) + 88.362)

	elif (gender=='F' or gender=='f'):
		BMR= ((weight * 9.247) + (height_in_cm * 3.098) - (age * 4.330) + 447.593)

	else :
		print("Gender Error!")
		exit(1)

	return BMI,BMR

#function to determine the severity of the patient's current BMI range
#output: prompt displaying severity of BMI, whether to lose or gain, upper and lower bounds
def BMI_TABLE(BMI):
	if BMI < 16.00:
		severity= "Severe Thinness"
		maintain_weight= "gain"
		upper= 16.00
		lower= None

	elif BMI < 16.99:
		severity= "Moderate Thinness"
		maintain_weight= "gain"
		upper= 16.99
		lower= 16.00

	elif BMI < 18.49:
		severity= "Mild Thinness"
		maintain_weight= "gain"
		upper= 18.49
		lower= 17.00

	elif BMI < 24.99:
		severity= "Normal Range"
		maintain_weight= "normal"
		upper= 24.99
		lower= 18.50

	elif BMI < 29.99:
		severity= "Overweight"
		maintain_weight= "lose"
		upper= 30.00
		lower= 25.00

	elif BMI < 34.99:
		severity= "Obese Class I (Moderate)"
		maintain_weight= "lose"
		upper= 35.00
		lower= 30.00

	elif BMI < 39.99:
		severity= "Obese Class II(Severe)"
		maintain_weight= "lose"
		upper= 39.99
		lower= 35.00

	else:
		severity= "Obese Class III(Very Severe)"
		maintain_weight= "lose"
		upper= None
		lower= 39.99

	return severity, maintain_weight, upper, lower

#store patients info regarding to calculate BMI and BMR
patient_info = {}

try:
	opts, args = getopt.getopt(sys.argv[1:], 'x m f a:h:w:')
except getopt.GetoptError as msg:
	print (msg)
	sys.exit(1)

metric = False 
for o, a in opts:
	if o == '-x':
		metric = True
	elif o == '-m':
		patient_info['gender'] = 'M'
	elif o == '-f':
		patient_info['gender'] = 'F'
	elif o == '-a':
		patient_info['age']    = int(a)
	elif o == '-h':
		patient_info['height'] = float(a)
	elif o == '-w':
		patient_info['weight'] = float(a)

if not metric:
	patient_info['weight'] *= 0.453592 #now in kg
	patient_info['height'] *= 0.01 #now in m


#units currently in meteric
#pass through functions.
#returned BMI, BMI, and severity of BMI(BMI_Status)
patient_info['BMI'], patient_info['BMR']= Process_BMI_BMR(patient_info['weight'],patient_info['height'],patient_info['age'],patient_info['gender'])

#upper_BMI and lower_BMI used to determine the boundaries of the BMI category.
#if upper_BMI is none, meaning there is no catergory above the current location(meaning severe obese)
#if lower_BMI is none, meaning there is no catergory below the current location(meaning severe Thinness)
patient_info['BMI_Status'], weight_management, upper_BMI, lower_BMI= BMI_TABLE(patient_info['BMI'])

#display patient's BMI and BMI along with their current position in correlation with the BMI table
print ("Your BMI is %.2f. Category: %s" %(patient_info['BMI'],patient_info['BMI_Status']))
print ("Your BMR is %.2f " %(round(patient_info['BMR'],2)))
print()

#find the neccessary weight to achieve the above and lower BMI scales
weight_upper= (upper_BMI * (patient_info['height'] * patient_info['height']))
weight_lower= (lower_BMI * (patient_info['height'] * patient_info['height']))

#calculate the difference in weight as to how far they are from the BMI scales
upper_weight_needed = math.fabs(weight_upper - patient_info['weight'])
lower_weight_needed = math.fabs(weight_lower - patient_info['weight'])


#divide by the 7 days in a week for 1 day conversion:
#~(((7709 kcal * weight_needed)/ 52)/7) / day

#this is used ot calculate the extra calories needed to gain or lose weight to next BMI category
#measurements of kcal/day
cal_gain= (((7709 * upper_weight_needed)/ 52)/7) 
cal_lose= (((7709 * lower_weight_needed)/ 52)/7) 

#using the bounds to prompt user the possible routes of gain or losing weights into the next possible category.
#if the bounds are 'none' then there is no next possible category
#return cal gain/lose in kcal / day
if upper_BMI == None:
	print("You only choice in the next category is below, meaning you must lose weight")
	print("You need to maintain a %.02f calories per day over the next 52 weeks to lose enough weight to drop to the next category" %(patient_info['BMR'] -cal_lose))
elif lower_BMI == None:
	print("You only choice in the next category is above, meaning you must gain weight")
	print()
	print("You need to maintain a %.02f calories per day over the next 52 weeks to lose enough weight to drop to the next category" %(patient_info['BMR'] + cal_gain))
else:
	print("If you want to go to the above category in the BMI chart you need to maintain a %.02f calories per day for the next 52 weeks" %(patient_info['BMR'] + cal_gain))
	print()
	print("If you want to go to the below category in the BMI chart you need to maintain a %.02f calories per day for the next 52 weeks" %(patient_info['BMR'] - cal_lose))

