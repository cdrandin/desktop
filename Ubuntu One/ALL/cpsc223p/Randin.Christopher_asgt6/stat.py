import os
import sys

def main():
    try:
        file = sys.argv[1]
        fileName = open(file,'r')

    except IndexError:
        sys.exit("Error: No file name given!")

    except IOError:
        sys.exit("Error: File does no exist!")

    stat = os.stat(file)


if __name__ == "__main__":
    main()
