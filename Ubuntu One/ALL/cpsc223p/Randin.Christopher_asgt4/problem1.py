#!/usr/bin/env python3.2

#Christopher Randin
#CPSC 223
#Assignment 4
#Problem 1

import sys
from binary_tree import *

def main():
	#handle exception if a string isn't entered at command line
	#if error. Exit program.
	try:
		infile = open(sys.argv[1],'r')	
		outname = 'filename.txt' #sys.argv[1][:-4] + "-report.txt"
		outfile = open(outname, 'w')

	except IndexError:
		print("Missing file name.\nCommand format: ./problem1.py words.txt")
		exit(1)


	#when first root is created this turns false not allowing for the creation of multiples roots.
	#BTree creates the tree
	#counter is a dictionary to keep track of the words put into the binary tree
	BTree = Tree()
	counter = {}

	#read each word from the file. Seperating spaces, end characters. 
	#put words into a binary tree in order of the length of the word
	#~~Assuming there are no conjunctions~~
	for i in infile:
		#dump the first line read into temp which is a list
		temp = i.split()
		
		#traverse the list
		for j in temp:		
			#remove unwanted special characters	
			j = j.strip('.,;\'\"~!@#$%^&*()_+')

			# make all to lower keep track of words, excluding uppercase
			j = j.lower()

			#if this is the first root creating a new tree
			BTree.insert(j)

			#keep track of duplicate words, count them.
			if(j in counter):
				counter[j] += 1

			#if new word entered into binary tree, add to counter, which is a dictionary
			else:
				counter[j] = 1
	
	#write to file. Writing shortest word, longest word, most frequent word, least frequent word
	#and a histogram of the words from the file.
	shortWord, longWord = BTree.short_long_word()

	outfile.write("Shortest word:\n")
	outfile.write(shortWord)

	outfile.write("\n\nLongest word:\n")
	outfile.write(longWord)

	#contains words from the input file starting with most used ending with least used
	#counter - iterator
	#key - is the key to correspond with my dictionary displaying the value of that key
	#reverse - set to 'True' reverses the sorting from smallest to largest --> largest to smallest
	histogram_words = sorted(counter, key=lambda i: counter[i], reverse=True)

	mostFreqWord = histogram_words[0]
	leastFreqWord = histogram_words[len(histogram_words)-1]

	outfile.write("\n\nMost Frequent word:\n")
	outfile.write(mostFreqWord)

	outfile.write("\n\nLeast Frequent word:\n")
	outfile.write(leastFreqWord)

	outfile.write('\n\nTop 20 common words:\n\n')

	#write to file the top 20 cmost used words
	for i in histogram_words:
		#special format {0} is first parameter {0:<16} is first parameter filling in 16 spaces to the left '<' *directional*
		outfile.write("{0:<16}  ({1}) ".format(i, counter[i]) + ' ') 
		j = 0

		#create a histogram with '*' symbol representation
		while(j < counter[i]):
			outfile.write('*')
			j+=1

		outfile.write('\n')

	infile.close()
	outfile.close()

	print("Output is in {}".format(outname))



if __name__ == "__main__":
	main()

