#!/usr/bin/env python3.2

#Christopher Randin
#CPSC 223
#Assignment 4
#Problem 2

import sys
import random
from int_binary_tree import *

def writeTree(tree, infile):
	infile.write('digraph BST{\n')
	infile.write('\tnode [fontname="Helvetica"];\n')
	tree.dotDisplay(infile)


def main( ):
	if len(sys.argv) < 2:
		print('Please provide the number of keys to enter.')
		sys.exit(1)
	s = int(sys.argv[1])
	parts = int(s/3)
	t = Tree( )
	r = list(range(1,s+1))

	print('Randomly inserting the numbers from 1 to {}.'.format(len(r)))

	random.shuffle(r)

	for i in r:
		print('inserted {}'.format(i))
		t.insert(i)

	f = open('a.dot', 'w')
	#writeTree(t, f)
	f.flush( )
	f.close( )
	random.shuffle(r)

	print('\nBefore deletion:')
	t.display()
	print('~~~~~~~~~~~~~~~~~~~~~~~~~')
	for n in range(1, 3):
		m = r[(n-1) * parts : (n * parts)]
		print(len(m))
		for i in m:
			print('removed {}'.format(i))
			v = t.remove(i)
			if v:
				print('\tcompleted.')
			else:
				print('\terror.')

	print('\nAfter deletion:')
	t.display()
	print()
	c = chr(n + 97)
	filename = str(c) + '.dot'
	f = open(filename, 'w')
	#writeTree(t, f)
	f.flush( )
	f.close( )
	print('\nDidn\'t display these binary trees in the DOT syntax')
	
if __name__ == '__main__':
	main()
