class TreeNode:
	__slots__ = {'_left', '_right', '_parent', '_data'}
	
	def __init__(self, node, data):
		self._left = None
		self._right = None
		self._parent = node	
		self._data = data		
	
####################################################		
	def getData(self):
		return (self._data)
		
	def setData(self, data):
		self._data = data

	def delData(self, data):
		del self._data

	data=property(getData, setData, delData, "doc")
####################################################				
	def getParent(self):
		return (self._parent)
		
	def setParent(self, data):
		self._parent = data

	def delParent(self, data):
		del self._data

	parent=property(getParent, setParent, delParent, "doc")
####################################################				
	def getRight(self):
		return (self._right)
		
	def setRight(self, data):
		self._right = data

	def delRight(self, data):
		del self._data

	right=property(getRight, setRight, delRight, "doc")
####################################################				
	def getLeft(self):
		return (self._left)
		
	def setLeft(self, data):
		self._left = data

	def delLeft(self, data):
		del self._data

	left=property(getLeft, setLeft, delLeft, "doc")
####################################################		

class Tree:
	__slots__ = ['_root']

	#CONSTRUCTOR
	#creates root
	def __init__(self):
		self._root = TreeNode(None, None)



	#INSERT
	#create new node to be branched off
	def insert(self, data):
		self.insertNode(self._root, data)
		
	#add data into correct node location
	def insertNode(self, node, data):
		#if root is None
		if node.data == None:
			node.data = data

		#if entered data is smaller than the current node location
		#LEFT SIDE
		elif len(data) < len(node.data):

			#if there is a leaf node and a new node is needed to add to tree
			if node.left == None:
				node.left = TreeNode(node,data)

			#if node exist add new node to right child
			else:
				self.insertNode(node.left, data)

		#if entered data is larger than the current node location
		#RIGHT SIDE
		else:

			#if there is a leaf node and a new node is needed to add to tree
			if node.right == None:
				node.right = TreeNode(node, data)

			#if node exist add new node to right child
			else:
				self.insertNode(node.right, data)



	#DISPLAY
	#pass root to displayNodes
	def display(self):
		self.displayNodes(self._root)
	
	#display nodes inorder
	def displayNodes(self, node):
		#if finished traversing the tree
		if node == None:
			pass

		#display nodes inorder
		else:
			self.displayNodes(node.left)
			print(node.data)
			self.displayNodes(node.right)




	#SMALLEST AND LONGEST WORD
	#call both shortWord and longWord functions.
	#return the strings of the shortest and longest word.
	def short_long_word(self):
		return((self.shortWord(self._root)), (self.longWord(self._root)))

	#Traverse further most left node, which will contain the smallest word. Return data at current node.
	def shortWord(self, node):
		#go to the furthest left node, which contains the smallest word then return it.
		while(node.left != None):
			node = node.left

		return node.data

	#Traverse further most right node, which will contain the largest word. Return data at current node.
	def longWord(self, node):
		#go to the furthest right node, whcih contains the largest word then return it.
		while(node.right != None):
			node = node.right

		return node.data
