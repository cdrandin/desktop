#!/usr/bin/env python3.2

#Christopher Randin
#CPSC 223
#Assignment 4
#Problem 2

import random
from BTree import *

def main( ):
	t = Tree()
	
	#create a list of values between 20 and 40 at random
	rand = random.randint(20,40)
	r = [ x for x in list(range(rand)) ]

	#shuffle the values
	random.shuffle(r)

	print('~After creating a list of numbers between 20 and 40 at random. Then shuffle~\nMax range is',rand-1)
	print(r,end='\n\n')

	print('Inserting the list into tree')
	for i in r:
		print(i,'inserted')
		t.insert(i)

	print()

	print('Inorder traversal for the tree as a container')
	for i in t:
		print(i,end=' ')
	print()
if __name__ == '__main__':
	main()
