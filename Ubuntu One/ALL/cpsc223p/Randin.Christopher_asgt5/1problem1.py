#! usr/bin/env python3.2

#Christpher Randin
#CPSC 223P
#Assignment 5
#Problem 1

#Generator wordIterator
#receives file object as parameter
#search for a word. Read a line. Break the line into a list and strip punctuations.
#yield word - return the word and freezing the current state of the generator
import sys

def wordIterator(infile):
	for line in infile:
		for word in line.strip().split():
			yield word

def main():
	#try to open file and read file using generator
	try:
		with open(sys.argv[1],'r') as infile:
			for word in wordIterator(infile):
				print(word)
	#if no file passed through terminal as perameter. Error.
	except IndexError:
		print('Missing file. Format: ./1problem1.py file.txt')
		exit(1)

if __name__ == '__main__':
	main()
	
