#! usr/bin/env python3.2

#Christpher Randin
#CPSC 223P
#Assignment 5
#Problem 2
import sys
def fibonacciIterator(n):
	a, b = 0, 1
	while a < n:
		yield a
		a, b = b, a+b

def main():
	try:
		print("fibonacci seriescle up to {}".format(sys.argv[1]))
		for i in fibonacciIterator(int(sys.argv[1])):
			print(i,end=' ')
		print()
	except IndexError:
		print("Incorrect value. Enter a integer number.")
		exit(1)

if __name__ == '__main__':
	main()
