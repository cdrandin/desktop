#! usr/bin/env python3.2

#Christpher Randin
#CPSC 223P
#Assignment 5
#Problem 3

def sieveGenerator(number):
	number += 1 #for use of the range function to include the value in number itself
	numbers = [] #store truth values of all given numbers within the range
	prime = [] #store prime numbers

	#place the truth values for all numbers within the range as true
	for i in range(0,number):
		numbers.append(True)

	#process of eliminating composite numbers marking them as false
	#true values is considered prime, then that value will be appended into 
	#a seperate list
	for i in range(2,len(numbers)):
		if numbers[i] == True:
			yield(int(i))
			for j in range(i,len(numbers),i):
				numbers[j]=False

import sys
def main():
	try:
		print("primes up to {}".format(sys.argv[1]))
		for i in sieveGenerator(int(sys.argv[1])):
			print(i,end=' ')
		print()
	except IndexError:
		print("Incorrect value. Enter a integer number.")
		exit(1)
if __name__ == '__main__':
	main()
