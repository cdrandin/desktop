class TreeNode:
	__slots__ = {'_left', '_right', '_parent', '_data','_visited'}
	
	def __init__(self, node, data):
		self._left = None
		self._right = None
		self._parent = node	
		self._data = data		
		self._visited = False
####################################################		
	def getData(self):
		return (self._data)
		
	def setData(self, data):
		self._data = data

	def delData(self, data):
		del self._data

	data=property(getData, setData, delData, "doc")
####################################################				
	def getParent(self):
		return (self._parent)
		
	def setParent(self, data):
		self._parent = data

	def delParent(self, data):
		del self._data

	parent=property(getParent, setParent, delParent, "doc")
####################################################				
	def getRight(self):
		return (self._right)
		
	def setRight(self, data):
		self._right = data

	def delRight(self, data):
		del self._data

	right=property(getRight, setRight, delRight, "doc")
####################################################				
	def getLeft(self):
		return (self._left)
		
	def setLeft(self, data):
		self._left = data

	def delLeft(self, data):
		del self._data

	left=property(getLeft, setLeft, delLeft, "doc")
####################################################			

class Tree:
	__slots__ = ['_root','_current','_lastValue','_visited','_endTree']

	#CONSTRUCTOR
	#creates root
	def __init__(self):
		self._root = TreeNode(None, None)
		self._current = self._root
		self._lastValue = None
		self._endTree = False

	#return iterable object
	def __iter__(self):
		return self
		
	def __next__(self):
		#find last node in the tree for (INORDER) traversal
		self._lastValue = self.short_long_word()[1]

		#if there is no root
		if self._current.data == None:
			print('Empty tree')
			exit(1)

		#traverse the tree till reached the end node for inorder traversal
		while not self._endTree:

			#go to left child
			#if the current node has a only left check that hasn't been visited.  Go to that node.
			if self._current.left != None and self._current.left._visited != True:
				self._current = self._current.left

			#get data
			#if the current node has not been visited, returned value.
			#set visited to TRUE.  If this is the last node set endTree to TRUE, will be used to stop while loop.
			#return current node data
			elif self._current._visited == False:
				self._current._visited = True
				if self._current.data == self._lastValue: self._endTree = True
				return self._current.data 

			#go to right child
			#if the current node has a only right child that hasn't been visited. Go to that node.
			elif self._current.right != None and self._current.right._visited != True:
				self._current = self._current.right

			#if left child exist and has been visited and right child exist and has been visited. GO back up the the parent.
			else:
				self._current = self._current.parent

		#if we have reach the end of the tree. Throw stop iteration since we have reach the end of the container.
		else:
			raise StopIteration()

		
	#INSERT
	#create new node to be branched off
	def insert(self, data):
		self.insertNode(self._root, data)
		
	#add data into correct node location
	def insertNode(self, node, data):
		#if root is None
		if node.data == None:
			node.data = data

		#if entered data is smaller than the current node location
		#LEFT SIDE
		elif (data) < (node.data):

			#if there is a leaf node and a new node is needed to add to tree
			if node.left == None:
				node.left = TreeNode(node,data)

			#if node exist add new node to right child
			else:
				self.insertNode(node.left, data)

		#if entered data is larger than the current node location
		#RIGHT SIDE
		else:

			#if there is a leaf node and a new node is needed to add to tree
			if node.right == None:
				node.right = TreeNode(node, data)

			#if node exist add new node to right child
			else:
				self.insertNode(node.right, data)




	#DISPLAY
	#pass root to displayNodes
	def display(self):
		self.displayNodes(self._root)
	
	#display nodes inorder
	def displayNodes(self, node):
		#if finished traversing the tree
		if node != None:
			self.displayNodes(node.left)
			print(node.data)
			self.displayNodes(node.right)




	#SMALLEST AND LONGEST WORD
	#call both shortWord and longWord functions.
	#return the strings of the shortest and longest word.
	def short_long_word(self):
		return((self.shortWord(self._root)), (self.longWord(self._root)))

	#Traverse further most left node, which will contain the smallest word. Return data at current node.
	def shortWord(self, node):
		#go to the furthest left node, which contains the smallest word then return it.
		while(node.left != None):
			node = node.left

		return node.data

	#Traverse further most right node, which will contain the largest word. Return data at current node.
	def longWord(self, node):
		#go to the furthest right node, whcih contains the largest word then return it.
		while(node.right != None):
			node = node.right

		return node.data




	def search(self, node, value):
		if node._data == value:
			return node

		else:
			if value < node.data:
				return self.search(node.left,value)

			else:
				return self.search(node.right,value)


	def remove(self,value):
		return not self._deleteNode(self.search(self._root,value))

	def _deleteNode(self, n):
		if n.left == None:
			self._transplant(n, n.right)

		elif n.right == None:
			self._transplant(n, n.left)

		else:
			y = self._localMinimum(n.right)

			if y.parent != n:
				self._transplant(y, y.right)
				y.right = n.right
				y.right.parent = y
	
			self._transplant(n, y)
			y.left = n.left
			y.left.parent = y
		
		del n

	def _localMinimum(self, node):
		while node.left != None:
			node = node.left

		return node

	def _transplant(self, u, v):
		if u.parent == None:
			self._root = v

		elif u == u.parent.left:
			u.parent.left = v

		else:
			u.parent.right = v

		if v != None:
			v.parent = u.parent
