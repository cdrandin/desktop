def print_to_file(file, data):
  with open(file, 'w') as outfile:
   for row, columns in data.items():
    NULL = '.'
    
    if row in (4,7): 
      outfile.write('+'.join(['-'*3*2]*3))
      outfile.write('\n')
      
    for col, value in columns.items():
      if col in (4,7): outfile.write('|')
      if len(value) >2:
        outfile.write(NULL+' ')
      else:
        outfile.write(value+' ')
    outfile.write('\n')
    
def print_to_screen(data, row):
  width = 3
  line = '+'.join(['-'*width*3]*3)
  print('   1  2  3   4  5  6   7  8  9')####
  print('   ---------------------------')####
  for rs in row:
    for r in rs:
      if r[1] == '1': print(r[0],end=' ')####
      
      if type(data[r]) == type(set()):
        print(''.join('.'.center(width)+''),end='')
      else:
        print(''.join(data[r].center(width)+''),end='')
        
      if '3' in r or '6' in r: print('|', end='')
      if r in ('C9','F9'): print('\n ',line,end='')
    print()
      
    
  #for key, item in sorted(data.items()):
  #  print(key, ': ', item)

#if file is 9 lines by 9 characters long, reformat into one long string
def reformat(data):
  info = ''

  for i in data:
    if i == '\n':
      pass
    else:
      info += i
      
  return (info)
      
#Check if sudoku input is correct, if so return true, else false.
def check_if_valid(data):
  import re

  #return re eexpression matches from our file. 
  m = re.search('((\.)|([1-9]))+', data)

  #Grammar is ok
  if m.group(0) == data:
    return True

  return False