import sys
import os
from sudoku_board import Board
from sudoku import check_if_valid, reformat, print_to_screen, print_to_file

def display_values( values ):
  print('~~~~~~~~~~~~~~~~~~~~~~~~~~~')
  for key, value in sorted(values.items()):
    print(key,': ',value)
  print('~~~~~~~~~~~~~~~~~~~~~~~~~~~') 
  
def main():
  if len(sys.argv) != 2: sys.exit("Enter file or directory")
  
  CWD  = os.getcwd() +'/' #current directory
  path = os.path.join(CWD, sys.argv[1]) #path is now the full path to the directory or file entered
  
  #if directory exist, set path to expected fileName
  if os.path.isdir(path):
    fileName = path
    OUT_DIR = os.path.join(CWD, 'Solved Puzzles')
    if not os.path.isdir(OUT_DIR):
      os.mkdir('Solved Puzzles')
    
    
  elif os.path.isfile(path):
    fileName = path
    print('file')
    
  else:
    sys.exit('file error')
    
  exit(1)
  with open(fileName,'r') as infile:
    data = ''
    
    for i in infile.read().strip('\n'):
      data += i

  #if sudoku file is 9x9. Convert to 81 character line puzzle
  if (data.count('\n')) > 1:
    data = reformat(data)
  
  if not check_if_valid(data):
    sys.exit("Format for the sudoku file is incorrect!")
    exit(1)

  board = Board(data)

  #print_to_file(outfile, board._box)
  
  solved = board.solve()
  
  display_values(board._values)
  
  #if solved: print("Solved")
  #else:      print("Not solved")
  if board.is_solved(): print("SOLVED")
  else:                 print("NOT SOLVED")
  
  print_to_screen(board._values, board._row)
  
if "__main__" == __name__:
  main()

