class Board:
  __slots__ = ['squares', '_col', '_row', '_box', '_values', '_table', '_unit_list', 
	       '_unit', '_LoS_Unit']
  
  def __init__(self, data):
    #quick constant used to prepare table indicies
    sets = '123456789'
    row  = 'ABCDEFGHI'
    col  = sets
    
    #function of quick cross product
    cross_prod = lambda x, y: [i+j for i in x for j in y]
    
    #list label 9x9 table as A-I rows and 1-9 columns
    #top left A1, top right A9, bottom left I1, bottom right I9
    self.squares = cross_prod(row, col)
    
    #constraints of our board
    self._col     = [cross_prod(row, i) for i in col] #LoS in a given sub columns
    self._row     = [cross_prod(i , col) for i in row] #Los in a given sub rows
    self._box     = [cross_prod(rows, cols) for rows in ("ABC","DEF","GHI") for cols in ("123","456","789")]
    
    #contains all areas of LoS at at any given point, but not specified yet
    self._unit_list = (self._col + self._row + self._box)
    
    #given a specfic indicies this will return the corresponding LoS values to be focused on
    #row, column and the current box
    self._unit = dict((index, [unit for unit in self._unit_list if index in unit]) for index in self.squares)
    
    #squares that are shared with a unit
    #track of a specfic cell's common col,row, box uniqueness
    self._LoS_Unit  = dict((index, set(sum(self._unit[index],[])) - set([index])) for index in self.squares )

    #Possibilities of each cell, either contains a number or a set of numbers 
    self._values  = dict((index, set(sets)) for index in self.squares)
    
    #direct copy of sudoku format with real values
    self._table   = dict(zip(self.squares, [char for char in data if char in sets or char in '.']))
    
  #store real values from table into self._values, which is storing our real values and possibilities
  def store(self):
    for key, value in self._table.items():
      if value != '.':
        self._values[key] = value
        self.update(key, value)

        
  #when a value is assigned, update LoS vision of that unit(col, row, box) and 
  #   remove value from the set
  def update(self, key, value):
    for i in self._LoS_Unit[key]:
      if isinstance(self._values[i],set) and value in self._values[i]:
        #when updating don't overwrite values
        if len(self._values[i]) != 1:
          self._values[i] -= set(value)
      
  ####
  ####
  ####
  #find simple possibilities of single possibility and set cell to the single value 
  #   and update neightbours
  def find_possibles(self):
    for key, value in self._values.items():
      if isinstance(value, set) and len(value)==1:
        #used to check if last value is in our possibilities
        temp = tuple(value)[0] #use tuple to extract single value from set

        #1 possibility is now assigned as a true value
        self._values[key] = temp

        #search through each LoS Unit at the given unit and remove the possibility 
        #   given by the true value
        for u in self._LoS_Unit[key]:
          if temp in self._values[u]:
            self._values[u] -= set(temp)
            self.find_possibles()

        self.find_singles()
          
  #get hidden singles
  def find_singles(self):####
    
    for key in sorted(self._values.keys()):
      count = dict((str(i), [0,'']) for i in range(1,10,1))

      #check unit box to find hidden singles
      for unit in sorted(self._box[ord(tuple(key)[0]) - ord('A')]):
        for i in self._values[unit]:
          if isinstance(self._values[unit],set):
            count[i][0] += 1
            count[i][1] = unit

      for key, value in (sorted(count.items())):####
        if value[0] == 1:
          self._values[value[1]] = key####
          self.update(value[1], key)####
          self.find_singles()####

  ####
  ####
  ####
  
  
  #SOLVED == all(not isinstance(i, str)
  def solve(self):
    self.store()
      
  def is_solved(self): return all(not isinstance(i, str) for i in self._values)