import socket
import sys
import pygame
from pygame.locals import *

HOST, PORT = "127.0.0.2", 9999

# SOCK_DGRAM is the socket type to use for UDP sockets
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

size = width, height = 320, 240 #set window size
pygame.init() #Initalize pygame
screen = pygame.display.set_mode(size) #create window

while True:

    #Handle input
    for event in pygame.event.get():
        #Quit if 'x' on corner of window is clicked or esc is pressed
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            #tell server to shutdown by sending kill signal
            sock.sendto(bytes('kill', "utf-8"), (HOST, PORT))
            pygame.quit()
            exit(1)

        #Any key is pressed
        elif event.type == KEYDOWN:
            # As you can see, there is no connect() call; UDP has no connections.
            # Instead, data is directly sent to the recipient via sendto().
            sock.sendto(bytes(event.unicode, "utf-8"), (HOST, PORT))
            print("Sent:     {} TO:     {}".format(event.unicode,HOST))
            
            #Accept messages from server if any
            try: 
                received,addr = sock.recvfrom(1024)
                print("Received:     {} FROM:     {}".format(received,addr))

            except:
                pass


