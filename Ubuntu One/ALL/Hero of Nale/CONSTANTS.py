global FPSCLOCK,SURFACE,SCREEN,L_HERO_IMG,R_HERO_IMG,playerObj,HEART_IMG

FPS = 30 #frames per second to update onto screen
WIDTH = 800 #width of the program's window, pixels
HEIGHT = 600 #height of the program's window, pixels

HALF_WIDTH = int((WIDTH)/2)
HALF_HEIGHT = int((HEIGHT)/2)

HEART_LOCATION = (28,15) #puts heart icon near life bar

CAMERASLACK = 90 #how far from the center the figure moves before the screen moves

MOVERATE = 9 #how fast the player moves
IMMUNETIME= 2 #how long the player is invulnerable
MAXHEALTH = 20 #how much health player starts with

LEFT = 'left'
RIGHT = 'right'
