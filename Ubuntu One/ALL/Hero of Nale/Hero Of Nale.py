#!/usr/bin/python2.6
'''Need to force gravity to work after the implementation of side collisions'''

import pygame, sys, os
import pygame.mixer
from pygame.locals import *
import math
import random

FPS = 30 #frames per second to update onto screen
WIDTH = 800 #width of the program's window, pixels
HEIGHT = 600 #height of the program's window, pixels

HALF_WIDTH = int((WIDTH)/2)
HALF_HEIGHT = int((HEIGHT)/2)

HEART_LOCATION = (28,15) #puts heart icon near life bar

CAMERASLACK = 30 #how far from the center the figure moves before the screen moves
	
MOVERATE = 8 #how fast the player moves
ARROWRATE = 10 #how fast the arrow soars
JUMPHEIGHT = 90 #how high the player can jump in pixels
JUMPRATE = 12 #how fast player jumps

IMMUNETIME= 2 #how long the player is invulnerable
MAXHEALTH = 30 #how much health player starts with

FREQ_OF_HEADS = 2 # randomize values between 1 and 100. Will spawn at 1 and n. n% spawn rate
LEFT = 'left'
RIGHT = 'right'

START_LOCATION = (400, HEIGHT - 200)

class hero(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)

		#load image
		self.r_hero_img = pygame.image.load(os.path.join('images','Hero.png')).convert()

		#scale image to a good size
		self.r_hero_img = pygame.transform.scale(self.r_hero_img,(60,70)) 

		#flip image vertically
		self.l_hero_img = pygame.transform.flip(self.r_hero_img, True, False)
		
		#set rect of the hero image
		self.rect = self.r_hero_img.get_rect()

		#used to determine which image is being used for direction hero is facing
		self.facing = RIGHT
		self.facing_surface = self.r_hero_img

		#size of the image
		self.height = self.r_hero_img.get_height()	
		self.width = self.r_hero_img.get_width()

		#location of the hero object
		self.x, self.y = START_LOCATION 

		#set maximum health for the hero
		self.health = MAXHEALTH	

		#set number of lives the hero has till gameover
		self.life = str(3)

		#used to determine how height the hero is when jumping. Used to check max jump height
		self.jump_height = 0
		self.isJumping = False


	#sets player to face this direction. Having the surface change to left sided image
	def face_left(self):
		self.facing_surface = self.l_hero_img
		self.facing = LEFT

	#sets player to face this direction. Having the surface change to right sided image
	def face_right(self):
		self.facing_surface = self.r_hero_img
		self.facing = RIGHT

	#used to move the current position of the player. Focuses only on horizontal movement
	def move(self, movement, terrain, camerax):
		index = None

		#used to store the new position of the old terrain.
		temp = [] 

		#check for collision. Focus index to be the cell in the list of terrain which focuses on the collision with the wall infront of the player
		#else collision is false
		for i in range(len(terrain)):
			if self.rect.colliderect(terrain[i]):
				index = i

				if terrain[index].top < self.rect.centery and (self.rect.left <= terrain[index].left or self.rect.right >= terrain[index].right):
					collision = True
					break
				elif terrain[index].top > self.rect.centery and (self.rect.left > terrain[index].left or self.rect.right < terrain[index].right):
					collision = False

			elif not self.rect.colliderect(terrain[i]):
				collision = False

		#if player has moved left
		if movement == 'left':
			#check that the index tile is directly in front of the player
			#if so stop movement
			if collision and self.rect.left < terrain[index].right:
				self.x += MOVERATE

			#Allow player to move in the opposite direction when colliding with object in front of the tile
			elif collision and self.rect.left > terrain [index].right:
				self.x += MOVERATE
		
			#player moves freely when not colliding
			elif not collision:
				self.x -= MOVERATE
			
			#moving left
			#if player has moved out of the cameraslack. stop his movement and move the camerax. 
			#camerax will be used to move the tiles
			if (camerax + HALF_WIDTH) - self.rect.centerx > CAMERASLACK:
				self.x += MOVERATE
				camerax = self.rect.centerx + CAMERASLACK - HALF_WIDTH

		#if player has move right
		if movement == 'right':
			#check that the index tile is directly in front of the player
			#if so stop movement
			if collision and self.rect.right > terrain[index].left:
				self.x -= MOVERATE

			#Allow player to move in the opposite direction when colliding with object in front of the tile
			elif collision and self.rect.right < terrain[index].left:
				self.x -= MOVERATE

			#player moves freely when not colliding
			elif not collision:
				self.x += MOVERATE

			#moving right
			#if player has moved out of the cameraslack. stop his movement and move the camerax. 
			#camerax will be used to move the tiles
			if self.rect.centerx - (camerax + HALF_WIDTH) > CAMERASLACK:
				self.x -= MOVERATE
				camerax = self.rect.centerx - CAMERASLACK - HALF_WIDTH

		#stablize movement. Keep MOVERATE value.
		if(camerax < 0):
			camerax = -MOVERATE
		elif(camerax > 0):
			camerax = MOVERATE

		for i in range(len(terrain)):
			topleft = terrain[i].topleft
			temp.append(pygame.Rect(topleft[0] - camerax, topleft[1], FLOOR_IMG.get_width(), FLOOR_IMG.get_height()))

		#copy the new terrain into terrain object
		terrain = temp[:]

		del temp

		camerax = 0

		return camerax, terrain
			
	#implement gravity when player is above an object below him. Meaning he is in mid air ontop of 
	#an object and needs to fall back to the ground.
	def GRAVITY(self, terrain):
		#keep track of the index of which terrain has collided with the player
		index = None
		
		#traverse the list of terrain objects
		for i in range(len(terrain)):
			#if player has not collided with floor object then keep falling
			if not self.rect.colliderect(terrain[i]):
				collision = False
			
			#if player has collided with terrain object. Set collision true - telling later commands to prevent player from falling
			#index keeps track of which terrain object has been collided with.
			elif self.rect.colliderect(terrain[i]) and self.rect.bottom <= terrain[i].bottom:
				collision = True
				index = i
				break

		#if has collided and that object is indeed under player. Prevent falling
		if collision:
			if terrain[index].top <= self.rect.bottom:
				self.y = self.y
				
			else:
				self.y += JUMPRATE
				
		#if player has not collided. Player keeps falling.
		else:
			#if above a terrain object. Waiting to fall to terrain object.
			if terrain[i].top > self.rect.bottom:
				self.y += JUMPRATE

			#if missing terrain. Fall to 'death'
			elif terrain[i].top <= self.rect.bottom:
				self.y += JUMPRATE

	#jump up. Assuming that the player has pressed 'up' or 'w'. Force one click = 1 jump.
	def JUMP(self, terrain):
		#if player has jumped and has not reached player's jump height continue to ascend until height reached.
		#Then stop ascending. Reset jump_height to keep track of future jumps.
		#Return the state of the player when jumping
		index = None
	
		#traverse the list of terrain objects
		for i in range(len(terrain)):
			#if player has not collided with floor object then continue the jump cycle
			if not self.rect.colliderect(terrain[i]):
				collision = False
			#if player has collided with terrain object. Set collision true - telling later commands to prevent player from jumping further
			#index keeps track of which terrain object has been collided with.
			elif self.rect.colliderect(terrain[i]) and terrain[i].bottom <= self.rect.top:
				collision = True
				index = i
				break
	
		#if object has collided with object above player. Stop player from jumping up.
		if collision:
			self.isJumping = False
			self.jump_height = 0

		#if object has not collided with object.
		else:
			#continue entire jump length
			if self.jump_height <= JUMPHEIGHT:
				self.isJumping = True
				self.y -= JUMPRATE
				self.jump_height += JUMPRATE
			
			#when jump length has reached prepare to decend	
			else:
				self.isJumping = False
				self.jump_height = 0

		return self.isJumping

	def drawHealthBar(self):
		#This is a function that display color health box
		#(215,237,58) is a Gold-ish color 
		#(255,255,255) is a white color
		#Displays box gold health bars and a white outline under the health bars
    		font = pygame.font.Font(None,30)

			#set up health bar. Align color bars within each other
    		for i in range(self.health):
			pygame.draw.rect(SURFACE, (215,237,58), (10, 5 + (5 * self.health) - i * 5, 15, 5))
    		for i in range(MAXHEALTH): # draw the white outlines
       		 	pygame.draw.rect(SURFACE, (255,255,255), (10, 5 + (5 * MAXHEALTH) - i * 5, 15, 5), 1)

    		SURFACE.blit(HEART_IMG,HEART_LOCATION)
   		text = font.render('x ' + self.life, True, (255,0,0))
   		SURFACE.blit(text,(HEART_LOCATION[0]*2 + 5,HEART_LOCATION[1]))

	def display(self, camerax):
		self.rect = pygame.Rect(self.x, self.y, self.width, self.height)
		
		#remove hero image background color
		self.facing_surface.set_colorkey((255,255,255),RLEACCEL)

		#display hero onto SURFACE with the correct direction image
		SURFACE.blit(self.facing_surface, self.rect)

		#update current status of the player and projectile
		if self.facing != RIGHT:
			PLAYER.face_left()
		else:
			PLAYER.face_right()
	
		PLAYER.drawHealthBar()

		#display all objects onto final SCREEN, double buffering
		SCREEN.blit(SURFACE, (0,0))

		pygame.display.update()
		FPSCLOCK.tick(FPS)

	def getPos(self):
		return self.x, self.y

class Soaring_NPC(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
	
		#load image
		self.NPC = pygame.image.load(os.path.join('images','floating_head.png')).convert()

		#get rect of image
		self.rect = self.NPC.get_rect()

		#size of the image
		self.height = self.NPC.get_height()
		self.width  = self.NPC.get_width()

		#coordinates of where the object is
		self.x, self.y = (WIDTH - 15, HALF_HEIGHT)

		#used to determine how far the object has soared towards the hero
		self.distance = 0

	def move(self):
		'''Move object to the left'''
		shift_rate = MOVERATE*1.25
		self.x -= shift_rate
		self.distance += shift_rate

	def printMove(self):
		print 'self.x', self.x

	def  isDone(self):
		'''Return true when object has reached its maxed distance. If true then delete this object'''
		if self.distance >= WIDTH:
			return True
		else:
			return False

	def display(self):
		self.rect = pygame.Rect(self.x, self.y, self.width, self.height)

		self.NPC.set_colorkey((255,255,255),RLEACCEL)
		SURFACE.blit(self.NPC, self.rect)


def main():
   global FPSCLOCK, SURFACE, SCREEN, BACKGROUND, HEART_IMG, FLOOR_IMG, PLAYER, ARROW_SND

   pygame.init()
   pygame.mixer.init()

   #make screen. Use two display surfaces for double buffering
   SCREEN = pygame.display.set_mode((WIDTH,HEIGHT))
   SURFACE = pygame.Surface(SCREEN.get_size())

   #set name of game in caption area
   pygame.display.set_caption('Hero of Nale')

   #start up time object
   FPSCLOCK = pygame.time.Clock()

   #load hero head as life icon
   HEART_IMG = pygame.image.load(os.path.join('images','heart.png')).convert()
   HEART_IMG = pygame.transform.scale(HEART_IMG, (30,28))

   #load in tile floor (50x50)
   FLOOR_IMG = pygame.image.load(os.path.join('images','block.png')).convert()

   #load background image to screen
   BACKGROUND = pygame.image.load(os.path.join('images', 'dungeon.png')).convert()
   BACKGROUND = pygame.transform.scale(BACKGROUND, (850,570))

   #load arrow sound, play when shooting
   ARROW_SND = pygame.mixer.Sound('arrow.png')

   PLAYER = hero()

   while True:
	run_game()



####move background versus moving hero

def run_game():
  immuneMode = False #if the player is immune to all damage
  immuneTime = 0 #time the player became immune to all damage
  winMode = False #if player has won
  gameOver = False #used for movement statement. so player doesn't keep moving if died

  #coordinates of the game world
  camerax = 0
  cameray = 0

  #test to determine which directions the player should move according to the key pressed
  moveLeft  = False
  moveRight = False
  jump    = False

  #used to allow the player to gradually jump up
  jumpCycle = False

  #used to know if the player has shot an arrow
  shooting = False

  #will contain all the platforms as a rect object	
  terrain = []


  #number of floor images needs to fill up a row across the screen. For pretend floor
  blocks = int(math.ceil(WIDTH/FLOOR_IMG.get_width()))


  for i in range(2*blocks):
	#stairs 
	#terrain.append(pygame.Rect(i*FLOOR_IMG.get_width()- 200, i*FLOOR_IMG.get_height() - 50, FLOOR_IMG.get_width(), FLOOR_IMG.get_height()))

	#under hero
	terrain.append(pygame.Rect(i*FLOOR_IMG.get_width(), HEIGHT - FLOOR_IMG.get_height(),FLOOR_IMG.get_width(),FLOOR_IMG.get_height()))
	


  terrain.append(pygame.Rect(10*FLOOR_IMG.get_width(), HEIGHT - 2*FLOOR_IMG.get_height(), FLOOR_IMG.get_width(),FLOOR_IMG.get_height()))
  terrain.append(pygame.Rect(12*FLOOR_IMG.get_width(), HEIGHT - 2*FLOOR_IMG.get_height(), FLOOR_IMG.get_width(),FLOOR_IMG.get_height()))  
  terrain.append(pygame.Rect(10*FLOOR_IMG.get_width(), HEIGHT - 3*FLOOR_IMG.get_height(), FLOOR_IMG.get_width(),FLOOR_IMG.get_height()))
  terrain.append(pygame.Rect(12*FLOOR_IMG.get_width(), HEIGHT - 3*FLOOR_IMG.get_height(), FLOOR_IMG.get_width(),FLOOR_IMG.get_height())) 	
  terrain.append(pygame.Rect(11*FLOOR_IMG.get_width(), HEIGHT - 3*FLOOR_IMG.get_height(), FLOOR_IMG.get_width(),FLOOR_IMG.get_height()))

  for i in range(blocks):
	terrain.append(pygame.Rect(0, i*FLOOR_IMG.get_height(), FLOOR_IMG.get_width(), FLOOR_IMG.get_height()))

  NPC = Soaring_NPC()

  while True:
	#clears screen
	SURFACE.fill((205,233,239))
	background_rect = pygame.Rect(-15,-17,BACKGROUND.get_width(),BACKGROUND.get_height())
	SURFACE.blit(BACKGROUND,background_rect)

	#display each tile that was read in. End to front.
	for i in range(len(terrain)):	
	   SURFACE.blit(FLOOR_IMG,terrain[i])

	for event in pygame.event.get():
	   if event.type == QUIT:
		terminate()
	   
	   elif event.type == KEYDOWN:
		#jump
		if event.key in (K_UP, K_w) and not jump:
		   isJumping   = True
		   jump = True
		   jumpCycle = True

		elif event.key in (K_LEFT, K_a):
		   moveRight = False
		   moveLeft  = True

		   #face the image to the left
		   PLAYER.face_left()

		elif event.key in (K_RIGHT, K_d):
		   moveLeft  = False
		   moveRight = True

		   #face the image to the right
		   PLAYER.face_right()

		elif event.key in (K_SPACE,):
			print 'PEW'

		elif event.key == K_ESCAPE:
		   terminate()

	   elif event.type == KEYUP:

		#when key is lifted up stop movement
		if event.key in (K_LEFT,K_a):
		   moveLeft = False

		elif event.key in (K_RIGHT,K_d):
		   moveRight = False

	if not gameOver:
		if moveLeft:
		   camerax, terrain = PLAYER.move('left', terrain, camerax)

		if moveRight:
		   camerax, terrain = PLAYER.move('right', terrain, camerax)

		if jump:
			jump = PLAYER.JUMP(terrain)
		
		if not jump:
			PLAYER.GRAVITY(terrain)

	NPC.display()
	#contains all blits
	PLAYER.display(camerax)
	
		
def terminate():
    pygame.quit()
    sys.exit()

if __name__ == '__main__':
    main()
