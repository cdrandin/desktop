import pygame
import random
from CONSTANTS import *

class Bricks(pygame.sprite.Sprite):
  def __init__(self, x, y):
    # Call the parent class (Sprite) constructor
    pygame.sprite.Sprite.__init__(self)

    # Create the image of the block of appropriate size
    # The width and height are sent as a list for the first parameter.
    self.image = pygame.Surface([B_Width, B_Height])

    # Fill the image with the appropriate color
    self.image.fill((random.randint(25,255), random.randint(25,255), random.randint(25,255)))

    # Fetch the rectangle object that has the dimensions of the image
    self.rect = self.image.get_rect()

    # Move the top left of the rectangle to x,y.
    # This is where our block will appear
    self.rect.x = x
    self.rect.y = y