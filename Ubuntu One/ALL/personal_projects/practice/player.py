import pygame
from CONSTANTS import *

#Player class using Dirty Sprite object
class Player(pygame.sprite.DirtySprite):
  # Constructor function
  def __init__(self):
    # Call the parent's constructor
    pygame.sprite.DirtySprite.__init__(self)

    self.image = pygame.Surface((P_Width, P_Height))
    self.image.fill((255,255,255))
      
    # Make our top-left corner the passed-in location.
    self.rect = self.image.get_rect()
    self.screenheight = pygame.display.get_surface().get_height()
    self.screenwidth = pygame.display.get_surface().get_width()

    self.rect.x = self.screenwidth/2 - P_Width/2  #center of screen
    self.rect.y = self.screenheight - P_Height
    
    self.run = False
  

    
  # Update the player
  def update(self):	
    if pygame.mouse.get_pressed()[0] and not self.run:
      self.run = True
    
    if self.run:
      # Get where the mouse is
      pos = pygame.mouse.get_pos()

      #center the cursor to be in the middle of the player's brick
      self.rect.x = pos[0] - self.image.get_width()/2
      
      #Make sure player doesn't go off screen
      if self.rect.x > self.screenwidth - P_Width:
        self.rect.x = self.screenwidth - P_Width
        
      if self.rect.x < 0:
        self.rect.x = 0
      
    #used for dirty sprite displaying image
    self.dirty = 1
