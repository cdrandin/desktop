import pygame 		              #import basic pygame modules
from pygame.locals import *       #import pygame constants
from bricks import Bricks         #import bricks class
from player import Player         #import player class
from ball import Ball             #import ball class
from bricks import Bricks         #import brick class
from load_level import Load_Level #import load level class
from CONSTANTS import *           #import constants that will be used throughout the program
import random

def terminate():
  pygame.quit()
  exit(1)

def main():
  lvl_num = str(1)
  current_level = 'levels/level'+lvl_num+'.txt'
   
  pygame.init()
  
  #set screen size and background.
  #Make game title
  screen = pygame.display.set_mode((WIDTH,HEIGHT))
  pygame.display.set_caption("Brick Breakers")
  background = pygame.Surface(screen.get_size())
  
  # This is a font we use to draw text on the screen (size 36)
  font = pygame.font.Font(None, 36)

  #create instances of our player, the ball, and sprite group containing the bricks to be hit
  player = Player()
  ball   = Ball()
  bricks = pygame.sprite.Group()
  balls   = pygame.sprite.Group() #used to store ball object and use spritecollide for easy collision detection
  balls.add(ball)
  
  #store these classes into a group of dirty sprites known as LayeredDirty
  allSprites = pygame.sprite.LayeredDirty((player, ball))  

  #read a level returns container of sprites which is then added into sprite.Group 'bricks'
  for i in Load_Level(current_level):
    bricks.add(i)

  clock = pygame.time.Clock() #keep track of FPS.  Keep @ 30fps
  
  press_to_start   = 'Click on the screen where to launch ball'
  press_to_con     = 'Press \'p\' to continue the game'
  game_over_prompt = 'Game Over!'
  
  #boolean values to determine different states in the game
  game_started = False
  game_paused  = False
  
  allSprites.clear(screen, background) #set background
  
  while True:
    #make background black and draw to screen
    background.fill((0,0,0))
    screen.blit(background,(0,0))
    
    #Handle events
    for event in pygame.event.get():
      if event.type == QUIT:	
        terminate()
        
      elif event.type == KEYDOWN:
        if event.key == K_ESCAPE:
         terminate()
         
	#prevent pausing while game hasn't strted
        elif event.key == K_p and game_started:
          game_paused = True

      elif event.type == MOUSEBUTTONDOWN:
        game_started = True
        pygame.mouse.set_visible(False)
        
    #update and display player, ball, and brick
    allSprites.update()
    bricks.draw(screen)
    allSprites.draw(screen)
    #pygame.display.update(allSprites.draw(screen))
    
    #Ball has collided with player paddel
    #### Ugly way of bouncing
    if pygame.sprite.spritecollide(player, balls, False):
      ball.vel_x *= -1
      ball.vel_y *= -1
    
    #Ball has collided with brick(s)
    #### Ugly way of bouncing
    if pygame.sprite.spritecollide(ball, bricks, False):
      ball.vel_x *= -1
      ball.vel_y *= -1
      
    ####
    #### GAME STATES - game_over, pause, game_started
    ####
    #Game Over state
    if ball.is_game_over():	
      blank = pygame.Surface([200,50])
      blank.fill((0,0,0))
      screen.blit(blank, blank.get_rect(centerx = background.get_width()/2, centery = background.get_height()/2))
      
      text=font.render(game_over_prompt, True, (random.randint(0,225),random.randint(0,225),random.randint(0,225)))
      textpos = text.get_rect(centerx = background.get_width()/2, centery = background.get_height()/2)
      screen.blit(text, textpos)
      
      pygame.display.update()
      
      while True:
        event = pygame.event.wait()
        if event.type == KEYDOWN and event.key == K_ESCAPE:
          terminate()
          
    #player has not launched ball yet
    if not game_started:
      text=font.render(press_to_start, True, (random.randint(0,225),random.randint(0,225),random.randint(0,225)))
      textpos = text.get_rect(centerx = background.get_width()/2, centery = background.get_height()/2 + 100)
      
      blank = pygame.Surface([textpos.width, textpos.height])
      blank.fill((0,0,0))
      blankpos = blank.get_rect(centerx = background.get_width()/2, centery = background.get_height()/2 + 100)
      
      screen.blit(blank, blankpos) 
      screen.blit(text, textpos)
      
      
    #simple quick pause with no menu
    #display pause prompt to screen
    #Freeze real gameplay
    if game_paused:
      blank = pygame.Surface([200,50])
      blank.fill((0,0,0))
      screen.blit(blank, blank.get_rect(centerx = background.get_width()/2, centery = background.get_height()/2 + 100))
      
      text=font.render(press_to_con, True, (random.randint(0,225),random.randint(0,225),random.randint(0,225)))
      textpos = text.get_rect(centerx = background.get_width()/2, centery = background.get_height()/2 + 100)
      screen.blit(text, textpos)    
      
      pygame.display.update()
      
      while True:
        event = pygame.event.wait()
        if event.type == KEYDOWN and event.key == K_p:
          game_paused = False
          break
    
    #display everything
    pygame.display.update() 
    clock.tick(FPS)
    
    
if "__main__" == __name__:
  main()

def terminate():
  pygame.quit()
  exit(1)