import pygame
from CONSTANTS import *
from bricks import Bricks

#STRICT file length 20(col) x 24(row)
#0 - empty space    >0 - some brick  (1) - normal brick
#TODO Future implmentation unique bricks with different numbers

#Read in file storing brick classes in a list
def Load_Level(fileName):
  bricks = []
  x_move = 2 #number of pixels to leave space inbetween bricks
  y_move = 5 # number of pixel to leave space under and above each brick *more flexible to change number*
  
  coord_x = 4 #start 4 pixels from the left of the screen
  coord_y = 50 #start 50 pixels away from top of screen
  
  with open(fileName, 'r') as infile:
    data = [i.strip('\n') for i in infile]
    
  for row in data:
    #Reading each char. If there is a 1, meaning their is a brick needed to display
    if '1' in row:
      for brick in row:
        #if read in 1 create brick at (coord_x, coord_y)
	#move coord the size of brick width to print next brick along side it
        if brick == '1':
          bricks.append(Bricks(coord_x, coord_y))
      
        coord_x += B_Width + x_move
          
      #finished creating bricks on the current level
      #move to level below, reset coord x and increase coord_y by the brick height
      coord_x = 4
      coord_y += B_Height + y_move
      
    #No brick needed to be printed on this floor, move down to next line to check if there is a brick
    else:
      coord_y += B_Height + y_move
      
  return bricks