import pygame
from CONSTANTS import *
import math

class Ball(pygame.sprite.DirtySprite):
  def __init__(self):
    pygame.sprite.DirtySprite.__init__(self)
    self.image = pygame.Surface((10,10))
    self.image.fill((201,56,168))
    self.x = 0
    self.y = 0
    
    self.rect = self.image.get_rect()
    self.rect.center=(pygame.display.get_surface().get_width()/2, 
                      pygame.display.get_surface().get_height() - P_Height - 7)
    
    self.run = False #when player starts game allow ball to move accordingly
    self.dead = False #check to see if ball is still in play
    
  def update(self):
    #game has not started.  Launch ball at location then no longer enter this state.
    if pygame.mouse.get_pressed()[0] and not self.run :
      self.run = True
      
      pos = pygame.mouse.get_pos()
    
      adj = self.rect.centerx - pos[0]
      opp = self.rect.centery - pos[1]
      hyp = (adj*adj + opp*opp)**0.5
      
      #get angle at which the ball is ith respects to it's desginated Trajectory
      self.angle = math.acos(adj/hyp)
      
      #get rates at which the ball to move diagonally
      self.scale_x = math.cos(self.angle)
      self.scale_y = math.sin(self.angle)
      
      #scale the rate with the set Ball_Speed
      self.vel_x = Ball_Speed * self.scale_x
      self.vel_y = Ball_Speed * self.scale_y
      
    #if ball hit left or right side of the screen. Reverse x direction
    if self.run:
      if self.rect.x < 0 or self.rect.x > WIDTH - B_Width:
        self.vel_x *= -1
        
    #if ball hits top of the screen then bounce
      if self.rect.y < 0:
        self.vel_y *= -1
        
      if self.rect.y > HEIGHT:
        self.dead = True
      
      self.rect.centerx -= self.vel_x
      self.rect.centery -= self.vel_y

    self.dirty = 1
    
  def is_game_over(self): return self.dead