import socketserver
import pygame
from pygame.locals import *

class MyUDPHandler(socketserver.BaseRequestHandler):
    """
    This class works similar to the TCP handler class, except that
    self.request consists of a pair of data and client socket, and since
    there is no connection the client address must be given explicitly
    when sending data back via sendto().
    """

    def handle(self):
		#						       (bytes sent , socket info)
        # example:  self.request  ==   (b's'       , <socket.socket object, fd=3, family=2, type=2, proto=0>)
       
        #print(self.request)
        #print(self.client_address)
        #print(self.server) 
        

        data = self.request[0].strip()
        socket = self.request[1]
        print("{} wrote:".format(self.client_address[0]))
        print(data)
        event = data.decode()
       #print("Decoded is {}".format(event))

        move = None

        if event == 'kill':
           self.server.shutdown()
           exit(1)

        else:
            if event == 'w': move = 'up'
            elif event == 'a': move = 'left'
            elif event == 's': move = 'down'
            elif event == 'd': move = 'right'

            #only return info when 'thing' is moving
            if not move: socket.sendto(bytes('none','utf-8'), self.client_address)
            else: socket.sendto(bytes(move,'utf-8'), self.client_address)


if __name__ == "__main__":
    HOST, PORT = "127.0.0.2", 9999
    server = socketserver.UDPServer((HOST, PORT), MyUDPHandler)
    try:
        server.serve_forever()
    except:
        exit(0)

