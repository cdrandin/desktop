import socketserver
import pickle
import socket

class MyUDPHandler(socketserver.BaseRequestHandler):
    """
    This class works similar to the TCP handler class, except that
    self.request consists of a pair of data and client socket, and since
    there is no connection the client address must be given explicitly
    when sending data back via sendto().
    """

    def handle(self):
        data = self.request[0].strip()
        socket = self.request[1]
        print("{} wrote:".format(self.client_address[0]))
        print(pickle.loads(data),end='\n')
        socket.sendto(data, self.client_address)

if __name__ == "__main__":
    HOST, PORT = '10.67.29.87', 12345
    server = socketserver.UDPServer((HOST, PORT), MyUDPHandler)
    server.serve_forever()

