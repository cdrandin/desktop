import socket
import sys
import pickle

mydict = {'a': [1, 2.0, 3, 4+6j],
         'b': ('string', bytes('Unicode string','utf-16')),
         'c': None}

HOST, PORT = "localhost", 12345
data = pickle.dumps(mydict)

# SOCK_DGRAM is the socket type to use for UDP sockets
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# As you can see, there is no connect() call; UDP has no connections.
# Instead, data is directly sent to the recipient via sendto().
sock.sendto(data, (HOST, PORT))
received = sock.recv(1024)

print("Sent:     %s" % mydict)
print()
print("Received: %s" % pickle.loads(received))

