from create_table_driven_predictive_parser import return_table
import lexer
import sys

#split the word to work with out driver given what is read from the table
def SPLIT(list):
	temp = []
	word = ''
	Open = False

	for i in list:
		if i == '<':
			Open = True

		if i == ' ' and not Open:
			word += i
			temp.append(word)
			word = ''

		elif i == '>' and Open:
			word += i
			temp.append(word)
			word = ''
			Open = False

		elif i != '>':
			word += i
	
	tmp = []

	for i in range(len(temp)):	
		if len(temp[i]) == 1 and temp[i] == ' ':
			pass
		else:
			tmp.append(temp[i])

	#handle 'or' statments.  If arises. Dump into string and return
	if '| ' in tmp:
		string =''
		for i in range(len(tmp)):
			string += tmp[i]

		del tmp
		tmp = []
		tmp.append(string[0:len(string)]) #remove space at the end
		 

	return tmp

def main():
	#**Table input must end in ' ' as an invisiable delimeter to handle parsing through the spreadsheet
	#get table from our module that reads in spreadsheet that has the values of the table
	#also the terminal and nonterminal values of the table
	table, terminals, nonterminals = return_table()

	#Create our stack and push end marker ($) into stack
	stack = []
	stack.append('$')
	
	InfileName = sys.argv[1]

	#start current point to beginning of file
	cp = 0

	#empty character in token
	token = ''

	#store values from our lexer and dumps all into a list
	Input = []

	#return current pointer after is has read in chars and returned an accepting token.
	with open(InfileName, 'r') as infile:
		fileRead = infile.read()
		file_len = len(fileRead) - 1 
		
		#handle EOF null character vs space character
		if fileRead[file_len] == ' ' or fileRead[file_len] == '\n':
			file_len -= 1


	#how long the file is and go through each token
	#dump token and lexemes from lexer all into a list as tuple information (*token*, *lexeme*)
	#list called "Input"
	while cp < file_len:
		#call our lexer
		cp, lexeme, token = lexer.lexer(cp, open(InfileName, 'r'))

		#ignore comments.  Call again.
		if lexeme in (' ','\n'):
			cp, lexeme, token = lexer.lexer(cp, open(InfileName, 'r'))

		if lexeme != ' ':
			#Add to a list
			Input.append((lexeme.strip(' \n'),token))

	counter = 0 # counter to traverse list which dumped tokens and lexeme from lexer function
	stack.append("<Rat12F>") #push start production on to stack

	#Print first set of info
	top = stack[len(stack) - 1]
	stack[len(stack) - 1].strip() #top of stack
	inc = Input[counter][0] # lexeme, counter iterates through our (lexemes,tokens) list
	#print("TOP: {}\nINC: {}\nSTACK: {}\n".format(top,inc,stack))

	skip = False # used when dealing with symbols like ':' within a production definition

	for k in range(18):

		#when we need to read flip back on
		if skip:
			skip = False

		#when we are still dealing with the incoming value which the top of 
		#the production is still expanding	
		if not skip: 
			top = stack[len(stack) - 1] #top of stack

		if not skip:
			inc = Input[counter][0] # lexeme, counter iterates through our (lexemes,tokens) list

		if not skip: 		
			term = Input[counter][1] #get token


		#check info before stack changes
		#print("TOP: {}\nINC: {}\nSTACK: {}\n".format(ascii(top),ascii(inc),stack))

		#if top is a terminal symbol
		if top in terminals:
			if top in inc or top == Input[counter][1]:
				stack.pop()
				counter += 1

			else:
				sys.exit("Error")
		
		else:
			#handle expanding productions, when inc lexeme is a terminal value. assign its value using term
			if term in terminals: #preparing to expand since inc is a terminal, but a non-terminal is needed
				inc = term

			elif top == inc: #terminals
				#get next item from lexer. pop from stack
				stack.pop()
				counter += 1
				skip = True

				#top = stack[len(stack) - 1] #top of stack
				#inc = Input[counter][0] # lexeme, counter iterates through our (lexemes,tokens) list
				

			#if there are multiple options for terminal items
			if '|' in top:
				if inc[1:] in top:
					top = inc
					stack.pop()
					counter += 1
					skip = True

			if not skip:
				#table entry
				#check to see if their is expanding production given what's on top of the stack and the incoming token
				try:
					item = table[(top,inc)]

				except:
					sys.exit("Syntax Error: \"{}\".  {} is the expected values1".format(inc,Input[counter][1]))

				#if table entry has an existing value
				if item:
					stack.pop()

					#split nicely to be able to put in reverse order
					temp = SPLIT(item)

					#push production into stack in reverse order
					for i in range(len(temp)-1, -1, -1):
						#remove 'function ', because extra space causes error. Bad way in solving the issue
						if ((temp[i] in ('>',)) and (temp[i] in ('<',))):
							stack.append(temp[i])

						else:	
							stack.append(temp[i].strip(' '))
				
				elif top == "<Opt Dec List>":
					#if <Opt Dec List> and "{" meaning <Opt Dec List> -> <Empty>  instead of <Decleration List>
					if inc == "{":
						#just pop off <Opt Dec List> since we have no initial dec list
						#don't get next item since '{' is still needed
						stack.pop()

				#no value at table entry.  Error
				else:
					#reverse look up table to determine what the expect input should have been
					sys.exit("Syntax Error {}.  {} are the expected values3".format(inc,Input[counter][1]))

		#check info after stack changes
		print("TOP: {}\nINC: {}\nSTACK: {}\n".format(ascii(top),ascii(inc),stack))

	'''
	#print info of one after   ~~~~~TEMP~~~~~
	top = stack[len(stack) - 1].strip() #top of stack

	inc = Input[counter][0] # lexeme, counter iterates through our (lexemes,tokens) list

	print("TOP: {}\nINC: {}\nSTACK: {}\n".format(top,inc,stack))
	'''
if "__main__" == __name__:
	main()
