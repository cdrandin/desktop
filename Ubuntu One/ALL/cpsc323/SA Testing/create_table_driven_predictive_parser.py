import zipfile

import xml.dom.minidom

def return_table():
	filename = 'Table_Driven_Predictive_Parser.ods'
	mod_odf = zipfile.ZipFile(filename)
	filelist = mod_odf.infolist()

	#Table being read in cannot have the same entries in a single row, put in distinctions!

	#read the odf file, which is zipped. parse the string. Find our filter notation in finding our data ('txt:p')
	infile = mod_odf.read('content.xml')
	doc = xml.dom.minidom.parseString(infile)
	paras = doc.getElementsByTagName('text:p')
	
	#constant variables in handling to make our table driven predictive parser
	#table = Ddict( dict )
	TABLE = {}
	TERMINAL = []
	NONTERMINAL = []

	row = []
	col = None
	row_read = True
	start = False
	cont = False
	counter = 0
	
	for p in paras:
		for ch in p.childNodes:
			#Create our table and place into 2D array
			#Read row by row handling all at once
			if ch.nodeType == ch.TEXT_NODE:
				#read in the first row, which are the terminal values
				if row_read:
					#ignore 0th row, which are our terminal values (labels)
					if ch.data != '0':
						row.append(ch.data)
						TERMINAL.append(ch.data)

				#handle 0th col, which are the non-terminal values. Set value equal to col. enter next state.
				if start:
					col = ch.data
					start = False
					cont = True
					#Delimeter to stop reading the spreadsheet
					if str(col) == '<END>': 
						start = False
						cont  = False
						break
				
				elif cont:
					if '~' in ch.data:
						ch.data = None

					TABLE[(col,row[counter])] = ch.data
					NONTERMINAL.append(ch.data)

					#print out table
					#print ("table[{}][{}] = {}".format(col, row[counter], ch.data))

					counter += 1
					if counter >= len(row):
						counter = 0
						start = True
						cont = False

				#used to handle 0th row.
				if ch.data == '\'<=\'':	
					row_read = False
					start = True

				#check bugs in Open Office Spreadsheet when trying to read. Space sensative. 1 space max.
				#used to test up to certain cases. inside ' ' put Nonterminal value.
				#if str(col) == '<END>': exit(1)

	return TABLE, TERMINAL, NONTERMINAL

#if "__main__" == __name__:
#	return_table()
