class Symbol_Table():
  __slots__ = ['table', 'memory_address', 'read']
  
  def __init__(self):
    self.table = dict()
    self.memory_address = 1000
    
  def insert(self, key, value):
    if self.read:
      self.table[key] = (value, self.memory_address)
      self.memory_address += 1

  def display(self):
  #display symbol table with respects to ascending order of memory_address
    for k,v in sorted(self.table.items(), key = lambda x:x, reverse = False):
      print("{}: {}".format(k,v))

  def accept(self, truth):
    self.read = truth

  def can_read(self):
    return self.read