

'''
Christopher Randin
CPSC 323 Program Languages and Translation

DUE: November 9, 2012

File: Syntax Analyer
Purpose:
Using a module lexer to invidually piece out unique token in the source code and then return these tokens and lexemes.
Lexer was a burte force method, most certainly not optimized in anyway.
This program then reads these tokens into a list and traverse the list analying each lexeme to see if it matchs with
our unique grammar.  Given our grammar we can devise a way in making our own complier that is capable of doing basic set of instructions.	
'''

import lexer
import sys
import Symbol_Table

##
## NOTE: Lexer (lexeme, tokens) in list "Input" which is a 2D array.  First [] is the iteration through the list
##	Second [] is the type of information. [0] - is lexeme. [1] - is token. [2] - is current line number the token is in
##

#short functional method to pretty print (token, lexeme)
Print=lambda token,lexeme: outfile.write('\nToken: {}     Lexeme: {}\n'.format(token,lexeme))

table = Symbol_Table.Symbol_Table()
data_type = None

#
#<Rat12F> --> <Function> <Rat12F Prime>
def Rat12F():
	global counter 
	global outfile

	try:
		Print(Input[counter][1],Input[counter][0])

	except IndexError:
		sys.exit(1)
	
	outfile.write("<Rat12F> --> <Function> <Rat12F Prime>\n")

	#call function to check multiple functions or our main()
	Function()

	#check for additional functions
	Rat12F_Prime()

#
#<Rat12F Prime> --> <Rat12F> | empty
def Rat12F_Prime():
	global counter
	global outfile

	try:
		#call function to check multiple functions or our main()
		if Input[counter][0] == 'function':
			outfile.write('<Rat12F Prime> --> <Rat12F>\n')
			Rat12F()

	except IndexError:
		outfile.write("<Rat12F Prime> --> empty\n")
		pass

#
#<Function> --> function <Identifier> ( <Parameter> ) <Opt Dec List> <Body>
def Function():
	global counter
	global outfile

	#check if funciton(lexeme) keyword exist
	if Input[counter][0] == 'function':
		outfile.write('<Function> --> function <Identifier> ( <Parameter> ) <Opt Dec List> <Body>\n')
		counter += 1

	else:
		print("Line {}:: Error: Keyword \'function\' expected.".format(Input[counter][2]+1))
		exit(1)

	#next token should be identifier
	if Input[counter][1] == '<Identifier>':
		Print(Input[counter][1],Input[counter][0])
		counter += 1

	else:
		print("Line {}:: Error: Identifier expected when declaring function name".format(Input[counter][2]+1))
		exit(1)

	#followed by "(" for parameter. Note if "main()" there should be no parameter
	if Input[counter][0] == '(':
		Print(Input[counter][1],Input[counter][0])
		counter += 1
	
	else:
		print("Line {}:: Error: \'(\' expected.".format(Input[counter][2]+1))
		exit(1)

	#if main() is in.  It can be with or without parameters
	#or handle normal functions
	if Input[counter-2][0] != 'main':
		Print(Input[counter][1],Input[counter][0])
		#call parameter funciton, if main program, doesn't need parameters
		Parameter()
	
	#main()
	elif Input[counter-2][0] == 'main' and Input[counter][0] == ')':
		Print(Input[counter][1],Input[counter][0])
		outfile.write('<Parameter> --> empty\n')
		pass

	#main(<Parameter>)
	elif Input[counter-2][0] == 'main':
		Print(Input[counter][1],Input[counter][0])
		Parameter()
	
	if Input[counter][0] == ')':
		counter += 1

	else:
		print("Line {}:: Error: \')\' expected to close function parameter".format(Input[counter][2]+1))
		exit(1)

	Opt_Declaration_List()

	Body()

#
#<Parameter> --> <Identifier> : <Qualifier>
def Parameter():
	global counter
	global outfile

	outfile.write('<Parameter> --> <Identifier> : <Qualifier>\n')

	#next token should be identifier
	if Input[counter][1] == '<Identifier>':

		counter += 1

	else:
		print("Line {}:: Error: Identifier expected".format(Input[counter][2]+1))
		exit(1)

	if Input[counter][0] == ':':
		Print(Input[counter][1],Input[counter][0])
		counter += 1

	else:
		print("Line {}:: Error: \':\' expected".format(Input[counter][2]+1))
		exit(1)

	#handle data types
	Qualifier()

#
#<Qualifier> --> int | real | boolean
def Qualifier():
	global counter
	global outfile
	
	#if next lexeme is a variable type decleration
	if Input[counter][0] in ('int', 'real', 'boolean'):
		outfile.write('<Qualifier> --> {}\n'.format(Input[counter][0]))
		counter += 1

	else:
		print("Line {}:: Error: Type ('int', 'real', 'boolean') expected".format(Input[counter][2]+1))
		exit(1)

#
#<Opt_Declaration_List> --> <Declaration List> | empty
def Opt_Declaration_List():
	global counter
	global outfile

	#no options, immediately start the body
	if Input[counter][0] == '{':
		Print(Input[counter][1],Input[counter][0])
		outfile.write('<Opt_Declaration_List> --> empty\n')
		Body()

	else:
		Print(Input[counter][1],Input[counter][0])
		outfile.write('<Opt_Declaration_List> --> <Declaration List>\n')
		Declaration_List()

#
#<Declaration List> --> <Declaration> ; <Declaration List Prime>
def Declaration_List():
	global counter
	global outfile
	global table ####<-
	
	outfile.write('<Declaration List> --> <Declaration> ; <Declaration List Prime>\n')

	table.accept(True) ####<- When to accept for symbol table
	
	Declaration()

	if Input[counter][0] == ';':
		table.accept(False) ####<- When to not accept for symbol table
		counter += 1

	else:
		print("Line {}:: Error: \';\' expected".format(Input[counter][2]+1))
		exit(1)

	Declaration_List_Prime()

#
#<Declaration List Prime> --> <Declartion List> | empty
def Declaration_List_Prime():
	global counter
	global outfile

	#try to run Declaration_List, if nothing can happen then expected empty
	if Input[counter][0] == '{':
		outfile.write('<Declaration List Prime> --> empty\n')
		pass
	
	else:
		outfile.write('<Declaration List Prime> --> <Declartion List>\n')
		Declaration_List()

#
#<Declaration> --> <Qualifier> <IDs>
def Declaration():
	global counter
	global outfile
	global data_type ####<-

	outfile.write("<Declaration> --> <Qualifier> <IDs>\n")

	data_type = Input[counter][0] ####<-
	
	Qualifier()

	IDs()

#
#<IDs> --> <Identifier> <IDs Prime>
def IDs():
	global counter
	global data_type ####<-
	global table ####<-
	
	Print(Input[counter][1],Input[counter][0])
	outfile.write('<IDs> --> <Identifier> <IDs Prime>\n')

	if Input[counter][1] == '<Identifier>':
		table.insert(Input[counter][0], data_type) ####<-
		counter += 1
	
	else:
		print("Line {}:: Error: Identifier expected".format(Input[counter][2]+1))
		exit(1)

	IDs_Prime()


	#catch when variable decleration should end.  If fail to end correctly pop error
	#also handle [ <IDs> ']'
	if Input[counter][0] in (')',']'):
		pass

	elif Input[counter][0] != ';':
		print("Line {}:: Error: \';\' expected".format(Input[counter][2]+1))
		exit(1)

#
#<IDs Prime> -->  ,<IDs> | empty 
def IDs_Prime():
	global counter
	global outfile

	Print(Input[counter][1],Input[counter][0])

	if Input[counter][0] == ',':
		outfile.write('<IDs Prime> -->  ,<IDs>\n')
		counter += 1

		IDs()
	
	else:
		outfile.write('<IDs Prime> --> empty\n')
		pass

#
#'<Body> --> { <Statement List> }
def Body():
	global counter
	global outfile

	try:
		if Input[counter][0] == '{':	
			Print(Input[counter][1],Input[counter][0])	
			outfile.write('<Body> --> { <Statement List> }\n')
			counter += 1

			Statement_List()

			if Input[counter][0] == '}':
				counter += 1

	except:
		pass

#
#<Statement List> --> <Statement> <Statement List Prime>
def Statement_List():
	global outfile

	Print(Input[counter][1],Input[counter][0])
	outfile.write("<Statement List> --> <Statement> <Statement List Prime>\n")

	Statement()

	Print(Input[counter][1],Input[counter][0])

	Statement_List_Prime()

#
#<Statement List Prime> --> <Statement List> | empty
def Statement_List_Prime():
	global counter
	global outfile

	if Input[counter][0] == '}':
		outfile.write("<Statement List Prime> --> empty\n")
		pass
	
	else:
		outfile.write("<Statement List Prime> --> <Statement List>\n")
		Statement_List()

#
#<Statement> --> <Compound> | <Assign> | <If> | <While> | <Return> | <Printf> | <Scanf>
def Statement():
	global counter
	global outfile

	#Compound
	if Input[counter][0] == '{':
		outfile.write("<Statement> --> <Compound>\n")
		counter += 1
		Compound()

	#Assign
	elif Input[counter][1] == '<Identifier>':
		outfile.write("<Statement> --> <Assign>\n")
		counter += 1
		
		Assign()
	#If
	elif Input[counter][0] == 'if':
		outfile.write("<Statement> --> <If>\n")
		counter += 1

		If()

	#While
	elif Input[counter][0] == 'while':
		outfile.write("<Statement> --> <While>\n")
		counter += 1

		While()

	#Return
	elif Input[counter][0] == 'return':
		outfile.write("<Statement> --> <Return>\n")
		counter += 1

		Return()

	#Printf
	elif Input[counter][0] == 'printf':
		outfile.write("<Statement> --> <Printf>\n")
		counter += 1

		Printf()

	#Scanf
	elif Input[counter][0] == 'scanf':
		outfile.write("<Statement> --> <Scanf>\n")
		counter += 1

		Scanf()
	
#
#<Compoud> --> { <Statement List> }
def Compound():
	global counter
	global outfile

	outfile.write('<Compoud> --> { <Statement List> }\n')

	Statement_List() 

	if Input[counter][0] == '}':
		counter += 1
		
		#if missing brace in compound
		if counter == len(Input):
			print("\'}\' expected")
			exit(1)
	else:
		print("Line {}:: Error: } expected".format(Input[counter][2]+1))		
		exit(1)

#
#<Assign> --> <Identifier> = <Expression>;
def Assign():
	global counter 
	global outfile

	outfile.write('<Assign> --> <Identifier> = <Expression>;\n')

	Print(Input[counter][1],Input[counter][0])

	if Input[counter][0] == '=':
		counter += 1
		Print(Input[counter][1],Input[counter][0])

	else:
		print("Line {}:: Error: \'=\' expected for assignment".format(Input[counter][2]+1))
		exit(1)

	Expression()

	if Input[counter][0] == ';':
		counter += 1

	else:
		print("Line {}:: Error: \';\' expected".format(Input[counter][2]+1))
		exit(1)

#
#<If> --> if ( <Condition>) <Statement> <If Prime> fi
def If():
	global counter
	global outfile

	outfile.write('<If> --> if ( <Condition>) <Statement> <If Prime> fi\n')

	if Input[counter][0] == '(':
		Print(Input[counter][1],Input[counter][0])
		counter += 1

	else:
		print("Line {}:: Error: \'(\' expected for if statement".format(Input[counter][2]+1))
		exit(1)

	Condition()

	if Input[counter][0] == ')':
		counter += 1

	else:
		print("Line {}:: Error: \')\' expected for if statement".format(Input[counter][2]+1))
		exit(1)

	Statement()

	If_Prime()

	if Input[counter][0] == 'fi':
		Print(Input[counter][1],Input[counter][0])
		counter += 1

	else:
		print("Line {}:: Error: \'fi\' expected at the end of if statement".format(Input[counter][2]+1))
		exit(1)

#
#<If Prime> --> else <Statement> | empty
def If_Prime():
	global counter
	global outfile

	if Input[counter][0] == 'fi':
		outfile.write('<If Prime> --> empty\n')
		pass
	
	else:
		Print(Input[counter][1],Input[counter][0])
		outfile.write('<If Prime> --> else <Statement>\n')

		counter += 1
		Print(Input[counter][1],Input[counter][0])

		Statement()

#
#<While> --> while (<condition>) <Statement>
def While():
	global counter
	global outfile

	outfile.write('<While> --> while (<condition>) <Statement>\n')

	if Input[counter][0] == '(':
		Print(Input[counter][1],Input[counter][0])
		counter += 1

	else:
		print("Line {}:: Error: \'(\' expected".format(Input[counter][2]+1))
		exit(1)

	Condition()

	if Input[counter][0] == ')':
		counter += 1
		Print(Input[counter][1],Input[counter][0])

	else:
		print("Line {}:: Error: \')\' expected".format(Input[counter][2]+1))
		exit(1)

	Statement()

#
#<Return> --> return <Return Prime> ;
def Return():
	global counter
	global outfile

	outfile.write("<Return> --> return <Return Prime>;\n")

	Return_Prime()

	Print(Input[counter][1],Input[counter][0])

	if Input[counter][0] == ';':
		counter += 1

	else:
		print("Line {}:: Error: \';\' expected".format(Input[counter][2]+1))
		exit(1)

#
#<Return Prime> --> <Expression> | empty
def Return_Prime():
	global outfile

	try:
		Print(Input[counter][1],Input[counter][0])
		outfile.write('<Return Prime> --> <Expression>\n')
		Expression()

	except:
		outfile.write('<Return Prime> --> empty\n')
		pass	

#
#<Printf> --> printf( <Expression> );
def Printf():
	global counter
	global outfile

	outfile.write('<Printf> --> printf( <Expression> );\n')

	if Input[counter][0] == '(':
		Print(Input[counter][1],Input[counter][0])
		counter += 1
		Print(Input[counter][1],Input[counter][0])

	else:
		print("Line {}:: Error: \'(\' expected".format(Input[counter][2]+1))
		exit(1)

	Expression()
	
	if Input[counter][0] == ')':
		counter += 1

	else:
		print("Line {}:: Error: \')\' expected".format(Input[counter][2]+1))
		exit(1)

	if Input[counter][0] == ';':
		Print(Input[counter][1],Input[counter][0])
		counter += 1

	else:
		print("Line {}:: Error: \';\' expected".format(Input[counter][2]+1))
		exit(1)

#
#<Scanf> --> scanf ( <IDs> );
def Scanf():
	global counter
	global outfile

	outfile.write('<Scanf> --> scanf ( <IDs> );\n')

	if Input[counter][0] == '(':
		Print(Input[counter][1],Input[counter][0])
		counter += 1

	else:
		print("Line {}:: Error: \'(\' expected".format(Input[counter][2]+1))
		exit(1)

	IDs()

	if Input[counter][0] == ')':
		counter += 1

	else:
		print("Line {}:: Error: \')\' expected".format(Input[counter][2]+1))
		exit(1)

	if Input[counter][0] == ';':
		Print(Input[counter][1],Input[counter][0])
		counter += 1

	else:
		print("Line {}:: Error: \';\' expected".format(Input[counter][2]+1))
		exit(1)

#
#<Condition> --> <Expression> <Reloop> <Expression>
def Condition():
	global outfile

	Print(Input[counter][1],Input[counter][0])
	outfile.write('<Condition> --> <Expression> <Reloop> <Expression>\n')

	Expression()

	Reloop()
	
	Print(Input[counter][1],Input[counter][0])
	
	Expression()

#
#<Reloop> --> == | != | > | < | => | <=
def Reloop():
	global counter
	global outfile

	if Input[counter][0] in ('==', '!=', '>', '<', '=>', '<='):
		outfile.write('<Reloop> --> {}\n'.format(Input[counter][0]))
		counter += 1

	else:
		print("Line {}:: Error: ('==', '!=', '>', '<', '=>', '<=') expected for reloop in while loop".format(Input[counter][2]+1))
		exit(1)

#
#<Expression> --> <Term> <Expression Prime>
def Expression():
	global outfile

	outfile.write('<Expression> --> <Term> <Expression Prime>\n')

	Term()

	Expression_Prime()		

#
#<Expression Prime> --> {} <Term> <Expression Prime>
def Expression_Prime():
	global counter
	global outfile

	if Input[counter][0] in ('+', '-'):
		outfile.write('<Expression Prime> --> {} <Term> <Expression Prime>\n'.format(Input[counter][0]))
		counter += 1

		Print(Input[counter][1],Input[counter][0])

		Term()

		Expression_Prime()
	
	else:
		outfile.write('<Expression Prime> --> empty\n')
		pass

	if Input[counter][1] in ('int', 'real'):
		print("Line {}:: Error: \'*\',\'/\',\'+\',\'-\' expected".format(Input[counter][2]+1))
		exit(1)

#
#<Term> --> <Factor> <Term Prime>
def Term():
	global outfile

	outfile.write('<Term> --> <Factor> <Term Prime>\n')

	Factor()

	Print(Input[counter][1],Input[counter][0])

	Term_Prime()

#
#<Term Prime> --> {} <Factor> <Term Prime>
def Term_Prime():
	global counter
	global outfile

	if Input[counter][0] in ('*', '/'):
		outfile.write('<Term Prime> --> {} <Factor> <Term Prime>\n'.format(Input[counter][0]))
		counter += 1

		Factor()

		Print(Input[counter][1],Input[counter][0])

		Term_Prime()

	else:
		outfile.write('<Term Prime> --> empty\n')
		pass

	if Input[counter][1] in ('int', 'real'):
		print("Line {}:: Error: \'*\',\'/\',\'+\',\'-\' expected".format(Input[counter][2]+1))
		exit(1)

#
#<Factor> --> - <Primary> | <Primary>
def Factor():
	global counter
	global outfile

	if Input[counter][0] == '-':
		outfile.write('<Factor> --> - <Primary>\n')
		counter += 1

	else:
		outfile.write('<Factor> --> <Primary>\n')

	Primary()

#
#<Primary> --> <Identifier> <Primary Prime> | int | <Expression> | real | true | false 
def Primary():
	global counter 
	global outfile

	#if Expression is scoped with (  )
	if Input[counter][0] == '(':
		outfile.write('<Primary> --> (<Expression>)\n')
		Print(Input[counter][1],Input[counter][0])

		counter += 1

		Print(Input[counter][1],Input[counter][0])

		Expression()

		if Input[counter][0] == ')':
			counter += 1

		else:
			print("Line {}:: Error: \')\' expected".format(Input[counter][2]+1))
			exit(1)

	#token is an identifier
	elif Input[counter][1] == '<Identifier>':
		outfile.write('<Primary> --> <Identifier> <Primary Prime>\n')
		counter += 1

		Primary_Prime()

	#lexeme is true or false and if tokens are int or real type
	elif Input[counter][0] in ('true', 'false'):
		outfile.write('<Primary> --> {}\n'.format(Input[counter][0]))
		counter += 1

	elif Input[counter][1] in ('int', 'real'):
		outfile.write('<Primary> --> {}\n'.format(Input[counter][1]))
		counter += 1

	else:
		print("Line {}:: Error: Primary error".format(Input[counter][2]+1))
		exit(1)

#
#<Primary Prime> --> [ <IDs> ] | empty 
def Primary_Prime():
	global counter
	global outfile

	if Input[counter][0] == '[':
		Print(Input[counter][1],Input[counter][0])
		outfile.write('<Primary_Prime> --> [<IDs>]\n')
		counter += 1

		IDs() 

		if Input[counter][0] == ']':
			counter += 1

		else:
			print("Line {}:: Error: \']\' expected".format(Input[counter][2]+1))
			exit(1)

	else:
		outfile.write('<Primary_Prime> --> empty\n')
		pass

Input = [] #contains token and lexeme called by the lexer, which are in a list
counter = 0  #current position of our list which contains (token,lexeme) from the lexer()

OutfileName = sys.argv[1][:-4] + '-Syntax_Analyer_Output.txt'
outfile = open(OutfileName, 'w')


def main():
	InfileName = sys.argv[1]

	global line #used to keep track of current line for displaying errors

	#start current point to beginning of file
	cp = 0

	#empty character in token
	token = ''

	#return current pointer after is has read in chars and returned an accepting token.
	with open(InfileName, 'r') as infile:
		fileRead = infile.read()

		file_len = len(fileRead) - 1 #how long file is
		
		#handle EOF null character vs space character
		if fileRead[file_len] == ' ' or fileRead[file_len] == '\n':
			file_len -= 1

	#how long the file is and go through each token
	#dump token and lexemes from lexer all into a list as tuple information (*token*, *lexeme*)
	#list called "Input"
	while cp < file_len:
		#call our lexer
		cp, lexeme, token, line = lexer.lexer(cp, open(InfileName, 'r'))

		#ignore comments.  Call again.
		if lexeme in (' '):
			cp, lexeme, token, line = lexer.lexer(cp, open(InfileName, 'r'))

		if lexeme != ' ':

			#Add to a list
			Input.append((lexeme.strip(' \n'),token, line))


	global counter
	global outfile
	Rat12F()
	global table
	table.display()
	if counter == len(Input):
		print("\nCompleted: Source code read.  Result in file: '{}'\n".format(OutfileName))	
	else:
		print("\nError in source code. If no error message above then possible hidden bug.\nUnable to completely read\n")

if "__main__" == __name__:
	main()
