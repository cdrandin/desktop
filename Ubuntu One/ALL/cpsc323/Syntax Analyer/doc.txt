Christopher Randin
CSID: 894534346


<Problem statement>
Using the lexer problem before we follow a set of production rules which is to be valid.  In checking each lexeme to see if it matches with the grammar, expanding and reducing the production.  If the Syntax Analyzer goes through the whole process without interruption then the source code is syntactically correct.  Also, handling errors which is meaningful to the user to help easy in finding mistakes.


<How to use the program>
**BE AWARE** The end of the cource code file needs to end with a ' ' or '\n'.  Minor bug that I can't find, but this is how it will run.

I have made the program simple to work for 3.x python versions.
Simply to execute is as such,

RUN: >> python3 Syntax_Analyer.py simple.txt   

Program will prompt where the results have been posted, within the same directory.

IF FOR ANY REASON THE COMMAND LIST DOESN'T WORK
1) Make sure the files are in the same directory you are currently working with

<Design of your program>
Creating the multiple productions functions for our productions in our grammar.  Also, removing and left recursion and backtracking that our grammar might have.  Functions are all recursive, so this entire syntax check is recursive calls, possibility if code is too long and keeps recursing, recursion stack might overflow, but this will take a large amount of inner recursions to happen.


<Any Limitation>
None

<Any shortcomings>
None
