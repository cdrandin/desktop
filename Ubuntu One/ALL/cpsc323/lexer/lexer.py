#!/usr/bin/env python2.7

import sys
import os
#create FSM reading in file
#returns 2D array of the states
def createDFSM(filename):
	dfsm = []

	#Help creating demensions of the array
	#how many states are there and number of valid inputs
	with open(filename, 'r') as infile:
		e = infile.readlines()
		E = len(e)
		Q = len(e[0].split())

	#create array
	for i in range(E):
		dfsm.append([])
		for j in range(Q):
			dfsm[i].append(None)

	#counters for table
	col = 0 #max rows
	row = 0 #max len(line)
	
	#quickly open file. Scan through it. Then close.
	#add states into our table
	with open(filename, 'r') as infile:
		#read DFSM machine lexeme table
		#read file line by line
		for line in infile:
			#split the line into seperate pieces, removing spaces
			temp = line.split()
	
			#check each piece of the lexemes
			for char in temp:
				#add to dfsm table
				dfsm[col][row] = char
				row += 1
				if row >= len(temp): row = 0
			col += 1

	return dfsm

#create tables and stored in 2D array.
#to access according to DFSM table  fsm[col][row]
#prepare to use globally
int_and_real_fsm=createDFSM('Int_and_Real_DFSM.txt')
id_fsm=createDFSM('Identifier_DFSM.txt')

seperators = (';',':',',','(',')','[',']','{','}')
operators  = ('+','-','=','>','<','*','/','>=','<=','.','!=','%', '!')
keywords   = ('int','if','else','fi','while','return','scanf','printf','for','long','float','void','double','continue','break','const', 'function', 'boolean', 'True', 'False')
end        = (' ', '\n')
	
#save line number to be used for displaying error messages
lineNumber = 0

#make sure file is given.
#use file and make output file with the name of the entiered file into [filename]'-output.txt' file
try:	
	InfileName = sys.argv[1]
	open(InfileName, 'r')

except IndexError:
	sys.exit("Please enter a file.")

except IOError:
	sys.exit("There is no file named \"{}\"".format(sys.argv[1]))

# luxer handles seperating each unique lexeme. Then return the lexeme and its type
def lexer(recentPos, infile = sys.argv[1]):

	#make our constant declared tuples static
	__slot__ =['operators','keywords','seperator']

	#constants. Tuples(hash table) statically declared.  Used to check lexemes for special lexemes read in.
	global seperators 
	global operators  
	global keywords  
	global end       

	#declare or current values to be global
	global int_and_real_fsm
	global id_fsm
	
	global lineNumber #find current line number

	#start state to 0
	state = 0

	#create a null lexeme value to keep track of lexeme sets
	lexeme = ''

	#create a null value for returning the type
	token = ''

	#used to keep track of current file pointer. Pass through function parameter
	cp = recentPos

	#used for seperating operators when reading in a valid lexeme
	lexeme_read = False

	#used to determine when comments are being used and should ignored anything in between
	ignore = False

	#determins if lexeme is a possible ID
	isID = False

	#start at where recently left off
	infile.seek(cp)

	#continue reading the file till an acceptable lexeme is read. Else brake from the whole function. Display error.
	while True:
		#read in a character.
		char = infile.read(1)

		#find current file pointer location
		cp = infile.tell()

		#check if comment is forgotten. Handles infinite comment loop.
		with open(infile.name, 'r') as f:
			fileRead = f.read()
			file_len = len(fileRead) - 1 
			if cp == file_len and ignore:
				print("EOF missing end comment '*/'")
				exit(1)

		#Used to ignore special comments '/*' '*/'
		#Also, used to keep track of division '/'
		if char == '/':
			temp = infile.read(1)

			#comments
			if temp == '*':
				ignore = True

			#handle division. Seperate from integers
			elif temp == ' ' or temp.isdigit():
				cp = infile.seek(infile.tell() - 1)

			#if neither one. 
			else:
				cp = infile.seek(infile.tell() - 1)
				break

		#if a comment command has been entered. ignore all previous lines after it. Setting every input as nothing.
		if ignore:
			if char == '*':
				if infile.read(1) == '/':
					ignore = False
					char = ''
					lexeme = ''
					token = ''
				else:
					cp = infile.seek(infile.tell() - 1)	

		#no longer in between comments
		if not ignore:
			#if there is a space. Handle it.  Also helps for end of file, which must have a space at the end.
			if char in end:
				if char == '\n':
					lineNumber += 1

				lexeme += ' '
				break

			#if our lexeme is a digit, a letter, '.', or '_'.  Used to handle our expected values.
			elif (char.isdigit() or char == '.' or char.isalpha() or char == '_'):
				lexeme_read = True

				#handle real and integer values
				if (char.isdigit() or char == '.') and not isID:
					#depending on our input, determine which state is to be valid.
					if char.isdigit(): col = 0
					elif char == '.': 
						col = 1
	
					#accepting state for int_and_ream_fsm
					F = (1,3)

					#change current state after receiving our input	
					state = int(int_and_real_fsm[col][state])

					lexeme += char

				#handle valid identifiers
				elif char.isalpha() or char == '_' or char.isdigit():
					isID = True
					#depending on our input, determine which state is to be valid.
					if char.isalpha(): col = 0
					elif char.isdigit(): col = 1
					elif char == '_': col = 2

					#accepting state for id_fsm
					F = (1,5)

					#change current state after receiving our input	
					state = int(id_fsm[col][state]) 
					lexeme += char

				#check first char in our lexeme string.
				#handling special cases of reals
				if lexeme[0].isdigit():

					#traverse the lexeme string, making sure it is a valid real.
					for i in lexeme:
						if not i.isdigit():
							#if there real is incorrect format like '23.34' return error
							if i != '.':
								print('Line {}:: Error incorrect format >> {}'.format(lineNumber, lexeme))
								exit(1)

					#handling if lexeme was '.23', without leading 0 in front, yet still valid.
					if '.' in lexeme: 
						token = 'real'
						#if not a valid real, in other words. lexeme is '23.'  it should terminate for being invalid lexeme.
						#if correct and lexeme would be '23.0' accept as value
						if state not in F:
							#check next character.
							if infile.read(1).isdigit():
								#return to the previous location in the file b/c we read the next char in the file.
								cp = (infile.seek(infile.tell()-1))
							else:
								print('Line {}:: Error incorrect format >> {}'.format(lineNumber, lexeme))
								exit(1)

					#if not real then must be integer
					else:
						token = 'int'

				#determine if identifier is unique or keyword
				if lexeme[0].isalpha():
					if lexeme in keywords:
						token = 'Keyword'
					else:
						token = '<Identifier>'

			#handle seperators. If seperator reach. Stop their return the lexeme. Start back at correct position.
			#used to handle words followed by seperators with no spaces   e.x) while()
			elif char in seperators:
				lexeme += char
				if lexeme_read:
					cp -= 1
					lexeme = lexeme[:-1]
					lexeme_read = False
				break

			#check operators. Seperate them from the valid lexemes.
			elif char in operators:
				#if an operator is read while still appending to a valid lexeme.
				#cut lexeme. Return it. Come back to the operator.
				if len(lexeme)>0:
					cp = (infile.seek(infile.tell()-1))
				else:
					lexeme += char	
				break


	#Before sending out lexeme. Check if current state is in the accepting final states
	#If not return error and the incorrect lexeme argument
	if isID and not state in F:
		print('Line {}:: Error incorrect format >> {}'.format(lineNumber, lexeme))
		exit(1)

	#determines between seperators and operators.
	#
	if lexeme[0] in seperators:
		token = 'seperator'

	elif lexeme[0] in operators:
		token = 'operator'
		temp = infile.read(1)

		#handle mathemtical operators to seperate them
		if lexeme[0] in ('+','-','/','*'):
			cp = (infile.seek(infile.tell()-1))


		#keep track of compound operators.
		#
		#if there is an operators need to a digit. Seperate them so they don't get stuck together.
		#reset pointer 1 char back
		elif temp.isdigit():
			cp = (infile.seek(infile.tell()-1))

		#handle operators followed by a letter.  EX) ">height" seperate '>' and 'height'
		elif temp.isalpha():
			cp = (infile.seek(infile.tell()-1))

		#if next char in the file is compound add more to the lexeme. Incr counter pointer.
		elif temp not in ('<=','>=','!='):
			lexeme += temp
			cp+=1
		#if we check next char in the file and is not compound, return to that pointer
		else:
			cp = (infile.seek(infile.tell()-1))

		#if there is an operator in the front of the lexeme. Assume it will be real. 
		#Check 2nd character in lexeme is digit, if so then it is real
		if len(lexeme)>1:
			#handle '.23'type format. If missing 0s before '.', yet still valid.
			if lexeme[1].isdigit():
				token = 'real'
	
				#seperate operators or seperators stuck to the lexeme
				#return as whole float number and return its string
				lexeme = str(float(lexeme[:len(lexeme)-1])) 
				cp = (infile.seek(infile.tell()-1))

	#Used to handle comments. since cp returns nothing. keep track of pointer when it is waiting for end comment
	if cp == None: cp = infile.tell()

	return cp, lexeme, token, lineNumber

	
def main():
	#make sure file is given.
	#use file and make output file with the name of the entiered file into [filename]'-output.txt' file
	try:	
		InfileName = sys.argv[1]
		OutfileName = InfileName[:-4] + "-output.txt"

	except IndexError:
		print("Missing file. Enter file in command prompt. EX) ./lexer.py file[*number*].txt")
		exit(1)

	#start current point to beginning of file
	cp = 0

	#empty character in lexeme
	lexeme = ''

	#return current pointer after is has read in chars and returned an accepting lexeme.
	with open(InfileName, 'r') as infile:
		fileRead = infile.read()
		file_len = len(fileRead) - 1 
		
		#handle EOF null character vs space character
		if fileRead[file_len] == ' ' or fileRead[file_len] == '\n':
			file_len -= 1

	with open(OutfileName, 'w') as outfile:
		outfile.write("{0:<16}  {1} ".format('lexeme', 'lexeme') + '\n\n')
		while cp < file_len:
			cp, lexeme, token= lexer(cp, open(InfileName, 'r'))

			#ignore spaces and end-character that has been returned
			#print to file neatly columed
			if lexeme not in (' ','\n'): 
				output = "{0:<16}  {1} ".format(token, lexeme) + ' \n'
				outfile.write(output)

		print("Output is in {}".format(OutfileName))

if __name__ == "__main__":
	main()
