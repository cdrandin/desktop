// meal.cpp //
// Programer: Kenny Van Alstyne //
// August 27, 2003 //
#include <iostream.h> // header file for cin and cout //
#include <iomanip.h> // header file for setprecision //
// begin //
main()
{
// declare some variables //
char name[40]; // your name //
// variables for equations //
float appetizer, entree, drink, desert, tip, meal, total, tax;
// ask some questions //
cout << "What is your name? ";
cin.get(name, 40);
cin.ignore(80, '\n');
cout << "How much was your appetizer? $";
cin >> appetizer;
cout << "How much was your entree? $";
cin >> entree;
cout << "How much was your drink? $";
cin >> drink;
cout << "How much was your desert? $";
cin >> desert;
// write some equations //
tip = 0.18;
tax = 0.07;
meal = (appetizer + entree + drink + desert);
total = ((meal * tip) * tax) + meal;
// give some output //
cout << "Your meal will cost $" << setprecision(4) << total << ". \n";
cout << "By: Kenny Van Alstyne";
return 0;
}


