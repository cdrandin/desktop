// Pro6-5.cpp - Project 6-5 //
// Programmer: Kenny Van Alstyne //
// August 27, 2003 //
#include <iostream.h> // header file for cout and cin //
#include <iomanip.h> // header file for setprecision //
// begin program //
main()
{
// declare some variables //
float inches, mat, width, length, total, inprice, mprice;
// ask for some input, makes 'em think they're doin' somethin' //
cout << "What is the length of the frame? ";
cin >> length;
cout << "What is the width of the frame? ";
cin >> width;
// write some equations... makes the cpu work for it's money //
inprice = 0.25;
mprice = 0.10;
inches = ((2 * length)+(2 * width)) * inprice;
mat = (length * width) * mprice;
total = inches + mat;
// give some output, makes users quite happy, indeed //
cout << "Your framing will cost " << setprecision(5) << total << ". \n";
cout << "By: Kenny Van Alstyne";
return 0;
}

