// ex72.cpp //
// Programmer: Kenny Van Alstyne //
// August 28, 2003 //
// header file //
#include <iostream.h>
// begin program //
main()
{
// declare some variables //
int i = 2;
int j = 3;
int true_false;
// write some statements //
true_false = (i < 3 && j > 3);
cout << "The result of (i < 3 && j > 3) is " << true_false << '\n';
true_false = (i < 3 && j >= 3);
cout << "The result of (i < 3 && j >= 3) is " << true_false << '\n';
cout << "The result of (i == 1 || i == 2) is "
	  << (i == 1 || i == 2) << '\n';
true_false = (j < 4);
cout << "The result of (j < 4) is " << true_false << '\n';
true_false = !true_false;
cout << "The result of !true_false is " << !true_false << '\n';
cout << "By: Kenny Van Alstne";
return 0;
}

