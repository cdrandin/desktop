// STest.CPP //
// Programmer: Kenny Van Alstyne //
// August 26, 2003 //
#include <iostream.h> // header file for cout and cin //
main()
{ // begin //
float t1; // grade on test 1 //
float t2; // grade on test 2 //
float t3; // grade on test 3 //
float t4; // grade on test 4 //
char name[40]; // student's name //
char cname[40]; // class' name //
float average; // average //
float div; // what to divide by //
// ask user for some input //
cout << "What is your name? ";
cin.get(name, 40);
cin.ignore(80, '\n');
cout << "What is the class name? ";
cin.get(cname, 40);
cin.ignore(80, '\n');
cout << "What did you make on test 1? ";
cin >> t1;
cout << "What did you make on test 2? ";
cin >> t2;
cout << "What did you make on test 3? ";
cin >> t3;
cout << "What did  you make on test 4? ";
cin >> t4;
// write an equation //
div = 4;
average = (t1 + t2 + t3 + t4) / div;
// output leads to a good mood //
cout << name << " has a " << average << " average in " << cname << ". \n";
cout << "By: Kenny Van Alstyne";
return 0;
}





