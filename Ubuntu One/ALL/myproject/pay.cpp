/* Kenny Van Alstyne
	September 23, 2003
	PAY */
#include <iostream.h>
#include <string.h>
#include <iomanip.h>
#include <stdio.h>
#define NumEntries 10
main()
{
float gpay[10],total[10],tax[10];
int x,y;
char name[10][12];
for(x=0; x<NumEntries; x++)
{
cout << "What is the last name? (Type end to end.) ";
cin >> name[x];
if(!strcmp(name[x], "end"))break;
cout << "What is the gross pay? $";
cin >> gpay[x];
tax[x]=((gpay[x] * .0765) + (gpay[x] * .18) + (gpay[x] * .03));
total[x]=(gpay[x] - tax[x]);
}
cout.setf(ios::right);
cout << setw(12) << "Name" << setw(12) << "Gross Pay" << setw(10) << "Taxes"
	<< setw(12) << "Net Pay\n";
for(y=0; y<NumEntries; y++)
{
cout.setf(ios::right);
cout << setw(12) << name[y] << setw(12) << gpay[y] << setw(10) << tax[y]
	<< setw(12) << total[y] << "\n";
}
cout << "By: Kenny Van Alstyne.";
return 0;
}

