// Kenny Van Alstyne //
// September 19, 2003 //
// Test 4a //
#include <iostream.h>
#include <iomanip.h>
#include <string.h>
#define NumEntries 6
main()
{
char fn[6][8];
char ln[6][12];
char spent[6][8];
int x;
int y;
cout << "Amount spent for week. \n";
for(x=0; x<NumEntries; x++)
{
cout << "What is your first name? ";
cin >> fn[x];
cout << "What is your last name? ";
cin >> ln[x];
cout << "How much did you spend? ";
cin >> spent[x];
}
cout << setw(10) << "First Name" <<setw(15) <<  "Last Name" << setw(15)
	<< "Amount Spent" << '\n';
for(y=0; y<NumEntries; y++)
{
cout << setw(10) << fn[y] << setw(15) << ln[y] << setw(15) << spent[y]
	<< "\n";
}
cout << "By: Kenny Van Alstyne";
return 0;
}


