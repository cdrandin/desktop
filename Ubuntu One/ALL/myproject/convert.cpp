// Convert //
// Kenny Van Alstyne //
// September 3, 2003 //
// header file //
#include <iostream.h>
// begin program //
main()
{
double data;
int choice;
float dollars;
cout << "Please enter the number of dollars you would like to convert.\n";
cout << "$";
cin >> dollars;
cout << "Choose to convert from the following \n";
cout << " 1 - German Deutsche Mark \n";
cout << " 2 - Japan Yan \n";
cout << " 3 - Russian Rubbles \n";
cout << " 4 - Mexico Paco \n";
cout << ": ";
cin >> choice;
switch(choice)
{
	case 1:
	data = (dollars * .5412);
	cout << "You converted from dollars to German Deutsche Mark.\n";
	cout << "The result was " << data << ". \n";
	break;
	case 2:
	data = (dollars * .0091);
	cout << "You converted from dollars to Japan Yen.\n";
	cout << "The result was " << data << ". \n";
	break;
	case 3:
	data = (dollars * .0412);
	cout << "You converted from dollars to Russian Rubbles.\n";
	cout << "The result was " << data << ". \n";
	break;
	case 4:
	data = (dollars * .9712);
	cout << "You converted from dollars to Mexico Paco.\n";
	cout << "The result was " << data << ". \n";
	break;
	default:
	data = 0;
	cout << "Invalid.";
	break;
}
cout << "By: Kenny Van Alstyne.";
return 0;
}


