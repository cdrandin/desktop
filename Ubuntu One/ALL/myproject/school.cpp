// School //
// Programmer:  Kenny Van Alstyne //
// August 21, 2003 //
#include <iostream.h>
main() // begin //
{
// declare some variables //
char sname[20]; // school name //
int fm; // freshman //
int sm; // sophomore //
int j;  // juniors //
int s;  // seniors //
int drive; // students driving to school //
int aver; // average number of students per grade //
int adrive; // average number of students driving to school //
int total;  // total students //
// ask some questions //
cout << "What is the name of your school? ";
cin >> sname;
cout << "How many freshman are in your school? ";
cin >> fm;
cout << "How many sophomores are in your school? ";
cin >> sm;
cout << "How many juniors are in your school? ";
cin >> j;
cout << "How many seniors are in your school? ";
cin >> s;
cout << "How many students drive to school? ";
cin >> drive;
// write some equations //
total = fm + sm + j + s;
aver = (fm + sm + j + s) / 4;
adrive = total / drive;
// output some stuff //
cout << "There are " << fm << " freshman. \n";
cout << "There are " << sm << " sophomores. \n";
cout << "There are " << j << " juniors. \n";
cout << "There are " << s << " seniors. \n";
cout << drive << " students drive to school \n";
cout << "You average " << aver << " students per grade. \n";
cout << "You average " << adrive << " students driving to school. \n";
cout << "By: Kenny Van Alstyne";
return 0;
}



