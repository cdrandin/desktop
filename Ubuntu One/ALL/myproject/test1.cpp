// TEST1.CPP  - FIRST COMPUTER TEST //
// Programmer: Kenny Van Alstyne //
// Date: August 22, 2003 //
// header file //
#include <iostream.h>
// begin the program //
main()
{
// declare some variables //
float classes;  // supplies for classes //
float clothes;  // clothes //
float shoes;  // shoes //
float bb;  // book bag //
float acces; // accesories //
float total;  // total spent //
float remainder;  // money left over //
float money;  // how much money you have //
money = 600;
// ask the people some questions //
cout << "You have $" << money << " to spend. \n";
cout << "How much did you spend on school supplies for classes? $";
cin >> classes;
cout << "How much did you spend on new shoes? $";
cin >> shoes;
cout << "How much did you spend on new clothes? $";
cin >> clothes;
cout << "How much did you spend on a book bag? $";
cin >> bb;
cout << "How much did you spend on accessories? $";
cin >> acces;
// write some equations //
total = classes + shoes + clothes + bb + acces;
remainder = money - total;
// give the user some output... that makes them happy //
cout << "You had $" << money << " to spend. \n";
cout << "You spent $" << classes << " on supplies for classes. \n";
cout << "You spent $" << shoes << " on new shoes. \n";
cout << "You spent $" << clothes << " on new clothes. \n";
cout << "You spent $" << bb << " on a new book bag. \n";
cout << "You spent $" << acces << " on accessories. \n";
cout << "You spent $" << total << " all together. \n";
cout << "You have $" << remainder << " left over. \n";
cout << "By: Kenny Van Alstyne";
return 0;
}


