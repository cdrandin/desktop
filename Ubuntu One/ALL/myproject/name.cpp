/* Kenny Van Alstyne
	Name
	October 7, 2003 */
#include<iostream.h>
#include <string.h>
#include <iomanip.h>
#include <stdio.h>
#define NumEntries 5
main()
{
int x,y;
char fname[5][8],lname[5][12],bdate[5][10],age[5][5];
for(x=0; x<NumEntries; x++)
{
cout << "What is the first name? (Type end to end.) ";
cin >> fname[x];
if(!strcmp(fname[x], "end"))break;
cout << "What is the last name? ";
cin >> lname[x];
cout << "What is the birthdate? (MM-DD-YY) ";
cin >> bdate[x];
cout << "What is the age? ";
cin >> age[x];
}
cout.setf(ios::right);
cout << setw(12) << "First Name" << setw(12) << "Last Name" << setw(15) 
<<
	"B-Date" << setw(8) << "Age\n";
for(y=0; y<NumEntries; y++)
{
cout.setf(ios::right);
cout << setw(12) << fname[y] << setw(12) << lname[y] << setw(15) << 
bdate[y]
	<< setw(8) << age[y] << "\n";
}
cout << "By: Kenny Van Alstyne.";
return 0;
}
