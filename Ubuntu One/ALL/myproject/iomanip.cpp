// IOMANIP.CPP //
// Programmer: Kenny Van Alstyne //
// August 25, 2003 ///
#include <iostream.h>
#include <iomanip.h>

// begin //
main()
{
// declare some variables //
int i = 1499;
int j = 618;
int k = 2;
float a = 34.87432;

cout << setw(10) << i << setw(10) << j << setw(10) << k << endl;
cout << setw(10) << k << setw(10) << i << setw(10) << j << endl;

cout << setprecision(3) << setw(10) << a << endl;
cout << "By: Kenny Van Alstyne";
return 0;
}

