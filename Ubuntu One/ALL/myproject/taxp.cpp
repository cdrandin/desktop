/* Kenny Van Alstyne
	September 23, 2003
	TAXP */
#include <iostream.h>
#include <string.h>
#include <iomanip.h>
#include <stdio.h>
#define NumEntries 5
main()
{
float stotal[5],tax[5],total[5];
int x,y;
char fname[5][8],lname[5][12];
for(x=0; x<NumEntries; x++)
{
cout << "What is the first name? (Type end to end.) ";
cin >> fname[x];
if(!strcmp(fname[x], "end"))break;
cout << "What is the last name? ";
cin >> lname[x];
cout << "What is the subtotal of the grocery bill? $";
cin >> stotal[x];
tax[x]=(stotal[x] * 0.07);
total[x]=(stotal[x] - tax[x]);
}
cout.setf(ios::right);
cout << setw(12) << "First Name" << setw(12) << "Last Name" << setw(10) << "Tax"
	<< setw(12) << "Total\n";
for(y=0; y<NumEntries; y++)
{
cout.setf(ios::right);
cout << setw(12) << fname[y] << setw(12) << lname[y] << setw(10) << tax[y]
	<< setw(12) << total[y] << "\n";
}
cout << "By: Kenny Van Alstyne.";
return 0;
}
