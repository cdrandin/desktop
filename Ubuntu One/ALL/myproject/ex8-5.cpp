// Ex. 8-5 //
// Kenny Van Alstyne //
// September 4, 2003 //
#include <iostream.h>
main()
{
float num;
cout  << "Please enter the number to divide:";
cin >> num;
while (num > 1.0)
{
cout << num << '\n';
num = num / 2;
}
cout << "By: Kenny Van Alstyne.";
return 0;
}
