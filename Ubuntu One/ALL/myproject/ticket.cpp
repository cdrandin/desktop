// Speeding Ticket //
// Programmer:  Kenny Van Alstyne //
// August 19, 2003 //
using namespace std;
main ()  // begin //
{
// declare some variables //
int speed,sl,fine;
// Ask some questions //
cout << "What was the speed limit? ";
cin >> sl;
cout << "How fast were you going? ";
cin >> speed;
// write an equation //
fine = 40 + ((speed-sl)*5);
// give the user some output //
cout << "Your fine will be $" << fine << '\n';
cout << "By: Kenny Van Alstyne";
return 0;
}  // the end //
