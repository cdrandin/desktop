// ex75.cpp //
// Programmer: Kenny Van Alstyne //
// August 28, 2003 //
// header files //
#include <iostream.h>
// begin program //
main()
{
char city_name[25];
unsigned long population;
cout << "What is the name of your city or town? ";
cin.get(city_name, 25);
cin.ignore(80, '\n');
cout << "What is the population of the city or town? ";
cin >> population;
if (population >= 171439)
{
cout << "According to the 1990 census, " << city_name
<< " is one of\nthe 100 largest U.S. cities.\n";
}
else
{
cout << "According to the 1990 census, " << city_name
<< " is not one of\nthe 100 largest U.S. cities.\n";
}
cout << "By: Kenny Van Alstyne";
return 0;
}
