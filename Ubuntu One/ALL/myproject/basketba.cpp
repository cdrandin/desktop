/* Kenny Van Alstyne
	September 24, 2003
	Basketball */
#include <iostream.h>
#include <stdio.h>
#include <string.h>
#include <iomanip.h>
#define NumEntries 8
main()
{
char date[8][8], team[8][20], time[8][6];
int x, y;
for(x=0;x<NumEntries;x++)
{
cout << "What is the date of the game? Type end to end. ";
cin.get(date[x], 8);
cin.ignore(80, '\n');
if(!strcmp(date[x], "end"))break;
cout << "What is the name of the team? ";
cin.get(team[x], 20);
cin.ignore(80, '\n');
cout << "What is the time of the game? ";
cin.get(time[x], 6);
cin.ignore(80, '\n');
}
cout.setf(ios::right);
cout << setw(8) << "Date" << setw(20) << "Team" << setw(15) << "Time \n";
for(y=0; y<NumEntries; y++)
{
cout.setf(ios::right);
cout << setw(8) << date[y] << setw(20) << team[y] << setw(15) << time[y]
	<< '\n';
}
cout << "By: Kenny Van Alstyne.";
return 0;
}
