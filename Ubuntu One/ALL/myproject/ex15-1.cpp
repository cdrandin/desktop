/* Ex 15-1
	Kenny Van Alstyne
	September 29, 2003 */
#include<iostream.h>
#include<iomanip.h>
main()
{
float United_States[10]={264.32, 264.11, 264.03, 263.39, 262.79, 262.65,
						262.04, 262.38, 261.87, 261.93};
float Japan[10]={24.23, 24.23, 24.23, 24.23, 24.23, 24.23, 24.23, 24.23,
						24.23, 24.23};
float Germany[10]={95.18, 95.18, 95.18, 95.18, 95.18, 95.18, 95.18, 95.18,
							95.18, 95.18};
float United_Kingdom[10]={18.84, 19.03, 19.01, 19.01, 19.03, 19.03, 19.01,
						19.01, 19.00, 18.99};
int index;
cout.setf(ios::showpoint);
cout.setf(ios::fixed);
for(index = 0; index <= 9; index++)
{
cout << "   " << (1980 + index)
		<< setw(15) << United_States[index]
		<< setw(15) << Japan[index]
		<< setw(15) << Germany[index]
		<< setw(15) << United_Kingdom[index] << '\n';
}
cout << "By: Kenny Van Alstyne.";
return 0;
}

