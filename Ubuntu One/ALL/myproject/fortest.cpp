/* Kenny Van Alstyne
	September 26, 2003
	ForTest */
#include<iostream.h>
#include<stdio.h>
#include<iomanip.h>
#include<string.h>
#define NumEntries 10
main()
{
float cnumber[10];
float bbalance[10];
float withdrawels[10];
float deposits[10];
float ebalance[10];
int x;
int y;
cout << "Program to Calculate the Balances of 10 Accounts\n";
for(x=0; x<NumEntries; x++)
{
cout << "What is the client's number? ";
cin >> cnumber[x];
cout << "What is the beginning balance? $";
cin >> bbalance[x];
cout << "What is the total value of deposits? $";
cin >> deposits[x];
cout << "What is the total value of withdrawals? $";
cin >> withdrawels[x];
ebalance[x]=(bbalance[x] - withdrawels[x] + deposits[x]);
}
cout.setf(ios::right);
cout << setw(8) << "Client #" << setw(15) << "Beg. Balance" << setw(15) <<
	"Deposits" << setw(15) << "Withdrawels" << setw(18) << "Ending Balance\n";
for(y=0; y<NumEntries; y++)
{
cout.setf(ios::right);
cout << setw(8) << cnumber[y] << setw(15) << bbalance[y] << setw(15) <<
	deposits[y] << setw(15) << withdrawels[y] << setw(15) << ebalance[y] << '\n';
}
cout << "By: Kenny Van Alstyne.\n";
return 0;
}

