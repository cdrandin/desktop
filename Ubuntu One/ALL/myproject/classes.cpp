// Health Class //
// Programmer:  Kenny Van Alstyne //
// August 18, 2003 //
#include <iostream.h> // Header file //
main() // Let's begin //
{
// Declare some variables //
int sclass; // number of maximum students in each class //
int taking; // number of larger classes //
int students; // students taking health //
int remainder; // students in the smaller class //
sclass = 30;
cout << "How many students will be going into health class? \n";
cin >> students;
taking = students / sclass;
remainder = students % sclass;
cout << "There will be " << taking << " classes with 30 students. \n" ;
cout << "The last class will have " << remainder << " students. \n" ;
cout << "By:  Kenny Van Alstyne";
return 0;
}

