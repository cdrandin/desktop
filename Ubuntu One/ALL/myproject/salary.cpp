/* Kenny Van Alstyne
	September 22, 2003
	Salary */
#include <iostream.h>
#include <iomanip.h>
#include <string.h>
#include <stdio.h>
#define NumEntries 5
main()
{
float hours[5],pay[5],netpay[5];
int x,y;
char name[5][8];
for(x=0; x<NumEntries; x++)
{
cout << "What is the name? (Type end to end.) ";
cin >> name[x];
if(!strcmp(name[x], "end"))break;
cout << "What is the hourly rate? ";
cin >> pay[x];
cout << "What is the amount of hours worked? ";
cin >> hours[x];
netpay[x]=(pay[x] * hours[x]);
}
cout.setf(ios::right);
cout << setw(12) << "Name" << setw(12) << "Hours" << setw(10) << "Pay"
	<< setw(12) << "Total Pay\n";
for(y=0; y<NumEntries; y++)
{
cout.setf(ios::right);
cout << setw(12) << name[y] << setw(12) << hours[y] << setw(10) << pay[y]
	<< setw(12) << netpay[y] << "\n";
}
cout << "By: Kenny Van Alstyne.";
return 0;
}

