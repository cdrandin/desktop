// Christmas //
// Kenny Van Alstyne //
// 9-9-3 //
#include <iostream.h>
#include <iomanip.h>
main()
{
const char ERROR = '\O';
int choice;
float item = 0;
float total;
float left;
char name[30];
cout << "What is your name? ";
cin.get(name, 30);
cin.ignore(80, '\n');
do
{
cout << "Choose from the following Christmas list ...\n";
cout << "1 - Barbie Dolls - $9.50\n";
cout << "2 - Truck - $14.95\n";
cout << "3 - Legos - $7.95\n";
cout << "4 - Necklace - $15.00\n";
cout << "5 - Disney Video - $14.95\n";
cout << "6 - Monopoly Game - $17.00\n";
cout << "7 - Playstation Game - $49.99\n";
cout << "8 - Power Ranger - $16.95\n";
cout << "Enter 9 When You Are Complete!\n";
cin >> choice;
switch(choice)
{
	case 1:
	item = item + 9.5;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 2:
	item = item + 14.95;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 3:
	item = item + 7.95;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 4:
	item = item + 15;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 5:
	item = item + 14.95;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 6:
	item = item + 17;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 7:
	item = item + 49.99;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 8:
	item = item + 16.95;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 9:
   total = item;
   left = 500 - total;
	cout << name <<"'s total is $"<< setprecision(3) << total << ". \n";
   cout << "You have $" << left << " left. \n";
	default:
	item = 0;
	break;
	}
}
while (choice==ERROR);
cout << "By: Kenny Van Alstyne.";
return 0;
}
