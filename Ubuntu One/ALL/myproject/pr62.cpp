// PROJECT62.CPP //
// Programmer: Kenny Van Alstyne //
// August 25, 2003 //
// header files //
#include <iostream.h>
#include <iomanip.h>

main() // begin //
{
// declare some very-ables //
float a = 123.443;
float b = 33.22;
float c = 1.9;
// output //
cout << setprecision(4) << a << setprecision(3) << setw(10) << b <<
	setprecision(2) << setw(10) << c << '\n';
cout << "By: Kenny Van Alstyne";
return 0;
}


