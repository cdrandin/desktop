// Family //
// Programmer:  Kenny Van Alstyne //
// August 20, 2003 //
// header files //
#include <iostream.h>
#include <string.h>
// begin //
main()
{
// declare some variables //
char fname[11];
char lname[15];
char people[5];
float groceries;
float gas;
float clothes;
float entertain;
float total;
// Ask some questions //
cout << "What is your first name? ";
cin >> fname;
cout << "What is your last name? ";
cin >> lname;
cout << "How many people are in your family? ";
cin >> people;
cout << "How much money does your family spend for groceries each week? $";
cin >> groceries;
cout << "How much money does your family spend each week for gas? $";
cin >> gas;
cout << "How much money does your family spend for clothes each week? $";
cin >> clothes;
cout << "How much money does your family spend for entertainment each week? $";
cin >> entertain;
// write an equation //
total = groceries + gas + clothes + entertain;
// give some output //
cout << "Your first name is " << fname << '\n';
cout << "Your last name is " << lname << '\n';
cout << "You have " << people << "people in your family \n" ;
cout << "You spend $" << total  << " each week. \n";
cout << "By: Kenny Van Alstyne";
return 0;
}

