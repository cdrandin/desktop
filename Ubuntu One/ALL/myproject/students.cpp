// STUDENTS //
// Kenny Van Alstyne //
// September 12, 2003 //
#include <iostream.h>
#include <iomanip.h>
#include <string.h>
#define NumEntries 10
main()
{
char fn[10][20];
char ln[10][20];
char g[10][5];
char s[10][20];
int b;
int k;
for(b=0; b<NumEntries; b++)
{
cout << "What is the student's first name? ";
cin >> fn[b];
if(!strcmp(fn[b], "end"))break;
cout << "What is the student's last name? ";
cin >> ln[b];
cout << "What grade is the student in? ";
cin >> g[b];
cout << "What is the student's favorite subject? ";
cin >> s[b];
}
cout << setw(10) << "First Name" << setw(20) << "Last Name" << setw(10)
	<< "Grade" << setw(20) << "Fav. Subject \n";
for(k=0; k<b; k++)
{
cout << setw(10) << fn[k] << setw(20) << ln[k] << setw(10) << g[k]
	<< setw(20) << s[k] << '\n';
}
cout << "By: Kenny Van Alstyne";
return 0;
}
