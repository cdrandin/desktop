// Datatype.CPP //
// Kenny Van Alstyne //
// August 14, 2003 //

#include <iostream.h>

main()
{
	// Declare a constant for the square root of two
	const double SQUARE_ROOT_OF_TWO = 1.414214;

	int i;	// Declare I as an integer //
	long j;	// Declare J as a long integer //
	unsigned long k;	// Declare K as an unsigned long integer //
	float n;	// Declare N as a floating point number //

	i = 3;	// initialize i to 3 //
	j = -2048111;	// declare j //
	k = 4000000001;	// declare k //
	n = 1.887;	// Declare n //

	// Output constant and variables to screen //
	cout << SQUARE_ROOT_OF_TWO << '\n';
	cout << i << '\n';
	cout << j << '\n';
	cout << k << '\n';
	cout << n << '\n';
   cout << "By: Kenny Van Alstyne";
	return 0;
}
