// Movie //
// Kenny Van Alstyne //
// 9-8-3 //
#include<iostream.h>
#include<iomanip.h>
main()
{
float total;
int choice;
const char ERROR = '\O';
float item = 0;
char name[40];
int people;
cout << "What is your name? \n";
cin.get(name, 40);
cin.ignore(80, '\n');
cout << "How many people are going to the movies? \n";
cin >> people;
do
{
cout << "Choose from the following menu...\n";
cout << "1 - Movie - $5.25 \n";
cout << "2 - Popcorn - $2.25 \n";
cout << "3 - Coke - $1.50 \n";
cout << "4 - M & M's - $1.25 \n";
cout << "5 - Nachos - $2.00 \n";
cout << "6 - Pizza - $2.75 \n";
cout << "Enter 7 When You Are Complete!\n";
cin >> choice;
switch(choice)
{
	case 1:
	item = item + (5.25 * people);
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 2:
	item = item + 2.25;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 3:
	item = item + 1.50;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 4:
	item = item + 1.25;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 5:
	item = item + 2;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 6:
	item = item + 2.75;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 7:
	total = item * 1.07;
	cout << name <<"'s total is $"<< setprecision(3) << total <<'\n';
	default:
	item = 0;
   break;
	}
}
while (choice==ERROR);
cout << "By: Kenny Van Alstyne.";
return 0;
}
