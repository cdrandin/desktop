// interest.cpp //
// Programmer: Kenny Van Alstyne //
// August 27, 2003 //
#include <iostream.h> // header file for cin and cout //
// begin //
main()
{
// declare some vary-ables to make this thingy work //
float rate, total, principal, time, interest;
// ask some questions to make these people type! //
cout << "What is the principal? $";
cin >> principal;
cout << "What is the interest rate? ";
cin >> rate;
cout << "How many years is the loan for? ";
cin >> time;
// write some equations, may not work on 8088's  :-) FPU RECOMMENDED! //
interest = (principal * rate * time);
total = (interest + principal);
// give some... output //
cout << "Your interest is $" << interest << ". \n";
cout << "Your total amount to pay is $" << total << ". \n";
cout << "By: Kenny Van Alstyne";
return 0;
} // the end //

