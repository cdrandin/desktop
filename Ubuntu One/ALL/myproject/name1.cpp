/* Kenny Van Alstyne
	October 9, 2003
	name1.cpp */
#include<iostream.h>
#include<iomanip.h>
#include<string.h>
#include<stdio.h>
#define NumEntries 5
main()
{
char fname[5][15];
char lname[5][15];
char ffood[5][15];
char fstate[5][4];
int x;
int y;
for(x=0; x<NumEntries; x++)
{
cout << "What is the first name? Type end to end. ";
cin >> fname[x];
if(!strcmp(fname[x], "end"))break;
cout << "What is the last name? ";
cin >> lname[x];
cout << "What is the favorite food? ";
cin >> ffood[x];
cout << "What is the favorite state? ";
cin >> fstate[x];
}
cout << setw(15) << "First Name" << setw(15) << "Last Name" << setw(15) <<
	"Fav. Food" << setw(15) << "Fav. State\n";
for(y=0; y<NumEntries; y++)
{
cout << setw(15) << fname[y] << setw(15) << lname[y] << setw(15) <<
	ffood[y] << setw(15) << fstate[y] << "\n";
}
cout << "By: Kenny Van Alstyne.\n";
return 0;
}
