// REMAIN2.CPP //
// Programmer:  Kenny Van Alstyne //
// August 15, 2003 //
#include <iostream.h> // Header file, people //

main()
{
int dividend, divisor; // Declare some variables //

cout << "Enter the dividend ";
cin >> dividend;
cout << "Enter the divisor ";
cin >> divisor;

cout << "The quotient is " << dividend / divisor;
cout << " with a remainder of " << dividend % divisor << '\n';
cout << "By:  Kenny Van Alstyne";
return 0;
} // The End //
