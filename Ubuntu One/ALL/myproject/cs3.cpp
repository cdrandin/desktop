/* Road Mileage Finder
	Kenny Van Alstyne
	September 30, 2003 */
#include<iostream.h>
void get_cities(int &originating_city, int &destination_city);
int main()
{
int cities[10][10] = {
	{ 0, 1037, 674, 440, 795, 1398, 699, 2182, 841, 2618 },
	{ 1037, 0, 963, 840, 1748, 1949, 695, 2979, 206, 2976 },
	{ 674, 963, 0, 287, 917, 996, 266, 2054, 802, 2013 },
	{ 440, 840, 287, 0, 920, 1164, 259, 2179, 647, 2300 },
	{ 795, 1748, 917, 920, 0, 781, 1143, 1387, 1552, 2078 },
	{ 1398, 1949, 996, 1164, 781, 0, 1253, 1059, 1771, 1307 },
	{ 699, 695, 266, 259, 1143, 1253, 0, 2311, 637, 2279 },
	{ 2182, 2979, 2054, 2179, 1387, 1059, 2311, 0, 2786, 1311 },
	{ 841, 206, 802, 647, 1552, 1771, 637, 2786, 0, 2815 },
	{ 2618, 2976, 2013, 2300, 2078, 1307, 2279, 1131, 2815, 0 }
	};
int originating_city;
int destination_city;
char answer;
do
{
get_cities(originating_city, destination_city);
cout << "\nMileage = " << cities[originating_city][destination_city] << endl;
cout << "\nContinue? [Y]es [N]o: ";
cin >> answer;
}
while((answer=='y') || (answer=='Y'));
}
void get_cities(int &originating_city, int &destination_city)
{
cout << "Kenny Van Alstyne's Case Study III - Mileage\n";
cout << "\nOriginating City		Destination City\n";
cout << "------------------		-----------------\n";
cout << " 1 - Atlanta            	1 - Atlanta\n";
cout << " 2 - Boston            		2 - Boston\n";
cout << " 3 - Chicago            	3 - Chicago\n";
cout << " 4 - Cincinnati            	4 - Cincinnati\n";
cout << " 5 - Dallas            		5 - Dallas\n";
cout << " 6 - Denver            		6 - Denver\n";
cout << " 7 - Detroit            	7 - Detroit\n";
cout << " 8 - Los Angeles            	8 - Los Angeles\n";
cout << " 9 - New York            	9 - New York\n";
cout << " 10 - Seattle            	10 - Seattle\n";
cout << "\nOriginating City [1-10]: ";
cin >> originating_city;
cout << "\nDestination City [1-10]: ";
cin >> destination_city;
}

