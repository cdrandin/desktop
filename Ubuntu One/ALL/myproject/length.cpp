// Lengths //
// Kenny Van Alstyne //
// September  3, 2003 //
// header files //
#include <iomanip.h>
#include <string.h>
// begin program //
main()
{
double data;
int choice;
float feet;
cout << "What is the length in feet? \n";
cin >> feet;
cout << "What would you like to convert the length (in feet) to? \n";
cout << "1 - Inches\n";
cout << "2 - Meters\n";
cout << "3 - Yards\n";
cout << "4 - Kilometer\n";
cout << "5 - Miles\n";
cout << ": ";
cin >> choice;
switch(choice)
{
	case 1:
	data = (feet * 12);
	cout << "You converted from feet to inches.\n";
	cout << "The result is: " << data << ". \n";
	break;
	case 2:
	data = (feet * .30479999);
	cout << "You converted from feet to meters.\n";
	cout << "The result is: " << data << ". \n";
	break;
	case 3:
	data = (feet * .333333);
	cout << "You converted from feet to yards.\n";
	cout << "The result is: " << data << ". \n";
	break;
	case 4:
	data = (feet * .00030479);
	cout << "You converted from feet to kilometers.\n";
	cout << "The result is: " << data << ". \n";
	break;
	case 5:
	data = (feet * .00018939);
	cout << "You converted from feet to miles.\n";
	cout << "The result is: " << data << ". \n";
	break;
	default:
	data = 0;
	cout << "Invalid.\n";
	break;
}
cout << "By: Kenny Van Alstyne.\n";
return 0;
}


