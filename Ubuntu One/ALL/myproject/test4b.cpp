// Kenny Van Alstyne //
// September 19, 2003 //
// Test 4b //
#include <iostream.h>
main()
{
char sname[25];
float ss;
float clothes;
float food;
float stotal;
float tax;
float total;
cout << "What is your name? ";
cin.get(sname, 25);
cin.ignore(80, '\n');
cout << "How much did " << sname << " spend on school supplies? $";
cin >> ss;
cout << "How much did " << sname << " spend on clothes? $";
cin >> clothes;
cout << "How much did " << sname << " spend on food? $";
cin >> food;
stotal = (food + clothes + ss);
tax = (stotal * .07);
total = (stotal + tax);
cout << "Your name is " << sname << ". \n";
cout << "Your subtotal is $" << stotal << ". \n";
cout << "Your tax is $" << tax << ". \n";
cout << "Your total is $" << total << ". \n";
cout << "By: Kenny Van Alstyne";
return 0;
}
