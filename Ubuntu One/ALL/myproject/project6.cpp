// Project61 //
// Programmer:  Kenny Van Alstyne //
// August 25, 2003 //
// header files //
#include <iostream.h>
// begin //
main()
{
// declare some vary-ables //
char name[25]; // name //
char address[40]; // street address //
char csz[60]; // City,State,Zip //
char number[15]; // phone number //

// statements //
cout << "Enter your name: ";
cin.get(name, 25);
cin.ignore(80, '\n');
cout << "Enter your address: ";
cin.get(address, 40);
cin.ignore(80, '\n');
cout << "Enter your city, state, and zip: ";
cin.get(csz, 60);
cin.ignore(80, '\n');
cout << "Enter your phone number: ";
cin.get(number, 15);
cin.ignore(80, '\n');
// give some output //
cout << "Your name is: " << name << '\n';
cout << "Your address is: " << address << '\n';
cout << "Your city, state, and zip is: " << csz << '\n';
cout << "Your phone number is: " << number << '\n';
cout << "By: Kenny Van Alstyne";
return 0;
}



