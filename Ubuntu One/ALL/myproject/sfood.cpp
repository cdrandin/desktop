// Kenny Van Alstyne //
// September 15, 2003 //
// sfood //
#include <iostream.h>
#include <iomanip.h>
#include <string.h>
#define NumEntries 6
main()
{
char n[10][10]; // name //
char a[10][20]; // age //
char f[10][20];  // fav. food //
char s[10][20]; // fav. subject //
int b;
int k;
for(b=0; b<NumEntries; b++)
{
cout << "What is the student's name? ";
cin >> n[b];
if(!strcmp(n[b], "end"))break;
cout << "What is student's age? ";
cin >> a[b];
cout << "What is the student's favorite food? ";
cin >> f[b];
cout << "What is the student's favorite subject? ";
cin >> s[b];
}
cout << setw(5) << "Name" << setw(10) << "Age" << setw(15)
	<< "Fav. Food" << setw(20) << "Fav. Subject \n";
for(k=0; k<b; k++)
{
cout << setw(5) << n[k] << setw(10) << a[k] << setw(15) << f[k]
	<< setw(20) << s[k] << '\n';
}
cout << "By: Kenny Van Alstyne";
return 0;
}
