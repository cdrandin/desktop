// Subway Meal //
// Programmer:  Kenny Van Alstyne //
// August 18, 2003 //
#include <iostream.h> // Header file //
main() // main function //
{ // let's begin //
// declare some variables //
float total;  // total price //
float sandwich;  // sandwich price //
float drink;  // drink price //
float cookie;  // cookie price //
float chips;  // chips price //
float money; // How much money you have //
float change;  // Your change //
// Ask some questions //
cout << "How much was your sandwich? $";
cin >> sandwich;
cout << "How much was your drink? $";
cin >> drink;
cout << "How much was your cookie? $";
cin >> cookie;
cout << "How much were your chips? $";
cin >> chips;
cout << "How much money do you have? $";
cin >> money;
total = sandwich + drink + cookie + chips;
change = money - total;
cout << "Your meal at Subway will cost $" << total << '\n';
cout << "Your change is $" << change << '\n';
cout << "By: Kenny Van Alstyne";
return 0;
}

