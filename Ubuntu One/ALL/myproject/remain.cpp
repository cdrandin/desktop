// REMAIN.CPP //
// Programmer:  Kenny Van Alstyne //
// August 15, 2003 //

#include <iostream.h> // This is the header file... //

main()
{
int dividend, divisor, quotient, remainder; // Declare some variables //

cout << "Enter the dividend ";
cin >> dividend;
cout << "Enter the divisor ";
cin >> divisor;

quotient = dividend / divisor;
remainder = dividend % divisor;

cout << "The quotient is " << quotient;
cout << " with a remainder of " << remainder << '\n';
cout << "By: Kenny Van Alstyne";
return 0;
} // The End //