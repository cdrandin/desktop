// School //
// Kenny Van Alstyne //
// 9-8-3 //
#include<iostream.h>
#include<iomanip.h>
main()
{
float total;
int choice;
const char ERROR = '\O';
float item = 0;
char name[40];
cout << "What is your name? \n";
cin.get(name, 40);
cin.ignore(80, '\n');
do
	{
	cout << "Choose items from the list. \n";
	cout << "1 - Pencils $1.50 \n";
	cout << "2 - Paper $2.00 \n";
	cout << "3 - Binder $5.50 \n";
	cout << "4 - Markers $3.50 \n";
	cout << "5 - Pens $1.75 \n";
	cout << "6 - Bookbag $10.75 \n";
	cout << "7 - Crayons $1.35 \n";
   cout << "Enter 8 when finished! \n";
	cin >> choice;
	switch(choice)
	{
	case 1:
	item = item + 1.5;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 2:
	item = item + 2.0;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 3:
	item = item + 5.50;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 4:
	item = item + 3.5;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 5:
	item = item + 1.75;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 6:
	item = item + 10.75;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 7:
	item = item + 1.35;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 8:
	total = item * 1.07;
	cout << name <<"'s total is $"<< setprecision(3) << total <<'\n';
	default:
	item = 0;
   break;
	}
}
while (choice==ERROR);
cout << "By: Kenny Van Alstyne.";
return 0;
}


