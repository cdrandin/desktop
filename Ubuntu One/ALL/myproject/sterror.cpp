// STRERROR.CPP //
// Programmer:  Kenny Van Alstyne //
// August 20, 2003 //

#include <iostream.h>  // header files //
#include <string.h>
main() // Begin //
{
char student_name[21] = "Brandon Adkins"; // declare some varables //
char city[] = "Lubbock";
char state[3] = "TX";
char ZIP_code[11] = "79413-3640";

// output some stuff //
cout << student_name << '\n';
cout << city << '\n';
cout << state << '\n';
cout << ZIP_code << '\n';
// copy a string //
strcpy(state,"CA");
// output stuff again //
cout << student_name << '\n';
cout << city << '\n';
cout << state << '\n';
cout << ZIP_code << '\n';
cout << "By: Kenny Van Alstyne";
return 0;
}
