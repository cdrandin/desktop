// test2.cpp //
// Programer: Kenny Van Alstyne //
// August 28, 2003 //
// header files are good //
#include <iostream.h>
// begin program with main function //
main()
{
// declare some variables //
char cname[40]; // customer name //
float cpayment; // car payment //
float insure; // insurance price //
float gas; // how much spend on gas //
float access;  // car accesories price //
float spend;  // total spent each month //
float leftover; // remaining money //
float payment=1000; // amount person gets per month //
// ask some questions //
cout << "You have $" << payment << " to spend each month.\n";
cout << "What is the customer's name? \n";
cin.get(cname, 40);
cin.ignore(80, '\n');
cout << "How much is your car payment? \n$";
cin >> cpayment;
cout << "How much is your insurance? \n$";
cin >> insure;
cout << "How much do you spend on gas each month? \n$";
cin >> gas;
cout << "How much do you spend on accessories for your car \n each month? \n$";
cin >> access;
// write some equations //
spend = (cpayment + insure + gas + access);
leftover = payment - spend;
// give some output.. leads to happiness, generally //
cout << "Your name is " << cname << ". \n";
cout << "You had $" << payment << " to spend. \n";
cout << "You spent $" << cpayment << " on your monthly car payment. \n";
cout << "You spent $" << insure << " on your insurance. \n";
cout << "You spent $" << gas << " on gas each month. \n";
cout << "You spent $" << access << " each month on accessories for the car.\n";
cout << "Your total amount spent is $" << spend << ". \n";
cout << "You have $" << leftover << " leftover money. \n";
cout << "By: Kenny Van Alstyne";
return 0;
}



