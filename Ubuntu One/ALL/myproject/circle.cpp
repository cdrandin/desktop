// CIRCLE.CPP //
// Kenny Van Alstyne //
// August 14, 2003 //

#include <iostream.h>

main()
{
	const float PI = 3.14159;	//declare PI as a constant
	float circumferance, radius;

	//Ask user for the radius of a circle //
	cout << "What is the radius of the circle? ";
	cin >> radius;

	circumferance = 2 * PI * radius; // Calculate Circumferance //

	// Output the circle's circumferance //
	cout << "The circle's curcumferance is ";
	cout << circumferance << '\n';
	cout << "By: Kenny Van Alstyne";
	return 0;
}
