// Kenny Van Alstyne //
// September 17, 2003 //
// sports //
#include <iostream.h>
#include <iomanip.h>
#include <string.h>
#define NumEntries 10
main()
{
int choice;
char lname[10][15],fname[10][10],d[10][11],c[10][35];
int x, y;
for(x=0;x<NumEntries;x++)
{
cout<<"What is the student's first name? \n";
cout << " Type: 'end' to end.\n";
cin >> fname[x];
if(!strcmp(fname[x], "end"))break;
cout<<"What is the student's last name? \n";
cin >> lname[x];
cout << "Choose from the following menu. \n";
cout << " 1 - Baseball \n";
cout << " 2 - Soccer \n";
cout << " 3 - Football \n";
cout << " 4 - Basketball \n";
cout << " 5 - Hockey \n";
cout << "Choice: ";
cin >> choice;
switch(choice)
{
case 1:
strcpy(d[x],"Baseball");
strcpy(c[x],"You are a real slugger!");
break;
case 2:
strcpy(d[x],"Soccer");
strcpy(c[x],"You have a lot of self control.");
break;
case 3:
strcpy(d[x],"Football");
strcpy(c[x],"You like to work hard.");
break;
case 4:
strcpy(d[x],"Basketball");
strcpy(c[x],"You have a lot of skill.");
break;
case 5:
strcpy(d[x],"Hockey");
strcpy(c[x],"You like the ice.");
break;
default:
cout << "Error.\n";
break;
}
}
cout.setf(ios::right);
cout << setw(8) << "First Name" << setw(11) << "Last Name" << setw(11)
	<< "Sport" << setw(35) << "Comment \n\n";
for(y=0;y<x;y++)
{
cout << setw(8) << fname[y] << setw(11) << lname[y] << setw(11) <<
	d[y] << setw(35) << c[y] << '\n';
}
cout << "By: Kenny Van Alstyne";
return 0;
}
