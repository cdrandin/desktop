// Project 7-4 //
// Kenny Van Alstyne //
// September 2, 2003 //
// header file //
#include <iostream.h>
// begin program //
main()
{
// declare some variables //
int planet;  // vary-able for planet //
float pw;  // Planet Weight //
int weight; // your weight //
// ask some questions //
cout << "What is your weight on earth? \n";
cin >> weight;
cout << "Choose a planet to calculate your weight on: \n";
cout << " 1 - Mercury \n";
cout << " 2 - Venus \n";
cout << " 3 - Mars \n";
cout << " 4 - Jupiter \n";
cout << " 5 - Saturn \n";
cout << " 6 - Uranus \n";
cout << " 7 - Neptune \n";
cout << " 8 - Pluto \n";
cout << ": ";
cin >> planet;
switch(planet)
	{
	case 1:
	pw = (weight * 0.37);
	cout << "Your weight on Mercury is " << pw << " pounds. \n";
	break;
	case 2:
	pw = (weight * 0.88);
	cout << "Your weight on Venus is " << pw << " pounds. \n";
	break;
	case 3:
	pw = (weight * 0.38);
	cout << "Your weight on Mars is " << pw << " pounds. \n";
	break;
	case 4:
	pw = (weight * 2.64);
	cout << "Your weight on Jupiter is " << pw << " pounds. \n";
	break;
	case 5:
	pw = (weight * 1.15);
	cout << "Your weight on Saturn is " << pw << " pounds. \n";
	break;
	case 6:
	pw = (weight * 1.15);
	cout << "Your weight on Uranus is " << pw << " pounds. \n";
	break;
	case 7:
	pw = (weight * 1.12);
	cout << "Your weight on Neptune is " << pw << " pounds. \n";
	break;
	case 8:
	pw = (weight * 0.04);
	cout << "Your weight on Pluto is " << pw << " pounds. \n";
	break;
	default:
	pw = 0.00;
	break;
	}
cout << "By Kenny Van Alstyne.";
return 0;
}


