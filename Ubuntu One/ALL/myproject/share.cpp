// SHARE.CPP //
// Programmer: Kenny Van Alstyne //
// August 15, 2003 //

#include <iostream.h> // Header file //

main()
{
int number_of_people;	//Declaring some variables //
float money, share;

cout << "How many people need a share of the money? ";
cin >> number_of_people;
cout << "How much money is available to divide among the people? ";
cin >> money;

share = money / number_of_people;

cout << "Give each person $" << share << '\n';
cout << "By: Kenny Van Alstyne";
return 0;
}  // The End //
