/* Kenny Van Alstyne
	October 7, 2003
	Average */
#include<iostream.h>
main()
{
char fname[10], lname[15];
int eng, math, hist, sci;
float average, total;
cout << "What is your first name? ";
cin >> fname;
cout << "What is your last name? ";
cin >> lname;
cout << "What is your grade in English? ";
cin >> eng;
cout << "What is your grade in Math? ";
cin >> math;
cout << "What is your grade in history? ";
cin >> hist;
cout << "What is your grade in science? ";
cin >> sci;
total=(eng + math + hist + sci);
average=total/4;
cout << fname << " " << lname << "'s average grade is " << average << ". 
\n";
cout << "By: Kenny Van Alstyne.";
return 0;
}

