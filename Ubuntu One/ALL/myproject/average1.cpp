/* Kenny Van Alstyne
	October 9, 2003
	average1.cpp */
#include<iostream.h>
main()
{
char fname[15], lname[15];
int mon, tues, wed, thurs, fri, total;
float average;
cout << "What is your first name? ";
cin >> fname;
cout << "What is your last name? ";
cin >> lname;
cout << "How many hours were worked on Monday? ";
cin >> mon;
cout << "How many hours were worked on Tuesday? ";
cin >> tues;
cout << "How many hours were worked on Wednesday? ";
cin >> wed;
cout << "How many hours were worked on Thursday? ";
cin >> thurs;
cout << "How many hours were worked on Friday? ";
cin >> fri;
total = (mon + tues + wed + thurs + fri);
average = (total/5);
cout << fname << " " << lname << "'s average number of hours is " << average
	<< ".\n";
cout << "By: Kenny Van Alstyne.\n";
return 0;
}
