// Excercise 7-7 //
// Kenny Van Alstyne //
// September 2, 2003 //
// header file //
#include <iostream.h>
// begin program //
main()
{
// declare some vary-ables //
float amount_to_deposit;
// output and stuff //
cout << "How much do you want to deposit to open the account? $";
cin >> amount_to_deposit;
if(amount_to_deposit < 1000.00 )
	{
	if(amount_to_deposit < 100.00 )
		{ cout << "You should consider the EconoCheck account. \n"; }
	else
		{ cout << "You should consider the FreeCheck account. \n"; }
	}
else
	{ cout << "You should consider an interest-bearing account. \n"; }
cout << "By: Kenny Van Alstyne.";
return 0;
}
