// Transportation //
// Programmer:  Kenny Van Alstyne //
// August 15, 2003 //

#include <iostream.h> // Header file for cout and cin //

main()
{
int bus, people, remainder, people_bus; // Declare some variables //

cout << "How many people will be going? ";
cin >> people;

people_bus = 50;
bus = people / people_bus;
remainder = people % people_bus;

cout << "You will need to charter " << bus << " bus(es). \n";
cout << remainder << " people will need to go in vans. \n";
cout << "By: Kenny Van Alstyne";
return 0;
} // The end //