// ex71.cpp //
// Programmer: Kenny Van Alstyne //
// August 28, 2003 //
#include<iostream.h> // header file for cin and cout //
// begin program //
main()
{
// declare some vary-ables //
int i = 2;
int j = 3;
int true_false;
// make some statements //
cout << (i == 2) << '\n'; // displays a 1 (true) //
cout << (i == 1) << '\n'; // displays a 0 (false) //
cout << (j > i) << '\n';
cout << (j < i) << '\n';
cout << (j <= 3) << '\n';
cout << (j >= i) << '\n';
cout << (j != i) << '\n';
true_false = (j > 4);
cout << true_false << '\n';
cout << "By: Kenny Van Alstyne";
return 0;
}

