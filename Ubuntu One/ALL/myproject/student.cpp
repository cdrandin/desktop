// Student //
// Programmer: Kenny Van Alstyne //
// August 20, 2003 //
#include <iostream.h> // header files //
#include <string.h>

main() // begin //
{
// declare some variables //
char fname[11];
char lname[15];
char address[20];
char grade[3];
char gclass[20];
// Ask some questions //
cout << "What is your first name? ";
cin >> fname;
cout << "What is your last name? ";
cin >> lname;
cout << "What is your address? ";
cin >> address;
cout << "What grade are you in? ";
cin >> grade;
cout << "What is your favorite class? ";
cin >> gclass;
// output stuff //
cout << "You first name is " << fname << '\n';
cout << "Your last name is " << lname << '\n';
cout << "Your address is " << address << '\n';
cout << "Your grade is " << grade << '\n';
cout << "Your favorite class is " << gclass << '\n';
cout << "By: Kenny Van Alstyne";
return 0;
}


