// Excercise 7-8 //
// Kenny Van Alstyne //
// September 2, 2003 //
// header file //
#include <iostream.h>
// begin program //
main()
{
// declare some vary-ables //
int shipping_method;
float shipping_cost;
// the rest of the stuff //
cout << "How do you want the order shipped? \n";
cout << "1 - Ground\n";
cout << "2 - 2-Day Air\n";
cout << "3 - Overnight Air\n";
cout << "4 - Carrier Pigeon\n";
cout << "Enter the number of the shipping method you want: ";
cin >> shipping_method;
switch(shipping_method)
	{
	case 1:
	shipping_cost = 5.00;
	break;
	case 2:
	shipping_cost = 7.50;
	break;
	case 3:
	shipping_cost = 10.00;
	break;
	case 4:
	shipping_cost = 30.00;
   break;
	default:
	shipping_cost = 0.00;
	break;
	}
cout << "The cost of shipping will be " << shipping_cost << ". \n";
cout << "By: Kenny Van Alstyne.";
return 0;
}

