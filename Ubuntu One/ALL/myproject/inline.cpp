// INLINE.CPP //
// Programmer: Kenny Van Alstyne //
// August 25, 2003 //
#include <iostream.h>

main()
{
// declare some variables //
char name[21];

cout << "Enter a name: ";
cin.get(name, 21);
cin.ignore(80, '\n');
cout << "You entered " << name << '\n';
return 0;
}
