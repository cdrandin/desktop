/* Kenny Van Alstyne
	September 18, 2003
	CLASSPROJECT */
#include <iostream.h>
#include <iomanip.h>
#include <string.h>
#define NumEntries 10
main()
{
int choice;
char lname[10][15],fname[10][10],d[10][30],s[10][12];
int x, y;
cout << "********SILICON VALLEY********\n";
for(x=0;x<NumEntries;x++)
{
cout << "What is the first name? \n";
cin >> fname[x];
if(!strcmp(fname[x], "end"))break;
cout<<"What is the last name? \n";
cin >> lname[x];
cout << "Choose from the following menu. \n";
cout << " 1 - Head Director \n";
cout << " 2 - Assistant Director \n";
cout << " 3 - Applications Systems Analyst \n";
cout << " 4 - Quality Assurance Technician \n";
cout << " 5 - Business Analyst \n";
cout << " 6 - Software Engineer \n";
cout << " Type End At the First Name to End the Program! \n";
cout << "Choice: ";
cin >> choice;
switch(choice)
{
case 1:
strcpy(d[x],"Head Director");
strcpy(s[x],"$120,000");
break;
case 2:
strcpy(d[x],"Assistant Director");
strcpy(s[x],"$100,000");
break;
case 3:
strcpy(d[x],"Applications Systems Analyst");
strcpy(s[x],"$60,000");
break;
case 4:
strcpy(d[x],"Quality Assurance Technician");
strcpy(s[x],"$30,000");
break;
case 5:
strcpy(d[x],"Business Analyst");
strcpy(s[x],"$45,000");
break;
case 6:
strcpy(d[x],"Software Engineer");
strcpy(s[x],"$80,000");
break;
default:
cout << "Error.\n";
break;
}
}
cout.setf(ios::right);
cout << setw(10) << "First Name" << setw(15) << "Last Name" << setw(30)
	<< "Job" << setw(10) << "Salary \n\n";
for(y=0;y<x;y++)
{
cout.setf(ios::right);
cout << setw(10) << fname[y] << setw(15) << lname[y] << setw(30) <<
	d[y] << setw(10) << s[y] << '\n';
}
cout << "By: Kenny Van Alstyne";
return 0;
}
