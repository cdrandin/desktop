// test program for friday test //
// Programmer: Kenny Van Alstyne //
// August 28, 2003 //
// header files are a must for anything useful :-) //
#include <iostream.h>
// begin program with the main function //
main()
{
// declare some variables //
char a[40];
char b[40];
int c;
// ask some questions //
cout << "What is your street address? \n";
cin.get(a, 40);
cin.ignore(80, '\n');
cout << "What is your full name? \n";
cin.get(b, 40);
cin.ignore(80, '\n');
// give user some output.. seems to make them happy enough //
cout << "Your address is " << a << ". \n";
cout << "Your name is " << b << ". \n";
cout << "Almost done! \n";
// fun stuff //
cout << "Isn't this program redundant?! (1 for yes. 0 for no.) \n";
cin >> c;
if (c==1)
	{
	cout << "Indeed!\n";
	}
else
	{
	cout << "Well, YOU'RE WRONG!\n";
	}
cout << "By: Kenny Van Alstyne\n";
return 0;
}

