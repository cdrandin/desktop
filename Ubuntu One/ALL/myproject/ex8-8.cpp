// Ex. 8-8 //
// Kenny Van Alstyne //
// September 4, 2003 //
// header file //
#include <iostream.h>
//begin program //
const char ERROR = '\0';
main()
{
int num_reps;
int i;
int democrats = 0, republicans = 0, independents = 0;
char party;
cout << "\nHow many U.S. representatives does your state have? ";
cin >> num_reps;
cout << "Enter the party affiliation for each representative.\n";
cout << "Enter D for Democrat, R for Republican,\n";
cout << "and I for independents or other parties.\n";
for (i = 1; i <= num_reps; i++)
{
do
{
cout << "Party of representative #" << i << ": ";
cin >> party;
switch(party)
{
case 'D':
case 'd':
democrats++;
break;
case 'R':
case 'r':
republicans++;
break;
case 'I':
case 'i':
independents++;
break;
default:
cout << "Invalid entry.  Enter D, R, or I.\n";
party=ERROR;
break;
}
}while(party==ERROR);
}
cout << "Your state is represented by " << democrats << " Democrats, "
	  << republicans << " Republicans,\nand " << independents
	  << " independents and members of other parties.\n\n";
cout << '\n';
cout << "By: Kenny Van Alstyne.";
return 0;
}
