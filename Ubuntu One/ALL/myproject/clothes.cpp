/* Kenny Van Alstyne
	October 7, 2003
	Clothes */
#include <iostream.h>
#include <iomanip.h>
main()
{
const char ERROR = '\O';
int choice;
float item = 0;
float total;
do
{
cout << "Choose from the following Christmas list ...\n";
cout << "1 - Shirt - $42.00\n";
cout << "2 - Brown Belt - $25.00\n";
cout << "3 - Skirt - $20.00\n";
cout << "4 - Capris - $19.00\n";
cout << "5 - T-Shirt - $17.00\n";
cout << "6 - Shoes - $65.00\n";
cout << "7 - Black Belt - $20.00\n";
cout << "8 - Purse - $45.00\n";
cout << "Enter 9 When You Are Complete!\n";
cin >> choice;
switch(choice)
{
	case 1:
	item = item + 42;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 2:
	item = item + 25;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 3:
	item = item + 20;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 4:
	item = item + 19;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 5:
	item = item + 17;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 6:
	item = item + 65;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 7:
	item = item + 20;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 8:
	item = item + 45;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 9:
	total = item;
	cout << "Your total is $"<< setprecision(3) << total << ". \n";
	default:
	item = 0;
	break;
	}
}
while (choice==ERROR);
cout << "By: Kenny Van Alstyne.";
return 0;
}
