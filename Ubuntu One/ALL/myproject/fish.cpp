// FISH //
// Kenny Van Alstyne //
// September 10, 2003 //
#include<iostream.h>
#include<iomanip.h>
main()
{
const char ERROR = '\O';
int choice;
float item = 0;
float total;
char number[20];
char fname[30];
char lname[30];
cout << "What is your first name? ";
cin.get(fname, 30);
cin.ignore(80, '\n');
cout << "What is your last name? ";
cin.get(lname, 30);
cin.ignore(80, '\n');
cout << "What is your phone number? ";
cin.get(number, 20);
cin.ignore(80, '\n');
do
{
cout << "Choose from the following fish...\n";
cout << "1 - Angelfish - $4.99\n";
cout << "2 - Red Barbs - $5.99\n";
cout << "3 - Striped Catfish - $6.99\n";
cout << "4 - Goldfish - $1.99\n";
cout << "5 - Neon - $1.99\n";
cout << "6 - Tiger Barbs - $7.99\n";
cout << "7 - 25 Gallon Tank - $50.00\n";
cout << "8 - 50 Gallon Tank - $99.00\n";
cout << "9 - Filter - $25.00\n";
cout << "10 - Pump - $30.00\n";
cout << "11 - Gravel - $2.99\n";
cout << "Enter 12 When You Are Complete!\n";
cin >> choice;
switch(choice)
{
	case 1:
	item = item + 4.99;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 2:
	item = item + 5.99;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 3:
	item = item + 6.99;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 4:
	item = item + 1.99;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 5:
	item = item + 1.99;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 6:
	item = item + 7.99;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 7:
	item = item + 50.00;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 8:
	item = item + 99.00;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	case 9:
	item = item + 25.00;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	case 10:
	item = item + 30.00;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	case 11:
	item = item + 2.99;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 12:
	total = item * 1.07;
	cout << fname << " " << lname <<"'s total is $"<<
		setprecision(5) << total << ". \n";
	cout << "Your phone number is " << number << ". \n";
	default:
	item = 0;
	break;
	}
}
while (choice==ERROR);
cout << "By: Kenny Van Alstyne.";
return 0;
}