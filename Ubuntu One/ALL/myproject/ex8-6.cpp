// Ex. 8-6 //
// Kenny Van Alstyne //
// September 4, 2003 //
#include <iostream.h>
main()
{
float num, squared;
do
{
cout << "Enter a number (Enter 0 to quit.): ";
cin >> num;
squared = num * num;
cout << num << " squared is " << squared << '\n';
}
while (num != 0);
cout << "By: Kenny Van Alstyne.";
return 0;
}
