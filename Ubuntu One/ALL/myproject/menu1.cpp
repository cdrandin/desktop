// Menu1 //
// Kenny Van Alstyne //
// September 5, 2003 //
#include <iostream.h>
#include <iomanip.h>
main()
{
const char ERROR = '\O';
int choice;
float item = 0;
float total;
char name[30];
cout << "What is your name? ";
cin.get(name, 30);
cin.ignore(80, '\n');
do
{
cout << "Choose from the following menu...\n";
cout << "1 - Shrimp Poboy - $5.50\n";
cout << "2 - Hamburger - $2.50\n";
cout << "3 - Hot Dog - $2.25\n";
cout << "4 - Chili - $3.00\n";
cout << "5 - French Fries - $1.50\n";
cout << "6 - Onion Rings - $1.75\n";
cout << "7 - Coke - $0.75\n";
cout << "8 - Coffee - $0.50\n";
cout << "9 - Ice Cream - $1.55\n";
cout << "10 - Pie - $2.75\n";
cout << "Enter 11 When You Are Complete!\n";
cin >> choice;
switch(choice)
{
	case 1:
	item = item + 5.5;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 2:
	item = item + 2.5;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 3:
	item = item + 2.25;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 4:
	item = item + 3;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 5:
	item = item + 1.5;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 6:
	item = item + 1.75;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 7:
	item = item + .75;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 8:
	item = item + .50;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 9:
	item = item + 1.55;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 10:
	item = item + 2.75;
	cout <<"$"<<item<<'\n';
	choice=ERROR;
	break;
	case 11:
	total = item * 1.07;
	cout << name <<"'s total is $"<< setprecision(3) << total <<'\n';
	default:
	item = 0;
   break;
	}
}
while (choice==ERROR);
cout << "By: Kenny Van Alstyne.";
return 0;
}

