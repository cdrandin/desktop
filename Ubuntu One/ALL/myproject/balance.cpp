// BALANCE //
// Kenny Van Alstyne //
// September 10, 2003 //
#include<iostream.h>
main()
{
char name[30];
float bbalance;
float cheque;
float deposit1;
float deposit2;
float deposit3;
float deposit4;
float fees;
float balance;
cout << "What is your name? ";
cin.get(name, 30);
cin.ignore(80, '\n');
cout << "What is your beginning balance? $";
cin >> bbalance;
cout << "What is the total checks for the month? $";
cin >> cheque;
cout << "What is the deposit for the first week? $";
cin >> deposit1;
cout << "What is the deposit for the second week? $";
cin >> deposit2;
cout << "What is the deposit for the third week? $";
cin >> deposit3;
cout << "What is the deposit for the fourth week? $";
cin >> deposit4;
cout << "What were the service fees? $";
cin >> fees;
balance = (bbalance + (deposit1 + deposit2 + deposit3 + deposit4)) -
	(cheque + fees);
cout << name << "'s balance is $" << balance << ". \n";
cout << "By: Kenny Van Alstyne.";
return 0;
}


