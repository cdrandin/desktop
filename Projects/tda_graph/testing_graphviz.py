from graphviz import Digraph
import sys
def main():
	print(sys.version)
	dot = Digraph(comment='The Round Table')
	dot.node('A', 'King Arthur')	
	dot.node('B', 'Sir Bedevere the Wise')
	dot.node('L', 'Sir Lancelot the Brave')
	dot.edges(['AB', 'AL'])
	dot.edge('B', 'L', constraint='false')
	print(dot.source)
	dot.render('test-output/round-table.gv', view=True)

if __name__ == '__main__':
	main()