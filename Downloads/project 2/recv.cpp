#include <sys/shm.h>
#include <sys/msg.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include "msg.h"    /* For the message struct */

/* The size of the shared memory chunk */
#define SHARED_MEMORY_CHUNK_SIZE 1000
using namespace std;
/* The ids for the shared memory segment and the message queue */
int shmid, msqid;

/* The pointer to the shared memory */
void *sharedMemPtr;

/* The name of the received file */
const char recvFileName[] = "recvfile";


/**
 * Sets up the shared memory segment and message queue
 * @param shmid - the id of the allocated shared memory 
 * @param msqid - the id of the shared memory
 * @param sharedMemPtr - the pointer to the shared memory
 */
void init(int& shmid, int& msqid, void*& sharedMemPtr)
{
	key_t theKey = ftok("keyfile.txt",'a');
	if(theKey<0)
	{
		perror("ftok");
		exit(-1);
	}
	shmid = shmget(theKey, SHARED_MEMORY_CHUNK_SIZE, IPC_CREAT | 0666);
	sharedMemPtr = (char*)shmat(shmid,0,0);
	if(sharedMemPtr < 0)
	{
		perror("shmat");
		exit(-1);
	}
	msqid = msgget(theKey, IPC_CREAT | 0666); 
	if( msqid < 0 )
	{
		perror("msgget!");
		exit(-1);
	}
	/* Store the IDs and the pointer to the shared memory region in the corresponding parameters */
	
}
 

/**
 * The main loop
 */
void mainLoop()
{
	/* The size of the mesage */
	int msgSize = 0;
	
	/* Open the file for writing */
	FILE* fp = fopen(recvFileName, "w");
		
	/* Error checks */
	if(!fp)
	{
		perror("fopen");	
		exit(-1);
	}
	

	/* Keep receiving until the sender set the size to 0, indicating that
 	 * there is no more data to send
 	 */	
	/*declare messages*/
	message rcvMsg;
	rcvMsg.size = 0;
	message sndMsg;
	sndMsg.mtype = RECV_DONE_TYPE;
	sndMsg.size = 1;
	do
	{	
		if(msgrcv (msqid, &rcvMsg, sizeof(message) - sizeof(long),SENDER_DATA_TYPE,0) < 0)
		{
			perror("msgrcv");
			exit(-1);
		}else{
			msgSize = rcvMsg.size;
		}

		/* If the sender is not telling us that we are done, then get to work */
		/* Save the shared memory to file */
		if(msgSize > 0)
		{
			if(fwrite(sharedMemPtr, sizeof(char), msgSize, fp) < 0)
			{
				perror("fwrite");
			}
			if(msgsnd(msqid, &sndMsg, sizeof(message)-sizeof(long), 0)<0)
			{
				perror("msgsnd2");
				exit(-1);
			}
		}	
			/* TODO: Tell the sender that we are ready for the next file chunk. 
 			 * I.e. send a message of type RECV_DONE_TYPE (the value of size field
 			 * does not matter in this case). 
 			 */
	}while(msgSize != 0);
}



/**
 * Perfoms the cleanup functions
 * @param sharedMemPtr - the pointer to the shared memory
 * @param shmid - the id of the shared memory segment
 * @param msqid - the id of the message queue
 */
void cleanUp(const int& shmid, const int& msqid, void* sharedMemPtr)
{
	shmdt(sharedMemPtr);/* Detach from shared memory */
	
	if(shmctl(shmid,IPC_RMID,0)<0) perror("shmctl");/* Deallocate the shared memory chunk */
	
	if(msgctl(msqid,IPC_RMID,0)<0) perror("msgctl");/* Deallocate the message queue */
	
}

/**
 * Handles the exit signal
 * @param signal - the signal type
 */
void ctrlCSignal(int signal)
{
	/* Free system V resources */
	cleanUp(shmid, msqid, sharedMemPtr);
}

int main(int argc, char** argv)
{
	
	signal(SIGINT,ctrlCSignal); //signal handler for Ctrl-C
	/* Initialize */
	init(shmid, msqid, sharedMemPtr);
	
	/* Go to the main loop */
	mainLoop();
	cleanUp(shmid, msqid, sharedMemPtr);
	return 0;
}
