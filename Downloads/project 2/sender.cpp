#include <sys/shm.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "msg.h"    /* For the message */
#include <iostream>


/* The size of the shared memory chunk */
#define SHARED_MEMORY_CHUNK_SIZE 1000
using namespace std;
/* The ids for the shared memory segment and the message queue */
int shmid, msqid;

/* The pointer to the shared memory */
void* sharedMemPtr;

/**
 * Sets up the shared memory segment and message queue
 * @param shmid - the id of the allocated shared memory 
 * @param msqid - the id of the shared memory
 */
void init(int& shmid, int& msqid, void*& sharedMemPtr)
{
	
	key_t theKey = ftok("keyfile.txt", 'a');
	if(theKey < 0)
	{
		perror("ftok");
		exit(-1);
	}
	shmid = shmget(theKey, SHARED_MEMORY_CHUNK_SIZE, IPC_EXCL | S_IRUSR |S_IWUSR);
	if(shmid < 0)
	{
		perror("shmget");
		exit(-1);
	}
	sharedMemPtr = (void*) shmat (shmid, 0,0);
	if(((void*)sharedMemPtr) < (void*)0)
	{
		perror("shmat");
		exit(-1);
	}
	msqid = msgget(theKey, 0666); //used to have 0666
	if(msqid < 0)
	{
		perror("msgget");
		exit(-1);
	}
	
	/* Store the IDs and the pointer to the shared memory region in the corresponding parameters */
	
}

/**
 * Performs the cleanup functions
 * @param sharedMemPtr - the pointer to the shared memory
 * @param shmid - the id of the shared memory segment
 * @param msqid - the id of the message queue
 */
void cleanUp(const int& shmid, const int& msqid, void* sharedMemPtr)
{
	if(shmdt(sharedMemPtr)<0) perror ("shmdt");
	/* TODO: Detach from shared memory */
}

/**
 * The main send function
 * @param fileName - the name of the file
 */
void send(const char* fileName)
{
	/* Open the file for reading */
	FILE* fp = fopen(fileName, "r");
	
	/* A buffer to store message we will send to the sender. */
	message sndMsg;
	sndMsg.mtype = SENDER_DATA_TYPE; 
	
	/* A buffer to store message received from the receiver. */
	message rcvMsg;
	
	/* Was the file open? */
	if(!fp)
	{
		perror("fopen");
		exit(-1);
	}
	
	/* Read the whole file */
	while(sndMsg.size != 0)
	{
		/* Read at most SHARED_MEMORY_CHUNK_SIZE from the file and store them in shared memory. 
 		 * fread will return how many bytes it has actually read (since the last chunk may be less
 		 * than SHARED_MEMORY_CHUNK_SIZE).
 		 */
		if((sndMsg.size = fread(sharedMemPtr, sizeof(char), SHARED_MEMORY_CHUNK_SIZE, fp)) < 0)
		{
			perror("fread");
			exit(-1);
		}

		if(msgsnd(msqid, &sndMsg, sizeof(sndMsg)-sizeof(long),0)<0)
		{
			perror("msgsnd");
			exit(-1);
		}
		
		if(sndMsg.size != 0)
		{
			if(msgrcv(msqid, &rcvMsg, sizeof(rcvMsg)-sizeof(long),RECV_DONE_TYPE,0)<0) 
			{
				perror("msgrcv");
				exit(-1);
			}
		}

	}
	if(msgsnd(msqid, &sndMsg, sizeof(sndMsg)-sizeof(long),0) < 0) 
	{
		perror("msgsend");
		exit(-1);
	}
	/* Close the file */
	fclose(fp);
	
}

int main(int argc, char** argv)
{
	
	/* Check the command line arguments */
	if(argc < 2)
	{
		fprintf(stderr, "USAGE: %s <FILE NAME>\n", argv[0]);
		exit(-1);
	}
		
	/* Connect to shared memory and the message queue */
	init(shmid, msqid, sharedMemPtr);
	
	/* Send the file */
	send(argv[1]);
	
	/* Cleanup */
	cleanUp(shmid, msqid, sharedMemPtr);
		
	return 0;
}