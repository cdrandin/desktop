#!/usr/bin/python3.2           # This is server.py file

from socket import *
import cPickle as pickle


def main():
	serverPort   = 12345
	serverSocket = socket(AF_INET, SOCK_STREAM)
	serverSocket.bind(('', serverPort))
	serverSocket.listen(1)

	print("Server is ready")

	while True:
		connectionSocket, addr = serverSocket.accept()

		info = connectionSocket.recv(2048)

		# unpickle object from returned pickled object
		info = pickle.loads(info) #data loaded.

		print("From client %s: %s" %(addr, info))

		# change data
		info['first'] = info['first'].upper()
		info['last' ] = info['last' ].upper()

		# pickle object, return unique string object
		info = pickle.dumps(info, -1) 

		connectionSocket.send(info)
		connectionSocket.close()

if __name__ == '__main__':
	main()