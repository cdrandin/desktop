#!/usr/bin/python3.2           # This is client.py file

from socket import *
import cPickle as pickle

def main():
	serverName = ''
	serverPort = 12345

	clientSocket = socket(AF_INET, SOCK_STREAM)
	clientSocket.connect((serverName,serverPort))

	first_name = raw_input('Enter first name: ')
	last_name = raw_input('Enter last name: ')

	info = { 'first': first_name, 'last': last_name }

	# pickle object, return unique string object
	info = pickle.dumps(info, -1) 

	clientSocket.send(info)

	info = clientSocket.recv(2048)

	# unpickle object from returned pickled object
	info = pickle.loads(info) #data loaded.

	print("From server: %s" % (info))

	clientSocket.close()

if __name__ == '__main__':
	main()